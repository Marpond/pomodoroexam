package consultant;

import consultant.records.ConsultantAccount;
import consultant.records.ConsultantAccountSettings;
import database.DatabaseHandlerConsultant;
import date.DateHandler;
import image.ImageHandler;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import sound.SoundHandler;
import stage.StageSwitcher;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;

public class Controller extends StageSwitcher implements Initializable {
    @FXML
    Pane paneRemoveTask,
            paneAddTask;
    @FXML
    TextField textFieldTaskAddEstimatedPomodoros,
            textFieldTaskAddOrder;
    @FXML
    ListView<TaskSegment> listViewOwnTasks,
            listViewUnoccupiedTasks;
    @FXML
    Label labelTimer,
            labelCurrentState,
            labelCurrentTask,
            labelOverallTime,
            labelConfirmFinish,
            labelTaskAddName,
            labelTaskRemove;
    @FXML
    Button buttonTaskAdd,
            buttonTaskRemove,
            buttonInterrupt,
            buttonPlay,
            buttonPause,
            buttonAccount,
            buttonSettings,
            buttonTaskAddCancel,
            buttonTaskAddConfirm,
            buttonTaskRemoveCancel,
            buttonTaskRemoveConfirm,
            buttonConfirmSettings,
            buttonRefresh,
            buttonSound;
    @FXML
    AnchorPane anchorPaneAccount,
            anchorPaneSettings,
            anchorPaneConfirmFinish,
            anchorPaneFinishedForToday;
    private final SoundHandler soundHandler = new SoundHandler();
    private final ImageHandler imageHandler = new ImageHandler();
    private final DateHandler dateHandler = new DateHandler();
    private final DatabaseHandlerConsultant db = DatabaseHandlerConsultant.getInstance();
    private final ConsultantAccount consultantAccount = db.getAllConsultantInfoByEmail(main.Controller.email);
    private ConsultantAccountSettings consultantAccountSettings = db.getAccountSettingsByConsultantEmail(consultantAccount.email());
    private String startHourMinuteSecond;
    private Timeline pomodoroTimer;
    private Timeline overallTimer;
    // Counter used to determine when is the next long break.
    private int workCounter = 0;
    // Counter used to determine if the estimated time has been reached.
    private int pomodoroCounter = 0;
    private boolean isSoundOn = true;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        System.out.println(consultantAccount.email());
        // Create a new to-do list if the consultant has none for the current day
        db.insertToDoList(consultantAccount.email());
        // Initial visibility values of the UI elements
        buttonPause.setVisible(false);
        paneRemoveTask.setVisible(false);
        paneAddTask.setVisible(false);
        anchorPaneAccount.setVisible(false);
        anchorPaneSettings.setVisible(false);
        anchorPaneConfirmFinish.setVisible(false);
        anchorPaneFinishedForToday.setVisible(false);
        buttonInterrupt.setDisable(true);
        // Set the graphics of the buttons
        buttonPlay.setGraphic(imageHandler.getImageView("src/main/resources/image/play.png", 50));
        buttonPause.setGraphic(imageHandler.getImageView("src/main/resources/image/pause.png", 50));
        buttonInterrupt.setGraphic(imageHandler.getImageView("src/main/resources/image/interrupt.png", 50));
        buttonRefresh.setGraphic(imageHandler.getImageView("src/main/resources/image/refresh.png", 30));
        buttonTaskRemove.setGraphic(imageHandler.getImageView("src/main/resources/image/delete.png", 30));
        buttonTaskAdd.setGraphic(imageHandler.getImageView("src/main/resources/image/add.png", 30));
        buttonAccount.setGraphic(imageHandler.getImageView("src/main/resources/image/account.png", 30));
        buttonSettings.setGraphic(imageHandler.getImageView("src/main/resources/image/account_settings.png", 30));
        buttonSound.setGraphic(imageHandler.getImageView("src/main/resources/image/sound_on.png", 30));
        buttonTaskRemoveCancel.setGraphic(imageHandler.getImageView("src/main/resources/image/cancel.png", 30));
        buttonTaskAddCancel.setGraphic(imageHandler.getImageView("src/main/resources/image/cancel.png", 30));
        buttonTaskAddConfirm.setGraphic(imageHandler.getImageView("src/main/resources/image/list_add.png", 30));
        buttonTaskRemoveConfirm.setGraphic(imageHandler.getImageView("src/main/resources/image/list_remove.png", 30));
        // Set the listViews
        updateListViewOwnTasks();
        updateListViewUnoccupiedTasks();
        // If the pomodoro has already started, then the consultant is not allowed to change the to-do list
        boolean isPomodoroStarted = db.checkIfConsultantStartedByConsultantEmail(consultantAccount.email());
        buttonTaskRemove.setDisable(isPomodoroStarted);
        buttonTaskAdd.setDisable(isPomodoroStarted);
        listViewUnoccupiedTasks.setDisable(isPomodoroStarted);
        listViewOwnTasks.setDisable(isPomodoroStarted);
        // These listeners deselect the previously selected item in the other listView
        if (listViewOwnTasks != null) {
            setSelectionSwitcher(listViewOwnTasks, listViewUnoccupiedTasks);
        }
        if (listViewUnoccupiedTasks != null) {
            setSelectionSwitcher(listViewUnoccupiedTasks, listViewOwnTasks);
        }
        // Bindings and listeners for the add task pane
        textFieldTaskAddOrder.textProperty().addListener((observable, oldValue, newValue) -> {
            // If it's a number and smaller than the number of tasks + 1, change the text-field to the number
            try {
                if (newValue.matches("\\d*") &&
                        Integer.parseInt(newValue) <= listViewOwnTasks.getItems().size() + 1 &&
                        Integer.parseInt(newValue) > 0) {
                    textFieldTaskAddOrder.setText(newValue);
                } else {
                    textFieldTaskAddOrder.setText(oldValue);
                }
            } catch (NumberFormatException ignored) {
            }
        });
        textFieldTaskAddEstimatedPomodoros.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                // If it's a number and bigger than 0, change the text-field to the number
                if (newValue.matches("\\d*") && Integer.parseInt(newValue) > 0) {
                    textFieldTaskAddEstimatedPomodoros.setText(newValue);
                } else {
                    textFieldTaskAddEstimatedPomodoros.setText(oldValue);
                }
            } catch (NumberFormatException ignored) {
            }
        });
        textFieldTaskAddEstimatedPomodoros.disableProperty().bind(textFieldTaskAddOrder.textProperty().isEmpty());
        // if textFieldTaskAddOrder and textFieldTaskAddEstimatedPomodoros are not empty, then the button is enabled
        buttonTaskAddConfirm.disableProperty().bind(textFieldTaskAddOrder.textProperty().isEmpty().or(textFieldTaskAddEstimatedPomodoros.textProperty().isEmpty()));

        buttonTaskAdd.disableProperty().bind(listViewUnoccupiedTasks.selectionModelProperty().get().selectedItemProperty().isNull());
        buttonTaskRemove.disableProperty().bind(listViewOwnTasks.selectionModelProperty().get().selectedItemProperty().isNull());
    }

    /**
     * Helper method that sets the listener for the own and unoccupied tasks listViews
     * Changes the selection of the listViews
     *
     * @param mainListView      the newly selected listView
     * @param secondaryListView the previously selected listView
     */
    private void setSelectionSwitcher(ListView<TaskSegment> mainListView, ListView<TaskSegment> secondaryListView) {
        mainListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                System.out.println(newValue.getTaskID());
                // If a task is selected in the secondary list, deselect it
                if (secondaryListView.getSelectionModel().getSelectedItem() != null) {
                    secondaryListView.getSelectionModel().clearSelection();
                }
            }
        });
    }

    /**
     * Action event for the buttonPlayPause
     */
    @FXML
    private void onButtonPlayClick() {
        playSoundPomodoro();
        listViewOwnTasks.getSelectionModel().clearSelection();
        listViewUnoccupiedTasks.getSelectionModel().clearSelection();
        // Set the values of the tasks list which will be used to keep track of the tasks
        if (!db.checkIfConsultantStartedByConsultantEmail(consultantAccount.email())) {
            System.out.println("Started pomodoro");
            db.updateTaskStatusByTaskID(listViewOwnTasks.getItems().get(0).getTaskID());
            buttonConfirmSettings.setDisable(true);
            listViewOwnTasks.setDisable(true);
            listViewUnoccupiedTasks.setDisable(true);
            startOverallTimer();
            startPomodoroTimer();
            startHourMinuteSecond = dateHandler.getCurrentHHMMSS();
        }
        buttonInterrupt.setDisable(true);
        if (db.checkIfConsultantPausedByConsultantEmail(consultantAccount.email())) {
            System.out.println("Resumed pomodoro");
            db.updatePauseStatusByConsultantEmail(consultantAccount.email());
            pomodoroTimer.play();
        }
        // Set the new values of the buttons
        buttonPlay.setVisible(false);
        buttonPause.setVisible(true);
        buttonPause.setDisable(true);
        temporarilyChangeButtonDisableProperty(buttonInterrupt);
        temporarilyChangeButtonDisableProperty(buttonPause);
        labelCurrentTask.setText(listViewOwnTasks.getItems().get(0).getTaskName());
        updateLabelCurrentState();
    }

    private void updateLabelCurrentState() {
        if (pomodoroTimer.getCycleCount() / 60 == consultantAccountSettings.workTime()) {
            labelCurrentState.setText("Working");
        } else if (pomodoroTimer.getCycleCount() / 60 == consultantAccountSettings.shortBreakTime()) {
            labelCurrentState.setText("Short break");
        } else if (pomodoroTimer.getCycleCount() / 60 == consultantAccountSettings.longBreakTime()) {
            labelCurrentState.setText("Long break");
        }
    }

    @FXML
    private void onButtonPauseClick() {
        playSoundPomodoro();
        System.out.println("Pausing");
        labelCurrentState.setText("Paused");
        db.updatePauseStatusByConsultantEmail(consultantAccount.email());
        // Set the new values of the buttons
        buttonPause.setVisible(false);
        buttonPlay.setVisible(true);
        buttonInterrupt.setDisable(true);
        temporarilyChangeButtonDisableProperty(buttonPlay);
        // Pause the timer
        pomodoroTimer.pause();
    }

    @FXML
    private void onButtonConfirmFinishClick() {
        playSoundClick();
        int taskID = listViewOwnTasks.getItems().get(0).getTaskID();
        System.out.println("Finishing task " + taskID);
        // Finish the current task
        db.updateTaskStatusByTaskID(taskID);
        db.insertTotalPomodorosByTaskID(taskID, pomodoroCounter);
        listViewOwnTasks.getItems().remove(0);
        pomodoroCounter = 0;
        anchorPaneConfirmFinish.setVisible(false);
        // Start the next task
        if (listViewOwnTasks.getItems().size() > 0) {
            labelCurrentTask.setText(listViewOwnTasks.getItems().get(0).getTaskName());
        } else {
            pomodoroTimer.stop();
            overallTimer.stop();
            db.updateBreakStatusByConsultantEmail(consultantAccount.email());
            anchorPaneFinishedForToday.setVisible(true);
        }
    }

    @FXML
    private void onButtonCancelFinishClick() {
        playSoundClick();
        System.out.println("Cancelling task finish" + listViewOwnTasks.getItems().get(0).getTaskID());
        anchorPaneConfirmFinish.setVisible(false);
    }

    @FXML
    private void onButtonSoundClick() {
        if (isSoundOn) {
            isSoundOn = false;
            buttonSound.setGraphic(imageHandler.getImageView("src/main/resources/image/sound_off.png", 30));
        } else {
            isSoundOn = true;
            playSoundClick();
            buttonSound.setGraphic(imageHandler.getImageView("src/main/resources/image/sound_on.png", 30));
        }
    }

    private void temporarilyChangeButtonDisableProperty(Button button) {
        button.setDisable(true);
        // Have to wait for the database to update
        new Timeline(new KeyFrame(Duration.seconds(1.5), e ->
                button.setDisable(false))).play();
    }

    private void startOverallTimer() {
        overallTimer = new Timeline(new KeyFrame(Duration.seconds(1), e ->
                labelOverallTime.setText(
                        dateHandler.getDifferenceHHMMSS(
                                startHourMinuteSecond,
                                dateHandler.getCurrentHHMMSS()
                        ))));
        overallTimer.setCycleCount(Timeline.INDEFINITE);
        overallTimer.play();
    }

    private void startPomodoroTimer() {
        AtomicInteger counter = new AtomicInteger();
        // Work segment
        if (db.checkIfConsultantHasBreakByConsultantEmail(consultantAccount.email())) {
            db.updateBreakStatusByConsultantEmail(consultantAccount.email());
        }
        int taskID = listViewOwnTasks.getItems().get(0).getTaskID();
        if (!db.checkIfConsultantHasOnGoingTaskByEmail(consultantAccount.email())) {
            db.updateTaskStatusByTaskID(taskID);
            System.out.println("Starting task " + taskID);
        } else {
            System.out.println("Consultant has an ongoing task");
        }
        setTimelineProperties(consultantAccountSettings.workTime(), counter);
        pomodoroTimer.play();
        updateLabelCurrentState();
        // Break segment
        pomodoroTimer.setOnFinished(e -> {
            playSoundBreakStart();
            workCounter++;
            pomodoroCounter++;
            // Estimated time has been reached
            if (pomodoroCounter >= db.getTaskEstimatedPomodorosByTaskID(taskID)) {
                showAnchorPaneConfirmFinish();
            }
            counter.set(0);
            db.updateBreakStatusByConsultantEmail(consultantAccount.email());
            // Long break
            if (workCounter % consultantAccountSettings.longBreakInterval() == 0) {
                setTimelineProperties(consultantAccountSettings.longBreakTime(), counter);
            }
            // Short break
            else {
                setTimelineProperties(consultantAccountSettings.shortBreakTime(), counter);
            }
            pomodoroTimer.play();
            updateLabelCurrentState();
            pomodoroTimer.setOnFinished(e1 -> {
                startPomodoroTimer();
                playSoundBreakFinish();
            });
        });
    }

    private void setTimelineProperties(int lengthMinutes, AtomicInteger counter) {
        pomodoroTimer = new Timeline(new KeyFrame(Duration.seconds(1), e -> {
            // Update the time
            labelTimer.setText(String.format("%s",
                    dateHandler.formatIntMinuteToHHMM(lengthMinutes * 60 - counter.incrementAndGet())));
        }));
        pomodoroTimer.setCycleCount(lengthMinutes * 60);
    }

    /**
     * "When the user clicks the Interrupt button, print a message to the console."
     * <p>
     * The first line of the function is a Java annotation. Annotations are a way to add metadata to Java code. In this case, the annotation
     * tells the JavaFX framework that this function should be called when the user clicks the Interrupt button
     */
    @FXML
    private void onButtonInterruptClick() {
        playSoundPomodoro();
        System.out.println("Interrupting");
        // Get to the end of the timeline
        temporarilyChangeButtonDisableProperty(buttonInterrupt);
        Timeline tempTimeline = pomodoroTimer;
        pomodoroTimer.stop();
        pomodoroTimer = tempTimeline;
        pomodoroTimer.setCycleCount(1);
        pomodoroTimer.play();
        // Have to wait one second for the timeline to get to the other cycle
        new Timeline(new KeyFrame(Duration.seconds(1), e ->
                updateLabelCurrentState())).play();
        labelTimer.setVisible(false);
        new Timeline(new KeyFrame(Duration.seconds(2.1), e ->
                labelTimer.setVisible(true)
        )).play();
    }

    /**
     * When the Account button is clicked, if the Account anchor pane is visible, hide it. If the Settings anchor pane is visible, hide it.
     * Then, set the Account anchor pane to visible and set the values of the anchor pane to the values of the consultantAccount object
     */
    @FXML
    private void onButtonAccountClick() {
        playSoundClick();
        System.out.println("Account clicked");
        if (anchorPaneAccount.isVisible()) {
            anchorPaneAccount.setVisible(false);
            return;
        }
        if (anchorPaneSettings.isVisible()) {
            anchorPaneSettings.setVisible(false);
        }
        anchorPaneAccount.setVisible(true);
        // Inside the anchor-pane is an HBox with 2 VBoxes inside it.
        // First vbox is the title of the info, second is the info itself.
        setAnchorPaneValues(anchorPaneAccount, consultantAccount);
    }

    /**
     * Action event for the buttonSettings.
     */
    @FXML
    private void onButtonSettingsClick() {
        playSoundClick();
        System.out.println("Settings clicked");
        if (anchorPaneSettings.isVisible()) {
            anchorPaneSettings.setVisible(false);
            return;
        }
        if (anchorPaneAccount.isVisible()) {
            anchorPaneAccount.setVisible(false);
        }
        anchorPaneSettings.setVisible(true);
        buttonConfirmSettings.setDisable(db.checkIfConsultantStartedByConsultantEmail(consultantAccount.email()));
        setAnchorPaneValues(anchorPaneSettings, consultantAccountSettings);
    }

    @FXML
    private void onButtonConfirmSettingsClick() {
        playSoundClick();
        anchorPaneSettings.getChildren().forEach(node -> {
            if (node instanceof VBox mainVBox) {
                String[] type = {"workTime", "shortBreakTime", "longBreakTime", "longBreakInterval"};
                HashMap<String, Integer> settings = new HashMap<>();
                HBox hBox = (HBox) mainVBox.getChildren().get(0);
                VBox vBox = (VBox) hBox.getChildren().get(1);
                AtomicInteger i = new AtomicInteger(0);
                vBox.getChildren().forEach(node1 -> {
                    if (node1 instanceof TextField textField) {
                        settings.put(type[i.getAndIncrement()], Integer.parseInt(textField.getText()));
                    }
                });
                db.updateAccountSettings(
                        consultantAccount.email(),
                        settings.get("workTime"),
                        settings.get("shortBreakTime"),
                        settings.get("longBreakTime"),
                        settings.get("longBreakInterval")
                );
            }
        });
        System.out.println("Settings confirmed");
        consultantAccountSettings = db.getAccountSettingsByConsultantEmail(consultantAccount.email());
        anchorPaneSettings.setVisible(false);
    }

    @FXML
    private void onButtonRefreshClick() {
        playSoundClick();
        updateListViewUnoccupiedTasks();
    }


    /**
     * For each field in the object, create a new label and add it to the vbox
     *
     * @param anchorPane The anchor pane that will be used to set the values of the object.
     * @param object     The object to get the values from
     */
    private void setAnchorPaneValues(AnchorPane anchorPane, Object object) {
        HBox hBox;
        if (object instanceof ConsultantAccountSettings) {
            VBox mainVbox = (VBox) anchorPane.getChildren().get(0);
            hBox = (HBox) mainVbox.getChildren().get(0);
        } else {
            hBox = (HBox) anchorPane.getChildren().get(0);
        }
        AtomicInteger vBoxIndex = new AtomicInteger(0);
        // Get the count of the fields in the object
        hBox.getChildren().forEach(node -> {
            if (node instanceof VBox vBox) {
                vBox.getChildren().clear();
                vBox.setPadding(new Insets(10, 10, 10, 10));
                // First vbox, field names
                if (vBoxIndex.getAndIncrement() == 0) {
                    if (object instanceof ConsultantAccount) {
                        Label labelEmail = new Label("Email: ");
                        Label labelFName = new Label("First name: ");
                        Label labelLName = new Label("Last name: ");
                        Label labelPhoneNo = new Label("Phone number: ");
                        Label labelZip = new Label("Zip code: ");
                        Label labelAddress = new Label("address: ");
                        Label officeID = new Label("Office ID: ");
                        vBox.getChildren().addAll(labelEmail, labelFName, labelLName, labelPhoneNo, labelZip, labelAddress, officeID);
                    } else if (object instanceof ConsultantAccountSettings) {
                        Label labelWorkTime = new Label("Work time: ");
                        Label labelShortBreakTime = new Label("Short break time: ");
                        Label labelLongBreakTime = new Label("Long break time: ");
                        Label labelLongBreakInterval = new Label("Long break interval: ");
                        vBox.getChildren().addAll(labelWorkTime, labelShortBreakTime, labelLongBreakTime, labelLongBreakInterval);
                        vBox.getChildren().forEach(node1 -> {
                            if (node1 instanceof Label label) {
                                label.setPrefHeight(30);
                            }
                        });
                    }
                    vBox.setPrefHeight(vBox.getChildren().size() * 30);
                }
                // Second vbox, field values
                else {
                    if (object instanceof ConsultantAccount consultantAccount) {
                        Label labelEmail = new Label(consultantAccount.email());
                        Label labelFName = new Label(consultantAccount.firstName());
                        Label labelLName = new Label(consultantAccount.lastName());
                        Label labelPhoneNo = new Label(consultantAccount.phoneNumber());
                        Label labelZip = new Label(consultantAccount.zip());
                        Label labelAddress = new Label(consultantAccount.address());
                        Label officeID = new Label(String.valueOf(consultantAccount.officeID()));
                        vBox.getChildren().addAll(labelEmail, labelFName, labelLName, labelPhoneNo, labelZip, labelAddress, officeID);
                    } else if (object instanceof ConsultantAccountSettings consultantAccountSettings) {
                        TextField textFieldWorkTime = new TextField(String.valueOf(consultantAccountSettings.workTime()));
                        TextField textFieldShortBreakTime = new TextField(String.valueOf(consultantAccountSettings.shortBreakTime()));
                        TextField textFieldLongBreakTime = new TextField(String.valueOf(consultantAccountSettings.longBreakTime()));
                        TextField textFieldLongBreakInterval = new TextField(String.valueOf(consultantAccountSettings.longBreakInterval()));
                        vBox.getChildren().addAll(textFieldWorkTime, textFieldShortBreakTime, textFieldLongBreakTime, textFieldLongBreakInterval);
                        vBox.getChildren().forEach(node1 ->
                        {
                            if (node1 instanceof TextField textField) {
                                textField.textProperty().addListener((observable, oldValue, newValue) -> {
                                    if (newValue.matches("\\d*")) {
                                        textField.setText(newValue);
                                    } else {
                                        textField.setText(oldValue);
                                    }
                                });
                                textField.setDisable(db.checkIfConsultantStartedByConsultantEmail(consultantAccount.email()));
                            }
                        });
                    }
                    vBox.setPrefHeight(vBox.getChildren().size() * 30);
                }
            }
        });
    }

    /**
     * Action event for the buttonTaskAdd.
     */
    @FXML
    private void onButtonTaskAddClick() {
        System.out.println("Add task clicked");
        int taskID = listViewUnoccupiedTasks.getSelectionModel().getSelectedItem().getTaskID();
        if (db.checkIfTaskIsEditableByTaskIDConsultantEmail(taskID, consultantAccount.email())) {
            playSoundClick();
            paneAddTask.setVisible(true);
            labelTaskAddName.setText("""
                    Selected task:
                    %s
                    """.formatted(
                    listViewUnoccupiedTasks.getSelectionModel().getSelectedItem().getTaskName()
            ));
        }
    }

    /**
     * Action event for the buttonTaskAddCancel.
     */
    @FXML
    private void onButtonTaskAddCancelClick() {
        playSoundClick();
        paneAddTask.setVisible(false);
    }

    /**
     * Action event for the buttonTaskAddConfirm.
     */
    @FXML
    private void onButtonTaskAddConfirmClick() {
        playSoundClick();
        int taskID = listViewUnoccupiedTasks.getSelectionModel().getSelectedItem().getTaskID();
        db.setOccupiedTaskByEmailTaskIDOrderEstimatedPomodoros(
                consultantAccount.email(),
                taskID,
                Integer.parseInt(textFieldTaskAddOrder.getText()),
                Integer.parseInt(textFieldTaskAddEstimatedPomodoros.getText())
        );
        System.out.println("New task added : " + taskID);
        System.out.println("New task order : " + textFieldTaskAddOrder.getText());
        System.out.println("New task estimated pomodoros : " + textFieldTaskAddEstimatedPomodoros.getText());
        paneAddTask.setVisible(false);
        db.reorderTasksByTaskID(taskID);
        updateListViewOwnTasks();
        updateListViewUnoccupiedTasks();
    }

    /**
     * Action event for the buttonTaskRemove.
     */
    @FXML
    private void onButtonTaskRemoveClick() {
        playSoundClick();
        System.out.println("Remove task clicked");
        paneRemoveTask.setVisible(true);
        labelTaskRemove.setText("""
                Removing task
                %s
                Are you sure?
                """.formatted(
                listViewOwnTasks.getSelectionModel().getSelectedItem().getTaskName()
        ));
    }

    /**
     * Action event for the buttonTaskRemoveCancel.
     */
    @FXML
    private void onButtonTaskRemoveCancelClick() {
        playSoundClick();
        paneRemoveTask.setVisible(false);
    }

    /**
     * Action event for the buttonTaskRemoveConfirm.
     */
    @FXML
    private void onButtonTaskRemoveConfirmClick() {
        playSoundClick();
        int taskID = listViewOwnTasks.getSelectionModel().getSelectedItem().getTaskID();
        db.unoccupyTaskByTaskID(taskID);
        System.out.println("Task removed : " + taskID);
        paneRemoveTask.setVisible(false);
        db.correctTaskOrdersByConsultantEmail(consultantAccount.email());
        updateListViewOwnTasks();
        updateListViewUnoccupiedTasks();
    }

    /**
     * Method needed to update the listViewOwnTasks after any change in the database.
     */
    private void updateListViewOwnTasks() {
        if (listViewOwnTasks == null) {
            return;
        }
        listViewOwnTasks.getItems().clear();
        db.getAllTasksTodayByConsultantEmail(consultantAccount.email())
                .forEach(task -> {
                    TaskSegment taskSegment = new TaskSegment(task, true);
                    listViewOwnTasks.getItems().add(taskSegment);
                });
        buttonPlay.setDisable(listViewOwnTasks.getItems().isEmpty() || db.checkIfConsultantStartedByConsultantEmail(consultantAccount.email()));
    }

    /**
     * Method needed to update the listViewUnoccupiedTasks after any change in the database.
     */
    private void updateListViewUnoccupiedTasks() {
        if (listViewUnoccupiedTasks == null) {
            return;
        }
        listViewUnoccupiedTasks.getItems().clear();
        // Editable tasks
        db.getAllEditableTasksByConsultantEmail(consultantAccount.email())
                .forEach(task -> {
                    TaskSegment taskSegment = new TaskSegment(task, true);
                    listViewUnoccupiedTasks.getItems().add(taskSegment);
                });
        // Non-project tasks
        db.getAllNonProjectTasks()
                .forEach(task -> {
                    TaskSegment taskSegment = new TaskSegment(task, true);
                    listViewUnoccupiedTasks.getItems().add(taskSegment);
                });
        // Viewable tasks
        db.getAllViewableTasksByConsultantEmail(consultantAccount.email())
                .forEach(task -> {
                    TaskSegment taskSegment = new TaskSegment(task, false);
                    listViewUnoccupiedTasks.getItems().add(taskSegment);
                });
    }

    private void showAnchorPaneConfirmFinish() {
        int taskEstimatedPomodoros = db.getTaskEstimatedPomodorosByTaskID(listViewOwnTasks.getItems().get(0).getTaskID());
        String taskName = listViewOwnTasks.getItems().get(0).getTaskName();
        labelConfirmFinish.setText(("""
                The estimated pomodoros of
                %d
                has been reached for the task
                %s.
                Do you want to finish it?""")
                .formatted(taskEstimatedPomodoros, taskName));
        anchorPaneConfirmFinish.setVisible(true);
    }

    private void playSoundBreakStart() {
        if (isSoundOn) {
            soundHandler.play("break_start.mp3");
        }
    }

    private void playSoundBreakFinish() {
        if (isSoundOn) {
            soundHandler.play("break_finish.mp3");
        }
    }

    private void playSoundClick() {
        if (isSoundOn) {
            soundHandler.play("click.mp3");
        }
    }

    private void playSoundPomodoro() {
        if (isSoundOn) {
            soundHandler.play("pomodoro.mp3");
        }
    }

}
