package consultant;

import database.DatabaseHandlerConsultant;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import stage.StageSwitcher;

import java.net.URL;
import java.util.ResourceBundle;

public class ControllerSetup extends StageSwitcher implements Initializable {

    @FXML
    Label labelOptionTitle;
    @FXML
    Label labelWorking, labelShortBreak, labelLongBreak, labelWorkingTime, labelShortBreakTime, labelLongBreakTime;
    @FXML
    TextField textFieldCounter;
    @FXML
    ProgressBar progressBarSetup;
    private final DatabaseHandlerConsultant db = DatabaseHandlerConsultant.getInstance();
    private final int[] setupValues = {0, 0, 0};
    @FXML
    Button buttonBack, buttonNext;
    String[] setupPages = {"Working time", "Short break time", "Long break time", "Confirm settings"};
    private int pageNum = 0;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        buttonBack.setDisable(true);
        // If the input is invalid (empty, is text, smaller than 1 or bigger than 60), disable the next button
        buttonNext.disableProperty().bind(textFieldCounter.textProperty().isEmpty());
        textFieldCounter.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                // Set the new value to the textfield, if it's a valid number between 1 and 60
                // otherwise use the old value
                if (newValue.matches("\\d*")
                        && Integer.parseInt(newValue) > 0
                        && Integer.parseInt(newValue) <= 60) {
                    textFieldCounter.setText(newValue);
                } else {
                    textFieldCounter.setText(oldValue);
                }
            } catch (NumberFormatException ignored) {
            }
            // If we're at the first page, the back button is disabled
            buttonBack.setDisable(pageNum == 0);
        });
        // If textfield isn't visible, then the label is visible
        labelWorking.visibleProperty().bind(textFieldCounter.visibleProperty().not());
        labelShortBreak.visibleProperty().bind(textFieldCounter.visibleProperty().not());
        labelLongBreak.visibleProperty().bind(textFieldCounter.visibleProperty().not());
        labelWorkingTime.visibleProperty().bind(textFieldCounter.visibleProperty().not());
        labelShortBreakTime.visibleProperty().bind(textFieldCounter.visibleProperty().not());
        labelLongBreakTime.visibleProperty().bind(textFieldCounter.visibleProperty().not());
    }
    private void updatePage() {
        // We're not at the last page
        if (pageNum < 3) {
            buttonNext.setText("Next");
            textFieldCounter.setText(String.valueOf(setupValues[pageNum]));
            progressBarSetup.setProgress(pageNum * 0.33);
        } else if (pageNum == 3) {
            // Set the textFieldCounter's value to 1, otherwise buttonNext will be disabled
            textFieldCounter.setText("1");
            buttonNext.setText("Save");
            labelWorkingTime.setText(String.valueOf(setupValues[0]));
            labelShortBreakTime.setText(String.valueOf(setupValues[1]));
            labelLongBreakTime.setText(String.valueOf(setupValues[2]));
            progressBarSetup.setProgress(1.0);
        } else if (pageNum == 4) {
            // Insert the new profile preferences into the database
            int longBreakInterval = 4;
            db.insertAccountSettings(main.Controller.email, setupValues[0], setupValues[1], setupValues[2], longBreakInterval);
            // Switch to the consultant scene
            DatabaseHandlerConsultant.closeConnection();
            ControllerSetup.switchTo("consultant.fxml", Controller.class);
        }
        if (pageNum > 3) {
            return;
        }
        labelOptionTitle.setText(setupPages[pageNum]);
        textFieldCounter.setVisible(pageNum < 3);
    }

    @FXML
    private void updateSetupValue() {
        if (textFieldCounter.getText().equals("") || pageNum > setupValues.length) {
            return;
        }
        setupValues[pageNum] = Integer.parseInt(textFieldCounter.getText());
    }

    @FXML
    private void onButtonNextClick() {
        textFieldCounter.setText("");
        pageNum++;
        updatePage();
    }

    @FXML
    private void onButtonBackClick() {
        pageNum--;
        updatePage();
    }

}
