package consultant;

import stage.StageLoader;

public class MainForTest extends StageLoader {

    public static void main(String[] args) {
        MainForTest.setFxml("consultant.fxml");
        launch(args);
    }
}
