package consultant.records;

public record ConsultantAccountSettings(int workTime, int shortBreakTime, int longBreakTime, int longBreakInterval) {
}
