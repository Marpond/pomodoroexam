package consultant.records;

public record ConsultantAccount(String email, String firstName, String lastName, String phoneNumber, String zip, String address,
                                int officeID) {
}