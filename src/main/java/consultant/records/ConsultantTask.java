package consultant.records;

public record ConsultantTask(int taskID, int projectID, String taskName, String description, int priority,
                             int estimatedPomodoros, String startTime, String finishTime, int order) {
}
