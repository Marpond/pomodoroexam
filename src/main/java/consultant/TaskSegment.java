package consultant;

import consultant.records.ConsultantTask;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class TaskSegment extends HBox {
    private final int taskID;
    private final String taskName;

    /**
     * Constructor for TaskSegment
     *
     * @param task       The task to be displayed
     * @param isEditable The permission of the consultant. A parameter for view only is not needed as not viewable tasks are
     *                   not displayed in the first place.
     */
    public TaskSegment(ConsultantTask task, boolean isEditable) {
        this.taskID = task.taskID();
        this.taskName = task.taskName();
        int maxWidth = 160;
        int maxHeight = 120;
        this.setPrefWidth(maxWidth);
        this.setPrefHeight(maxHeight);
        // Left side Pane
        // order number
        Pane orderNumberPane = new Pane();
        orderNumberPane.setPrefWidth(20);
        orderNumberPane.setPrefHeight(maxHeight);
        orderNumberPane.setStyle("-fx-background-color: #1e1e1e;");
        Label labelOrderNumber = new Label(String.valueOf(task.order()));
        labelOrderNumber.setPrefWidth(20);
        labelOrderNumber.setPrefHeight(maxHeight);
        labelOrderNumber.setAlignment(Pos.CENTER);
        labelOrderNumber.setStyle("-fx-font-size: 15px;");
        orderNumberPane.getChildren().add(labelOrderNumber);
        this.getChildren().add(orderNumberPane);
        // Right side VBox
        VBox vBox = new VBox();
        vBox.setPrefWidth(maxWidth - orderNumberPane.getPrefWidth());
        vBox.setPrefHeight(maxHeight);
        // task name
        Pane taskNamePane = new Pane();
        taskNamePane.setPrefWidth(maxWidth - orderNumberPane.getPrefWidth());
        taskNamePane.setPrefHeight(maxHeight - 90);
        taskNamePane.setStyle("-fx-background-color: #1e1e1e;");
        Label labelTaskName = new Label(task.taskName());
        labelTaskName.setPrefWidth(maxWidth - orderNumberPane.getPrefWidth());
        labelTaskName.setPrefHeight(maxHeight - 90);
        labelTaskName.setAlignment(Pos.CENTER);
        labelTaskName.setStyle("-fx-font-size: 15px;");
        taskNamePane.getChildren().add(labelTaskName);
        vBox.getChildren().add(taskNamePane);
        // task id, project id, priority
        Label labelTaskIDProjectIDPriority =
                new Label("T_ID: " + task.taskID() + " P_ID: " + task.projectID() + " Pri: " + task.priority());
        labelTaskIDProjectIDPriority.setPrefWidth(maxWidth - orderNumberPane.getPrefWidth());
        labelTaskIDProjectIDPriority.setPrefHeight(maxHeight - 100);
        labelTaskIDProjectIDPriority.setStyle("-fx-font-size: 12px;");
        labelTaskIDProjectIDPriority.setAlignment(Pos.TOP_CENTER);
        vBox.getChildren().add(labelTaskIDProjectIDPriority);
        // estimated time
        Label labelEstimatedTime = new Label("Estimated pomodoros: " + task.estimatedPomodoros());
        labelEstimatedTime.setPrefWidth(maxWidth - orderNumberPane.getPrefWidth());
        labelEstimatedTime.setPrefHeight(maxHeight - 100);
        labelEstimatedTime.setStyle("-fx-font-size: 12px;");
        labelEstimatedTime.setAlignment(Pos.TOP_CENTER);
        vBox.getChildren().add(labelEstimatedTime);
        // description
        Label labelDescription = new Label(task.description());
        labelDescription.setStyle("-fx-font-size: 12px;");
        labelDescription.setPrefWidth(maxWidth - orderNumberPane.getPrefWidth());
        labelDescription.setPrefHeight(maxHeight - 60);
        labelDescription.setWrapText(true);
        labelDescription.setAlignment(Pos.TOP_LEFT);
        vBox.getChildren().add(labelDescription);
        // add everything to the main containers
        this.getChildren().add(vBox);
        if (!isEditable) {
            this.setOpacity(0.5);
        }
    }

    public int getTaskID() {
        return this.taskID;
    }

    public String getTaskName() {
        return taskName;
    }
}
