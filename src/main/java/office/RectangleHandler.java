package office;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class RectangleHandler {

    public RectangleHandler() {
    }

    /**
     * @param x      the x coordinate of the top left corner of the rectangle
     * @param y      the y coordinate of the top left corner of the rectangle
     * @param width  the width of the rectangle
     * @param height the height of the rectangle
     * @param color  the color of the rectangle
     * @return a rectangle with the given parameters.
     */
    public Rectangle getRectangle(double x, double y, double width, double height, Color color) {
        Rectangle rectangle = new Rectangle(x, y, width, height);
        rectangle.setFill(color);
        return rectangle;
    }
}
