package office;

import database.DatabaseHandlerOffice;
import date.DateHandler;
import image.ImageHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import office.records.OfficeAccount;
import office.records.OfficeAccountSettings;
import office.records.OfficeTask;

import java.sql.Time;
import java.util.List;

public class ProgressSegment extends Pane {

    /**
     * Adds a pane that contains the progress of the consultant, their email and also a button that lists the consultant's
     * progress in further detail in an anchor pane.
     *
     * @param db                          The database handler.
     * @param officeAccount               The consultant.
     * @param officeAccountSettings       The consultant's settings.
     * @param officeTasks                 The consultant's tasks.
     * @param vBoxConsultantProgress      The pane that the is needed for the relative pixel value of the segments.
     * @param anchorPaneConsultantDetails The anchor pane that will be used to display the consultant's details.
     * @param rectangleVerticalLine       The vertical line that is used to determine the width of the rectangle in case the
     *                                    consultant is in the middle of the certain status.
     * @param dateHandler                 The date handler class.
     */
    public ProgressSegment(DatabaseHandlerOffice db, OfficeAccount officeAccount, OfficeAccountSettings officeAccountSettings,
                           List<OfficeTask> officeTasks, VBox vBoxConsultantProgress, AnchorPane anchorPaneConsultantDetails,
                           Rectangle rectangleVerticalLine, DateHandler dateHandler) {
        System.out.printf("""
                        |------------------------------------------------------|
                        | Generating progress for %s.
                        | Work time: %s
                        | Short break time: %s
                        | Long break time: %s
                        | Long break interval: %s
                        | Tasks: %s
                        |------------------------------------------------------|
                        """,
                officeAccount.email(),
                officeAccountSettings.workTime(),
                officeAccountSettings.shortBreakTime(),
                officeAccountSettings.longBreakTime(),
                officeAccountSettings.longBreakInterval(),
                officeTasks.size());
        VBox mainVBox = new VBox();
        mainVBox.setSpacing(0);
        mainVBox.setPrefHeight(80);
        mainVBox.setPrefWidth(vBoxConsultantProgress.getPrefWidth());
        //region Contents of details hbox
        Label labelBigEmail = new Label(officeAccount.email());
        labelBigEmail.setPrefHeight(30);
        labelBigEmail.setStyle("-fx-font-size: 14px;");
        Button button = new Button();
        button.setPrefWidth(30);
        button.setPrefHeight(30);
        ImageHandler imageHandler = new ImageHandler();
        button.setGraphic(imageHandler.getImageView("src/main/resources/image/info.png", 25));
        button.setOnAction(event -> {
            anchorPaneConsultantDetails.getChildren().clear();
            VBox vBox = new VBox();
            // Consultant info
            Label labelConsultantInfo = new Label("Consultant Info");
            centerLabel(anchorPaneConsultantDetails, labelConsultantInfo);
            labelConsultantInfo.setStyle("-fx-font-size: 20px;");
            vBox.getChildren().add(labelConsultantInfo);
            Label labelEmail = new Label("Email\n\t" + officeAccount.email());
            Label labelFName = new Label("First name\n\t" + officeAccount.firstName());
            Label labelLName = new Label("Last name\n\t" + officeAccount.lastName());
            Label labelPhoneNo = new Label("Phone number\n\t" + officeAccount.phoneNumber());
            vBox.getChildren().addAll(
                    labelEmail,
                    labelFName,
                    labelLName,
                    labelPhoneNo
            );
            // Tasks
            Label labelTasks = new Label("Tasks");
            centerLabel(anchorPaneConsultantDetails, labelTasks);
            labelTasks.setStyle("-fx-font-size: 20px;");
            vBox.getChildren().add(labelTasks);
            officeTasks.forEach(officeTask -> {
                Label labelSeparator = new Label("------------------------------------------------------\n");
                Label labelTaskName = new Label("Task name\n\t" + officeTask.taskName());
                Label labelPriority = new Label("Priority\n\t " + officeTask.priority());
                Label labelTaskDescription = new Label("Task description\n\t " + officeTask.description());
                Label estimatedPomodoros = new Label("Estimated pomodoros\n\t" + officeTask.estimatedPomodoros());
                Label totalPomodoros = new Label("Total pomodoros\n\t" + officeTask.totalPomodoros());
                Label labelTaskStartTime = new Label("Task start time\n\t" + officeTask.startTime());
                Label labelTaskFinishTime = new Label("Task finish time\n\t" + officeTask.finishTime());
                vBox.getChildren().addAll(
                        labelSeparator,
                        labelTaskName,
                        labelPriority,
                        labelTaskDescription,
                        estimatedPomodoros,
                        totalPomodoros,
                        labelTaskStartTime,
                        labelTaskFinishTime
                );
            });
            anchorPaneConsultantDetails.getChildren().add(vBox);
        });
        HBox hBoxDetails = new HBox();
        hBoxDetails.setSpacing(5);
        hBoxDetails.getChildren().addAll(button, labelBigEmail);
        mainVBox.getChildren().addAll(hBoxDetails);
        //endregion
        //region Contents of progress pane
        Pane progressPane = new Pane();
        progressPane.setPrefWidth(vBoxConsultantProgress.getPrefWidth());
        // Task progress
        Thread taskThread = new Thread(() ->
                addTaskSegments(db, officeAccount, rectangleVerticalLine, dateHandler, progressPane));
        // Break progress
        Thread breakThread = new Thread(() ->
                addBreakSegments(db, officeAccount, rectangleVerticalLine, dateHandler, progressPane));
        // Pause progress
        Thread pauseThread = new Thread(() ->
                addPauseSegments(db, officeAccount, rectangleVerticalLine, dateHandler, progressPane));
        try {
            taskThread.start();
            taskThread.join();
            breakThread.start();
            breakThread.join();
            pauseThread.start();
            pauseThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mainVBox.getChildren().add(progressPane);
        this.setPrefWidth(mainVBox.getPrefWidth());
        this.setPrefHeight(mainVBox.getPrefHeight());
        this.getChildren().add(mainVBox);
        Pane statusPane = new Pane();
        statusPane.setPrefWidth(this.getPrefWidth());
        statusPane.setPrefHeight(this.getPrefHeight());
        // Status overlay
        if (db.checkIfConsultantPausedByConsultantEmail(officeAccount.email())) {
            statusPane.setStyle("-fx-background-color: #94fff6");
        } else if (db.checkIfConsultantHasBreakByConsultantEmail(officeAccount.email())) {
            statusPane.setStyle("-fx-background-color: #7cff41");
        } else {
            statusPane.setStyle("-fx-background-color: #ff4141");
        }
        statusPane.setOpacity(0.1);
        statusPane.setDisable(true);
        this.getChildren().add(statusPane);
        //endregion
    }

    /**
     * Adds the pause segments to the progress pane.
     *
     * @param db                    Database handler.
     * @param officeAccount         Office account.
     * @param rectangleVerticalLine Vertical line.
     * @param dateHandler           Date handler.
     * @param progressPane          Progress pane.
     */
    private void addPauseSegments(DatabaseHandlerOffice db, OfficeAccount officeAccount,
                                  Rectangle rectangleVerticalLine, DateHandler dateHandler, Pane progressPane) {
        db.getAllPausesTodayByConsultantEmail(officeAccount.email()).forEach(officePause -> {
            addRectangleToPane(
                    officePause.startTime(),
                    officePause.endTime(),
                    Color.LIGHTBLUE,
                    progressPane,
                    rectangleVerticalLine,
                    dateHandler
            );
            System.out.println("Pause added");
        });
    }

    /**
     * Adds the break segments to the progress pane.
     *
     * @param db                    Database handler.
     * @param officeAccount         Office account.
     * @param rectangleVerticalLine Vertical line.
     * @param dateHandler           Date handler.
     * @param progressPane          Progress pane.
     */
    private void addBreakSegments(DatabaseHandlerOffice db, OfficeAccount officeAccount,
                                  Rectangle rectangleVerticalLine, DateHandler dateHandler, Pane progressPane) {
        db.getAllBreaksTodayByConsultantEmail(officeAccount.email()).forEach(officeBreak -> {
            addRectangleToPane(
                    officeBreak.startTime(),
                    officeBreak.endTime(),
                    Color.GREENYELLOW,
                    progressPane,
                    rectangleVerticalLine,
                    dateHandler
            );
            System.out.println("Break added");
        });
    }

    /**
     * Adds the task segments to the progress pane.
     *
     * @param db                    Database handler.
     * @param officeAccount         Office account.
     * @param rectangleVerticalLine Vertical line.
     * @param dateHandler           Date handler.
     * @param progressPane          Progress pane.
     */
    private void addTaskSegments(DatabaseHandlerOffice db, OfficeAccount officeAccount,
                                 Rectangle rectangleVerticalLine, DateHandler dateHandler, Pane progressPane) {
        db.getAllTasksTodayByConsultantEmail(officeAccount.email()).forEach(officeTask -> {
            addRectangleToPane(
                    officeTask.startTime(),
                    officeTask.finishTime(),
                    Color.RED,
                    progressPane,
                    rectangleVerticalLine,
                    dateHandler
            );
            System.out.println("Task added");
        });
    }

    /**
     * The function takes an AnchorPane and a Label as parameters, sets the Label's width to the AnchorPane's width, and sets the Label's text
     * alignment to center
     *
     * @param anchorPaneConsultantDetails The AnchorPane that contains the label.
     * @param label                       The label to be centered
     */
    private void centerLabel(AnchorPane anchorPaneConsultantDetails, Label label) {
        label.setPrefWidth(anchorPaneConsultantDetails.getPrefWidth());
        label.setAlignment(Pos.CENTER);
    }

    /**
     * Adds a rectangle to a pane with a given color, xPosition and width.
     *
     * @param start                 The start time used to calculate the xPosition of the rectangle.
     * @param end                   The end time used to calculate the width of the rectangle.
     * @param color                 The color of the rectangle.
     * @param progressPane          The pane that the rectangle will be added to.
     * @param rectangleVerticalLine The vertical line that is used to determine the width of the rectangle,
     *                              in case the end-time of the current status is null.
     */
    private void addRectangleToPane(Time start, Time end, Color color, Pane progressPane,
                                    Rectangle rectangleVerticalLine, DateHandler dateHandler) {
        if (start == null) {
            return;
        }
        int xPos = dateHandler.formatTimeToRelativePixelValueInt(
                progressPane, String.valueOf(start)
        );
        int width;
        if (end == null) {
            width = (int) (rectangleVerticalLine.getX() - xPos);
        } else {
            width = dateHandler.formatTimeToRelativePixelValueInt(
                    progressPane, String.valueOf(end)
            ) - xPos;
            // If the rectangle is out of bounds, set it to the bounds
            if (xPos + width > progressPane.getPrefWidth()) {
                width = (int) (progressPane.getPrefWidth() - xPos);
            }
        }
        RectangleHandler rectangleHandler = new RectangleHandler();
        Rectangle rectangle = rectangleHandler.getRectangle(
                xPos,
                0,
                width,
                45,
                color
        );
        System.out.println(start + " " + end);
        System.out.println("xPos: " + xPos + " width: " + width);
        progressPane.getChildren().add(rectangle);
    }
}
