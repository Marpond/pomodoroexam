package office;

import stage.StageLoader;

public class MainForTest extends StageLoader {

    public static void main(String[] args) {
        MainForTest.setFxml("office.fxml");
        launch(args);
    }
}
