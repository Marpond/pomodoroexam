package office.records;

public record OfficeAccount(String email, String firstName, String lastName, String phoneNumber) {
}