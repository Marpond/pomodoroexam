package office.records;

public record OfficePause(java.sql.Time startTime, java.sql.Time endTime) {
}
