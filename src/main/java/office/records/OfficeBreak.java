package office.records;

public record OfficeBreak(java.sql.Time startTime, java.sql.Time endTime) {
}
