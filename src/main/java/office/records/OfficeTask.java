package office.records;

public record OfficeTask(int projectID, String taskName, int toDoListID, String description,
                         int priority, int estimatedPomodoros, int totalPomodoros, java.sql.Time startTime, java.sql.Time finishTime) {
}
