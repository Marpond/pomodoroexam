package office.records;

public record OfficeAccountSettings(int workTime, int shortBreakTime, int longBreakTime, int longBreakInterval) {
}
