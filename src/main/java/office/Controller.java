package office;

import database.DatabaseHandlerOffice;
import date.DateHandler;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;
import office.records.OfficeAccount;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    TextField textFieldStart, textFieldEnd;
    @FXML
    ScrollPane scrollPaneConsultantProgress,
            scrollPaneConsultantDetails;
    @FXML
    AnchorPane anchorPaneConsultantProgress,
            anchorPaneConsultantDetails;
    @FXML
    VBox vBoxConsultantProgress;
    @FXML
    Label labelSelectedOffice,
            labelTextFieldStartIncorrect,
            labelTextFieldEndIncorrect;
    private final int mainTimelineTickRate = 15;
    private final DatabaseHandlerOffice db = DatabaseHandlerOffice.getInstance();
    private final DateHandler dateHandler = new DateHandler(8, 18);
    private final RectangleHandler rectangleHandler = new RectangleHandler();
    private final Text textClock = new Text();
    private final int officeID = db.getOfficeIDByEmail(main.Controller.email);
    private final String[] hours = {"08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00",
            "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00"};
    private final List<Rectangle> hoursRectangles = new ArrayList<>();
    private final List<Label> hoursLabels = new ArrayList<>();
    private Rectangle rectangleVerticalLine;
    private Rectangle rectangleVerticalLineShadow;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        System.out.println("Office ID: " + officeID);

        labelTextFieldStartIncorrect.setVisible(false);
        labelTextFieldEndIncorrect.setVisible(false);
        labelTextFieldStartIncorrect.setStyle("-fx-text-fill: #f32424");
        labelTextFieldEndIncorrect.setStyle("-fx-text-fill: #f32424");

        labelSelectedOffice.setText(db.getOfficeNameByID(officeID));
        // Set values of the start and end text
        textFieldStart.setText(dateHandler.getStartHourString());
        textFieldEnd.setText(dateHandler.getEndHourString());
        anchorPaneConsultantProgress.getChildren().add(textClock);

        // Hide scrollbar of scroll-pane
        scrollPaneConsultantProgress.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPaneConsultantDetails.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPaneConsultantProgress.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPaneConsultantDetails.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        textFieldStart.textProperty().addListener((observable, oldValue, newValue) -> {
            if (validateTextFieldHourEntry(newValue)) {
                dateHandler.setStartHour(dateHandler.formatHHMMToIntHour(newValue));
                textFieldStart.setText(dateHandler.getStartHourString());
                updateHorizontalClock();
                updateVBoxConsultantProgress();
                labelTextFieldStartIncorrect.setVisible(false);
            } else {
                labelTextFieldStartIncorrect.setVisible(true);
            }
        });
        textFieldEnd.textProperty().addListener((observable, oldValue, newValue) -> {
            if (validateTextFieldHourEntry(newValue)) {
                dateHandler.setEndHour(dateHandler.formatHHMMToIntHour(newValue));
                textFieldEnd.setText(dateHandler.getEndHourString());
                updateHorizontalClock();
                updateVBoxConsultantProgress();
                labelTextFieldEndIncorrect.setVisible(false);
            } else {
                labelTextFieldEndIncorrect.setVisible(true);
            }
        });
        setRectangleVerticalLine();
        setRectangleVerticalLineShadow();
        updateHorizontalClock();
        updateVBoxConsultantProgress();
        // Initialize the continuous update of the scene
        startMainTimeline(mainTimelineTickRate);
    }

    /**
     * Creates a vertical line for each hour, for better visualization of the time
     */
    private void setHourSegmentsHorizontalClock() {
        String startHour = dateHandler.getStartHourString();
        String endHour = dateHandler.getEndHourString();
        // For every value in the hours array that is between the start and end hour
        for (String hour : hours) {
            if (hour.compareTo(startHour) >= 0 && hour.compareTo(endHour) <= 0) {
                int xPos = dateHandler.formatTimeToRelativePixelValueInt(anchorPaneConsultantProgress, hour);
                Rectangle rectangle = rectangleHandler.getRectangle(
                        xPos,
                        0,
                        2,
                        anchorPaneConsultantProgress.getPrefHeight(),
                        Color.GRAY
                );
                rectangle.setOpacity(0.25);
                rectangle.setDisable(true);
                hoursRectangles.add(rectangle);
                Label label = new Label(hour);
                label.setLayoutX(xPos);
                label.setLayoutY(-20);
                label.setOpacity(0.25);
                hoursLabels.add(label);
                anchorPaneConsultantProgress.getChildren().addAll(rectangle, label);
            }
        }
    }

    /**
     * The function creates a yellow rectangle that is 5 pixels wide and the same height as the anchor pane
     */
    private void setRectangleVerticalLine() {
        rectangleVerticalLine = rectangleHandler.getRectangle(
                0,
                0,
                5,
                anchorPaneConsultantProgress.getPrefHeight(),
                Color.YELLOW);
        anchorPaneConsultantProgress.getChildren().add(rectangleVerticalLine);
    }


    /**
     * The function sets the rectangleVerticalLineShadow to the rectangleHandler.getRectangle() function, which returns a rectangle with the
     * given parameters
     */
    private void setRectangleVerticalLineShadow() {
        rectangleVerticalLineShadow = rectangleHandler.getRectangle(
                rectangleVerticalLine.getX() + rectangleVerticalLine.getWidth(),
                0,
                anchorPaneConsultantProgress.getPrefWidth() - rectangleVerticalLine.getX(),
                anchorPaneConsultantProgress.getPrefHeight(),
                Color.BLACK
        );
        rectangleVerticalLineShadow.setOpacity(0.25);
        anchorPaneConsultantProgress.getChildren().add(rectangleVerticalLineShadow);
        rectangleVerticalLineShadow.setDisable(true);
    }

    /**
     * Constantly updates the scene
     *
     * @param tickRate The number of seconds between each update
     */
    private void startMainTimeline(int tickRate) {
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(tickRate), event -> {
            System.out.printf("%d seconds passed%n", mainTimelineTickRate);
            updateHorizontalClock();
            updateVBoxConsultantProgress();
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    /**
     * Updates the horizontal clock's position along with it's shadow
     */
    private void updateHorizontalClock() {
        try {
            anchorPaneConsultantProgress.getChildren().removeAll(hoursRectangles);
            anchorPaneConsultantProgress.getChildren().removeAll(hoursLabels);
        } catch (Exception ignored) {
        }
        setHourSegmentsHorizontalClock();
        // Update the horizontal line position
        rectangleVerticalLine.setX(
                dateHandler.formatTimeToRelativePixelValueInt(
                        anchorPaneConsultantProgress,
                        dateHandler.getCurrentHHMM()
                ) - rectangleVerticalLine.getWidth()
        );
        if (rectangleVerticalLine.getX() < 0) {
            rectangleVerticalLine.setX(0);
        }
        // Update the text
        textClock.setText(dateHandler.getCurrentHHMM());
        textClock.setLayoutX(rectangleVerticalLine.getX() + rectangleVerticalLine.getWidth() - 28);
        textClock.setLayoutY(rectangleVerticalLine.getY() - 5);
        textClock.setStyle("-fx-font-size: 20; -fx-font-weight: bold;");
        textClock.setFill(Color.YELLOW);
        // Update the shadow position and size
        rectangleVerticalLineShadow.setWidth(
                anchorPaneConsultantProgress.getPrefWidth() - rectangleVerticalLine.getX()
        );
        rectangleVerticalLineShadow.setX(
                dateHandler.formatTimeToRelativePixelValueInt(
                        anchorPaneConsultantProgress,
                        dateHandler.getCurrentHHMM()
                )
        );
        if (rectangleVerticalLineShadow.getX() < 0) {
            rectangleVerticalLineShadow.setX(0);
        }
    }


    /**
     * Updates the listViewConsultantProgress with the given list of consultants
     */
    private void updateVBoxConsultantProgress() {
        System.out.println("Updating consultant progress");
        vBoxConsultantProgress.getChildren().clear();
        vBoxConsultantProgress.setPrefHeight(0);
        // For each consultant that started working
        List<OfficeAccount> officeAccounts = db.getAllConsultantInfoByOfficeID(officeID);
        officeAccounts.forEach(officeAccount -> {
                    if (db.checkIfConsultantStartedByConsultantEmail(officeAccount.email())) {
                        ProgressSegment progressSegment = new ProgressSegment(
                                db,
                                officeAccount,
                                db.getAccountSettingsByConsultantEmail(officeAccount.email()),
                                db.getAllTasksTodayByConsultantEmail(officeAccount.email()),
                                vBoxConsultantProgress,
                                anchorPaneConsultantDetails,
                                rectangleVerticalLine,
                                dateHandler
                        );
                        vBoxConsultantProgress.getChildren().add(progressSegment);
                        vBoxConsultantProgress.setPrefHeight(
                                vBoxConsultantProgress.getPrefHeight() + progressSegment.getPrefHeight()
                        );
                    }

                }
        );
    }

    /**
     * Validates the text in the textFieldStart and textFieldEnd
     * Validation requirements:
     * - The text must be in the format HH:MM
     * - The text must be a valid time
     *
     * @param entry The text to validate
     * @return True if the text is valid, false otherwise
     */
    private boolean validateTextFieldHourEntry(String entry) {
        boolean isValid = false;
        try {
            int minHour = 8;
            int maxHour = 20;
            if (entry.matches("^(\\d|0\\d|1\\d|2[0-3]):[0-5]\\d$") &&
                    (Integer.parseInt(entry.substring(0, 2)) >= minHour && Integer.parseInt(entry.substring(0, 2)) <= maxHour)) {
                isValid = true;
            }
        } catch (Exception ignored) {
        }
        return isValid;
    }
}
