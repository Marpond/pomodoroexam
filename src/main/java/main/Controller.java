package main;

import database.DatabaseHandlerMain;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import stage.StageSwitcher;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller extends StageSwitcher implements Initializable {

    public static String email;
    @FXML
    TextField textFieldEmail;
    @FXML
    PasswordField passwordField;
    @FXML
    Button buttonLogin;
    @FXML
    CheckBox checkBoxHardCoded;
    @FXML
    Label labelPromptMessage,
            label2FACode;
    private final TwoFactor twoFactor = new TwoFactor();
    private final EmailHandler emailHandler = new EmailHandler();
    DatabaseHandlerMain db = DatabaseHandlerMain.getInstance();
    private boolean sentEmail = false;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // Stuff that changes when the checkbox is checked
        passwordField.visibleProperty().bind(checkBoxHardCoded.selectedProperty().not());
        checkBoxHardCoded.selectedProperty().addListener((observable, oldValue, newValue) -> {
                    buttonLogin.setText(newValue || sentEmail ? "Login" : "Get code");
                    label2FACode.setVisible(!newValue);
                }
        );
        // Login button events
        buttonLogin.setOnAction(event -> {
            // Email validation
            email = textFieldEmail.getText();
            if (db.getAllAccountEmails().contains(email)) {
                if (db.checkIfAccountSettingExistsByEmail(email) &&
                        !db.checkIfAccountIsActivatedByEmail(email)) {
                    labelPromptMessage.setText("Account is not activated");
                    labelPromptMessage.setStyle("-fx-text-fill: #f32424");
                    return;
                }
            } else {
                labelPromptMessage.setText("Email is invalid");
                labelPromptMessage.setStyle("-fx-text-fill: #f32424");
                return;
            }
            // Log in with two factor
            if (!checkBoxHardCoded.isSelected()) {
                // If the email hasn't been sent yet
                if (!sentEmail) {
                    emailHandler.setRecipient(email);
                    emailHandler.send(twoFactor.getCode());
                    sentEmail = true;
                    buttonLogin.setText("Login");
                    labelPromptMessage.setText("Code sent to\n" + email);
                    labelPromptMessage.setStyle("-fx-text-fill: #2fff7b");
                } else {
                    // Check if the entered code is correct
                    if (twoFactor.getCode().equals(passwordField.getText())) {
                        switchToAppropriateScene();
                    } else {
                        labelPromptMessage.setText("Incorrect code");
                        labelPromptMessage.setStyle("-fx-text-fill: #f32424");
                    }
                }
            }
            // Hardcoded login
            else {
                switchToAppropriateScene();
            }
        });
    }

    /**
     * Switches to the appropriate scene based on the email
     */
    private void switchToAppropriateScene() {
        // Get the type ID of the account
        switch (db.getTypeIDByEmail(email)) {
            // Consultant
            case 1 -> {
                // Check if the consultant has a profile
                if (db.checkIfAccountSettingExistsByEmail(email)) {
                    DatabaseHandlerMain.closeConnection();
                    Controller.switchTo("consultant.fxml", consultant.Controller.class);
                } else {
                    DatabaseHandlerMain.closeConnection();
                    Controller.switchTo("consultantSetup.fxml", consultant.ControllerSetup.class);
                }
            }
            // Office
            case 2 -> {
                DatabaseHandlerMain.closeConnection();
                Controller.switchTo("office.fxml", office.Controller.class);
            }
            // Admin
            case 3 -> {
                DatabaseHandlerMain.closeConnection();
                Controller.switchTo("admin.fxml", admin.Controller.class);
            }
            // Default
            default -> {
                labelPromptMessage.setText("Account type is invalid");
                labelPromptMessage.setStyle("-fx-text-fill: #f32424");
            }

        }
    }
}
