package main;

import stage.StageLoader;

public class Main extends StageLoader {
    public static void main(String[] args) {
        Main.setFxml("main.fxml");
        Main.setTitle("Main");
        launch(args);
    }
}
