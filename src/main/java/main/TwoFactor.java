package main;

/**
 * This class is used to generate the two-factor authentication code.
 */
public class TwoFactor {

    private final String code;

    /**
     * Constructor for the class.
     */
    public TwoFactor() {
        code = generateCode();
    }

    public String getCode() {
        return code;
    }

    /**
     * This method generates the code.
     *
     * @return the code
     */
    private String generateCode() {
        int code = (int) (Math.random() * 1000000);
        return "%06d".formatted(code);
    }

}
