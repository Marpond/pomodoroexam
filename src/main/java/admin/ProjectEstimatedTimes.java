package admin;

//Turn this into a record
public class ProjectEstimatedTimes {
    private String name;
    private int estTime;

    public ProjectEstimatedTimes(String name, int estTime){
        this.name = name;
        this.estTime = estTime;
    }

    public String getName() {
        return name;
    }

    public int getEstTime() {
        return estTime;
    }
}
