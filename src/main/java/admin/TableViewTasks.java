package admin;

import admin.records.AdminTask;
import database.DatabaseHandlerAdmin;
import image.ImageHandler;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.util.ArrayList;

public class TableViewTasks {

    private final ImageHandler imageHandler = new ImageHandler();
    private SimpleIntegerProperty projectID;
    private SimpleIntegerProperty taskID; //Only in the overload constructor

    private final SimpleStringProperty projectName;

    private final SimpleStringProperty accountEmail;

    private final SimpleStringProperty reservationDate;
    private final TextField nameTextField;        //The nodes go into the initial constructor
    private final TextField descriptionTextField;
    private final TextField priorityTextField;
    private final TextField estimatedTimeTextField;
    private final Button editButton;
    private final Button saveButton;
    private final Button deleteButton;
    private boolean currentlyEditable = false;
    private ArrayList<TableViewTasks> completeTaskList;

    private final DatabaseHandlerAdmin db = DatabaseHandlerAdmin.getInstance();


    /**
     * @param taskName Name of task
     * @param taskDescription Description of task
     * @param projectName Name of project, which the task belongs to
     * @param priorityLevel Urgency level of task
     * @param accountEmail If reserved, emailID of consultant who reserved it
     * @param estimatedTime Estimated time of completion. Estimated when task is created
     * @param reservationDate Date of reservation by consultant
     * */
    public TableViewTasks(
            String taskName,
            String taskDescription,
            String projectName,
            int priorityLevel,
            String accountEmail,
            int estimatedTime,
            String reservationDate) {

        //Initialise the primitives
        this.projectName = new SimpleStringProperty(projectName);
        this.accountEmail = new SimpleStringProperty(accountEmail);
        this.reservationDate = new SimpleStringProperty(reservationDate);
        //Initialise the nodes
        nameTextField = new TextField(taskName);
        descriptionTextField = new TextField(taskDescription);
        priorityTextField = new TextField(priorityLevel + "");
        estimatedTimeTextField = new TextField(estimatedTime + "");

        //Can not edit the text fields
        nameTextField.setEditable(false);
        descriptionTextField.setEditable(false);
        priorityTextField.setEditable(false);
        estimatedTimeTextField.setEditable(false);

        //Construct butons with icons
        editButton = imageHandler.getButtonWithImageView("src/main/resources/image/edit.png", 20);
        saveButton = imageHandler.getButtonWithImageView("src/main/resources/image/save.png", 20);
        deleteButton = imageHandler.getButtonWithImageView("src/main/resources/image/delete.png", 20);

        //Remove the task from the database and query the db again. The list changes and using the change listener, update the table
        this.deleteButton.setOnMouseClicked(mouseEvent -> {
            db.deleteTaskByTaskID(taskID.get());
            updateTableView();
            updateHashtableOfListOfTaskNames();
        });

        //Set text fields to editable
        this.editButton.setOnMouseClicked(mouseEvent -> {
            changeEditableStatusOfFields();
        });

        //Using the information in the text fields, use an update query to update the database. Update the table view with the new values by querying the database and using the change listener
        this.saveButton.setOnMouseClicked(mouseEvent -> {
            db.updateTask(taskID.get(), nameTextField.getText(), descriptionTextField.getText(), Integer.parseInt(priorityTextField.getText()), Integer.parseInt(estimatedTimeTextField.getText()));
            changeEditableStatusOfFields();
            updateTableView();
        });
    }


    /**
     * @param adminTask input is a record which contains extra information from the other constructor
     * Overloaded constructor. Construct an object with additional information, projectID and taskID
     * */
    public TableViewTasks(AdminTask adminTask) {
        this(adminTask.taskName(), adminTask.taskDescription(), adminTask.projectName(), adminTask.priorityLevel(),
                adminTask.accountEmail(), adminTask.estimatedPomodoros(), adminTask.reservationDate());
        this.projectID = new SimpleIntegerProperty(adminTask.projectID());
        this.taskID = new SimpleIntegerProperty(adminTask.taskID());

    }

    /**
     * Change the status of the editable flag and make all textfields editable/uneditable, depending on what they currently are
     * */
    public void changeEditableStatusOfFields() {
        currentlyEditable = !currentlyEditable;
        nameTextField.setEditable(currentlyEditable);
        descriptionTextField.setEditable(currentlyEditable);
        priorityTextField.setEditable(currentlyEditable);
        estimatedTimeTextField.setEditable(currentlyEditable);
    }

    /**
     * Update the table view by changing the observable list and the change listener of the list updates the table.
     * */
    public void updateTableView() {
        //Update the contents of the table
        ControllerSelectedProject.observableListTableView.clear();
        completeTaskList = db.getAllTasksForTableView(projectID.get());
        ControllerSelectedProject.observableListTableView.setAll(completeTaskList);
    }

    /**
     * Clear the hashset and query the database again.
     * */
    public void updateHashtableOfListOfTaskNames(){
        //Refresh the hashtable
        ControllerSelectedProject.hashSetOfTaskNames.clear();
        completeTaskList.forEach((task) -> ControllerSelectedProject.hashSetOfTaskNames.add(task.getProjectName()));
    }

    public int getTaskID() {
        return taskID.get();
    }

    public SimpleIntegerProperty taskIDProperty() {
        return taskID;
    }

    public String getProjectName() {
        return projectName.get();
    }

    public SimpleStringProperty projectNameProperty() {
        return projectName;
    }

    public String getAccountEmail() {
        return accountEmail.get();
    }

    public SimpleStringProperty accountEmailProperty() {
        return accountEmail;
    }

    public String getReservationDate() {
        return reservationDate.get();
    }

    public SimpleStringProperty reservationDateProperty() {
        return reservationDate;
    }

    public TextField getNameTextField() {
        return nameTextField;
    }

    public TextField getDescriptionTextField() {
        return descriptionTextField;
    }

    public TextField getPriorityTextField() {
        return priorityTextField;
    }

    public TextField getEstimatedTimeTextField() {
        return estimatedTimeTextField;
    }

    public Button getEditButton() {
        return editButton;
    }

    public Button getSaveButton() {
        return saveButton;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }
}
