package admin;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.CheckBox;

public class TableViewPermissions {


    String email;

    int permissionID;
    SimpleIntegerProperty projectID;
    SimpleStringProperty name;
    SimpleBooleanProperty edit;
    SimpleBooleanProperty view;

    CheckBox editCheckBox;
    CheckBox viewCheckBox;

    boolean isUpdated = false;

    //Constructor
    public TableViewPermissions(String email, int permissionID, int projectID, String name, int edit, int view) {

        this.email = email;
        this.permissionID = permissionID;
        this.projectID = new SimpleIntegerProperty(projectID);
        this.name = new SimpleStringProperty(name);


        if (edit == 1) {
            this.edit = new SimpleBooleanProperty(true);
        } else {
            this.edit = new SimpleBooleanProperty(false);
        }
        if (view == 1) {
            this.view = new SimpleBooleanProperty(true);
        } else {
            this.view = new SimpleBooleanProperty(false);
        }

        this.editCheckBox = new CheckBox();
        this.viewCheckBox = new CheckBox();

        //this.editCheckBox.selectedProperty().bindBidirectional(this.edit);
        //this.viewCheckBox.selectedProperty().bindBidirectional(this.view);
        this.editCheckBox.setSelected(this.edit.get());
        this.viewCheckBox.setSelected(this.view.get());

        this.editCheckBox.selectedProperty().addListener(observable -> {
            isUpdated = true;
            if(editCheckBox.isSelected()){
                viewCheckBox.setSelected(true);
                viewCheckBox.setDisable(true);
            }
            else{
                viewCheckBox.setSelected(false);
                viewCheckBox.setDisable(false);
            }
        });
        this.viewCheckBox.selectedProperty().addListener(observable -> isUpdated = true);



    }




    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPermissionID() {
        return permissionID;
    }

    public void setPermissionID(int permissionID) {
        this.permissionID = permissionID;
    }

    public int getProjectID() {
        return projectID.get();
    }

    public SimpleIntegerProperty projectIDProperty() {
        return projectID;
    }

    public void setProjectID(int projectID) {
        this.projectID.set(projectID);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public boolean isEdit() {
        return edit.get();
    }

    public SimpleBooleanProperty editProperty() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit.set(edit);
    }

    public boolean isView() {
        return view.get();
    }

    public SimpleBooleanProperty viewProperty() {
        return view;
    }

    public void setView(boolean view) {
        this.view.set(view);
    }

    public CheckBox getEditCheckBox() {
        return editCheckBox;
    }

    public void setEditCheckBox(CheckBox editCheckBox) {
        this.editCheckBox = editCheckBox;
    }

    public CheckBox getViewCheckBox() {
        return viewCheckBox;
    }

    public void setViewCheckBox(CheckBox viewCheckBox) {
        this.viewCheckBox = viewCheckBox;
    }



}
