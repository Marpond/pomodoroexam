package admin;

import admin.enumerators.AdminSection;
import admin.records.AdminAccountSelected;
import database.DatabaseHandlerAdmin;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import stage.StageSwitcher;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ControllerSelectedAccount extends StageSwitcher implements Initializable{

    public static String email;


    private static final DatabaseHandlerAdmin db = DatabaseHandlerAdmin.getInstance();

    @FXML
    private ComboBox<String> editAccountComboBox, typeIDComboBox, officeIDComboBox;

    @FXML
    private Label selectedAccountTitle;

    @FXML
    VBox vBoxLeftSelectedAccount, vBoxRightSelectedAccount, vBoxCenterSelectedAccount;

    @FXML
    Button buttonSaveDetails, buttonCancelDetails, buttonSavePermissions, buttonCancelPermissions;

    Button buttonBack;

    @FXML
    AnchorPane bottomAnchorPane;


    TableViewAccounts account;

    CheckBox checkBoxView, checkBoxEdit;


    ObservableList<TableViewPermissions> permissionsList = FXCollections.observableArrayList();

    /**
     * Initializes the controller class.
     * @param url
     * The location used to resolve relative paths for the root object, or
     * {@code null} if the location is not known.
     *
     * @param resourceBundle
     * The resources used to localize the root object, or {@code null} if
     * the root object was not localized.
     *
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        selectedAccountTitle.setText(email);

        //set items for top comboBox in selected account
        ObservableList<String> comboBoxOptionsAccounts = FXCollections.observableArrayList("Details", "Permissions");
        editAccountComboBox.setItems(comboBoxOptionsAccounts);

        //show details of selected account on load
        //set combo box to "Details"
        editAccountComboBox.setValue("Details");
        displayAccountDetails();

        //show account details if Details is selected else show permissions
        EventHandler<ActionEvent> comboBoxEventHandler = actionEvent -> {
            String selectedWord = editAccountComboBox.getValue();
            if (selectedWord.equals("Details")) {
                displayAccountDetails();
            }
            else if (selectedWord.equals("Permissions")) {
                displayAccountPermissions();
            }
        };
        editAccountComboBox.setOnAction(comboBoxEventHandler);

    }

    public void setUpLabelsForDetails() {
        //set up labels for details
        Label firstNameLabel = new Label("First Name: ");
        Label lastNameLabel = new Label("Last Name: ");
        Label phoneNumberLabel = new Label("Phone Number: ");
        Label zipCodeLabel = new Label("Zip: ");
        Label addressLabel = new Label("address: ");
        Label typeLabel = new Label("Type ID: ");
        Label officeAddressLabel = new Label("Office ID: ");

        //add all Labels to vboxLeft
        vBoxLeftSelectedAccount.getChildren().addAll(firstNameLabel, lastNameLabel, phoneNumberLabel, zipCodeLabel, addressLabel, typeLabel, officeAddressLabel);

    }

    public void clearPage() {
        //Clear vboxes
        vBoxLeftSelectedAccount.getChildren().clear();
        vBoxCenterSelectedAccount.getChildren().clear();
        vBoxRightSelectedAccount.getChildren().clear();

        //clear bottom anchor pane
        bottomAnchorPane.getChildren().clear();

    }

    public void textFieldsAndButtonsForDetails() {

        //textfields for vboxLeft
        AdminAccountSelected adminAccountSelected = db.getAccountByEmail(email);

        TextField firstNameTextField = new TextField(adminAccountSelected.firstName());
        TextField lastNameTextField = new TextField(adminAccountSelected.lastName());
        TextField phoneNumberTextField = new TextField(adminAccountSelected.phone());
        TextField zipCodeTextField = new TextField(adminAccountSelected.zip());
        TextField addressTextField = new TextField(adminAccountSelected.address());

        //add textfields to vboxLeft
        vBoxCenterSelectedAccount.getChildren().addAll(firstNameTextField, lastNameTextField, phoneNumberTextField, zipCodeTextField, addressTextField);

        //set up comboBoxes for details
        setUpComboBoxesForDetails();

        //add save and cancel buttons to bottom of vboxRight
        buttonSaveDetails = new Button("Save");
        buttonCancelDetails = new Button("Cancel");

        //set up cancel button, restore original values
        buttonCancelDetails.setOnMouseClicked(mouseEvent -> {
            firstNameTextField.setText(adminAccountSelected.firstName());
            lastNameTextField.setText(adminAccountSelected.lastName());
            phoneNumberTextField.setText(adminAccountSelected.phone());
            zipCodeTextField.setText(adminAccountSelected.zip());
            addressTextField.setText(adminAccountSelected.address());
            officeIDComboBox.setValue(Integer.toString(adminAccountSelected.officeID()));
            typeIDComboBox.setValue(Integer.toString(adminAccountSelected.typeID()));
        });


        //set up save button, update values
        buttonSaveDetails.setOnMouseClicked(mouseEvent -> {
            String firstName = firstNameTextField.getText();
            String lastName = lastNameTextField.getText();
            String phone = phoneNumberTextField.getText();
            String zip = zipCodeTextField.getText();
            String address = addressTextField.getText();
            int officeID = Integer.parseInt(officeIDComboBox.getValue());
            int typeID = Integer.parseInt(typeIDComboBox.getValue());

            db.updateAccount(email, firstName, lastName, phone, zip, address, officeID, typeID);
        });


        //Constructs button to go back to Accounts page and adds function
        buttonBack = new Button("Back To Accounts");
        buttonBack.setOnMouseClicked(mouseEvent -> {
            Controller.initialiseType = AdminSection.Accounts;
            returnToHome();
        });

        //add buttons to vBoxCenter
        vBoxCenterSelectedAccount.getChildren().addAll(buttonSaveDetails, buttonCancelDetails);

        //add back button to bottom anchor pane
        AnchorPane.setBottomAnchor(buttonBack, 10.0);
        AnchorPane.setLeftAnchor(buttonBack, 10.0);
        bottomAnchorPane.getChildren().add(buttonBack);

    }

    public void setUpComboBoxesForDetails() {
        //set up comboBoxes for details
        AdminAccountSelected adminAccountSelected = db.getAccountByEmail(email);

        typeIDComboBox = new ComboBox<>();
        typeIDComboBox.setPrefWidth(100);

        //Type ID ComboBox
        List<Integer> listOfTypeIDs;
        listOfTypeIDs = db.getAllTypeIDs();
        listOfTypeIDs.forEach(id -> typeIDComboBox.getItems().add(Integer.toString(id)));


        //ComboBox for officeID
        officeIDComboBox = new ComboBox<>();
        officeIDComboBox.setPrefWidth(100);
        db.getAllOfficeIDs().forEach(id -> officeIDComboBox.getItems().add(Integer.toString(id)));

        //set initial values for comboBoxes
        officeIDComboBox.setValue(Integer.toString(adminAccountSelected.officeID()));
        typeIDComboBox.setValue(Integer.toString(adminAccountSelected.typeID()));


        //add all textFields and comboBoxes to vboxCenter
        vBoxCenterSelectedAccount.getChildren().addAll(typeIDComboBox, officeIDComboBox);

    }





    /**
     * Display account details in the left VBox of the selected account
     */
    public void displayAccountDetails(){
        clearPage();

        //set vbox sizes
        vBoxLeftSelectedAccount.setPrefWidth(200);
        vBoxCenterSelectedAccount.setPrefWidth(100);
        vBoxRightSelectedAccount.setPrefWidth(800);

        //labels for vboxLeft
        setUpLabelsForDetails();

        //text fields for vboxLeft
        textFieldsAndButtonsForDetails();



        //set up spacing and padding for vBoxes
        vBoxLeftSelectedAccount.setSpacing(15);
        vBoxLeftSelectedAccount.setPadding(new Insets(15, 15, 15, 15));
        vBoxCenterSelectedAccount.setSpacing(7);
        vBoxCenterSelectedAccount.setPadding(new Insets(15, 15, 15, 15));


    }

    /**
     * Display account permissions in center vBox of the selected account
     */
    public void displayAccountPermissions(){
        //Clear vBoxes
        vBoxLeftSelectedAccount.getChildren().clear();
        vBoxCenterSelectedAccount.getChildren().clear();
        vBoxRightSelectedAccount.getChildren().clear();

        //clear bottom anchor pane
        bottomAnchorPane.getChildren().clear();

        //set vbox sizes
        vBoxLeftSelectedAccount.setPrefWidth(100);
        vBoxCenterSelectedAccount.setPrefWidth(1000);
        vBoxRightSelectedAccount.setPrefWidth(100);

        //create tableview for permissions
        TableView<TableViewPermissions> tableView = new TableView<>();
        tableView.setPrefWidth(1000);
        tableView.setPrefHeight(500);

        //create columns for tableview
        TableColumn<TableViewPermissions, Integer> tableColumnProjectID = new TableColumn<>("Project ID");
        TableColumn<TableViewPermissions, String> tableColumnName = new TableColumn<>("Project Name");
        TableColumn<TableViewPermissions, CheckBox> tableColumnView = new TableColumn<>("View");
        TableColumn<TableViewPermissions, CheckBox> tableColumnEdit = new TableColumn<>("Edit");

        //add tableView to vboxCenter
        vBoxCenterSelectedAccount.getChildren().add(tableView);

        //construct buttons to bottom of vboxRight
        buttonSavePermissions = new Button("Save");
        buttonCancelPermissions = new Button("Cancel");

        //set up checkboxes for view and edit columns
        checkBoxView = new CheckBox();
        checkBoxEdit = new CheckBox();

        //set layout of buttons
        buttonSavePermissions.setPrefWidth(80);
        buttonCancelPermissions.setPrefWidth(80);

        //set buttons in the "middle" of the bottom anchor pane
        buttonSavePermissions.setLayoutX(bottomAnchorPane.getPrefWidth()/2 - buttonSavePermissions.getPrefWidth()/2 - 40);
        buttonCancelPermissions.setLayoutX(bottomAnchorPane.getPrefWidth()/2 + buttonCancelPermissions.getPrefWidth()/2 );

        bottomAnchorPane.getChildren().addAll(buttonSavePermissions, buttonCancelPermissions);

        //add columns to tableview
        tableView.getColumns().addAll(tableColumnProjectID,tableColumnName, tableColumnView, tableColumnEdit);

        //set factory values for tableview
        tableColumnProjectID.setCellValueFactory(new PropertyValueFactory<>("projectID"));
        tableColumnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableColumnView.setCellValueFactory(new PropertyValueFactory<>("viewCheckBox"));
        tableColumnEdit.setCellValueFactory(new PropertyValueFactory<>("editCheckBox"));


        //set preferred widths for columns to be a third of the tableview
        tableColumnProjectID.setPrefWidth(tableView.getPrefWidth() / 4);
        tableColumnName.setPrefWidth(tableView.getPrefWidth() / 4);
        tableColumnView.setPrefWidth(tableView.getPrefWidth() / 4);
        tableColumnEdit.setPrefWidth(tableView.getPrefWidth() / 4);

        ArrayList<TableViewPermissions> permissionsListFromDB = db.getPermissionsForAccount(email);


        permissionsList.addAll(permissionsListFromDB);
        tableView.setItems(permissionsList);

        //Center the content of the tableview
        tableView.getColumns().forEach(column ->
                column.setStyle("-fx-alignment: CENTER;")
        );

        permissionsList.addListener(new ListChangeListener<TableViewPermissions>() {
            @Override
            public void onChanged(Change<? extends TableViewPermissions> c) {

                tableView.setItems(permissionsList);
            }
        });


        //Constructs button to go back to Accounts page and adds function
        buttonBack = new Button("Back To Accounts");
        buttonBack.setOnMouseClicked(mouseEvent -> {
            Controller.initialiseType = AdminSection.Accounts;
            returnToHome();
        });
        //add back button to bottom anchor pane
        AnchorPane.setBottomAnchor(buttonBack, 10.0);
        AnchorPane.setLeftAnchor(buttonBack, 10.0);
        bottomAnchorPane.getChildren().add(buttonBack);


        buttonSavePermissions.setOnMouseClicked(mouseEvent -> {
            // see if checkboxes are  being checked or unchecked
            // if edit is checked then check view as well
            // if permissionsID is not null update permissions in database
            // else insert new permissions

            permissionsList.forEach(permissions -> {
                if(permissions.isUpdated){

                    if (permissions.getPermissionID() == 0) {
                        System.out.println("inserting");
                        db.insertPermission(permissions.getProjectID(), permissions.getEmail(), permissions.getViewCheckBox().isSelected(), permissions.getEditCheckBox().isSelected());
                        permissions.isUpdated = false;

                    } else {
                        System.out.println("Updating");
                        db.updatePermission(permissions.getPermissionID(), permissions.getViewCheckBox().isSelected(), permissions.getEditCheckBox().isSelected());
                        permissions.isUpdated = false;
                    }
                }
            });

            permissionsList.clear();
            permissionsList.addAll(db.getPermissionsForAccount(email));
        });

        buttonCancelPermissions.setOnMouseClicked(event -> {
            permissionsList.forEach(permission ->{
                if(permission.isUpdated){
                    permission.getEditCheckBox().setSelected(permission.edit.getValue());
                    permission.getViewCheckBox().setSelected(permission.view.getValue());
                    permission.isUpdated = false;
                }
            });
            tableView.setItems(permissionsList);
        });
    }
    public void returnToHome(){
        Controller.initialiseType = AdminSection.Accounts;
        main.Controller.switchTo("admin.fxml", admin.Controller.class);
    }
}





