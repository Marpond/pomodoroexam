package admin;

import admin.PDFGenerators.PDFGenerator;
import admin.PDFGenerators.PdfGeneratorProjects;
import admin.chartGenerators.*;
import admin.enumerators.AdminSection;
import admin.enumerators.ExportType;
import admin.spreadSheetGenerators.GenerateOverdueProjectSpreadSheet;
import admin.spreadSheetGenerators.GenerateOverdueTasksSpreadSheet;
import admin.spreadSheetGenerators.ProjectLengthSpreadSheet;
import admin.spreadSheetGenerators.WorkTimesSpreadSheet;
import database.DatabaseHandlerAdmin;
import image.ImageHandler;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import stage.StageSwitcher;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller extends StageSwitcher implements Initializable {

    private final ImageHandler imageHandler = new ImageHandler();
    @FXML
    ComboBox<String> menuComboBox;
    @FXML
    BorderPane borderPane;

    @FXML
    AnchorPane topAnchorPane, middleAnchorPane, bottomAnchorPane;
    @FXML
    ImageView logoView;

    FlowPane bottomFlowPane;

    //Set the default selection criteria when the page opens
    public static AdminSection initialiseType = AdminSection.Accounts;

    //Dynamically added common nodes
    DatabaseHandlerAdmin db = DatabaseHandlerAdmin.getInstance();
    TextField searchField;
    Button searchButton;
    Button exportProjectButton;

    //ObservableList for Accounts
    public static ObservableList<TableViewAccounts> observableAccountsList;
    //ArrayList for Accounts
    public static ArrayList<TableViewAccounts> queriedAccountsList;
    public static ObservableList<TableViewProjects> observableProjectList; //Static because they are called in the Project class to update the list
    //Change listener updates the table
    public static ArrayList<TableViewProjects> queriedList;


    TableView<TableViewProjects> projectsTableView;
    TableColumn<TableViewProjects, Integer> idColumn;
    TableColumn<TableViewProjects, String> descriptionColumn;
    TableColumn<TableViewProjects, TableColumn<TableViewProjects, Button>> nestedColumnProjects;

    //Account table
    TableColumn<TableViewProjects, String> deadlineColumn;
    TableColumn<TableViewProjects, String> nameColumn;
    TableColumn<TableViewProjects, Button> deleteButtonColumn;
    TableColumn<TableViewProjects, Button> editButtonColumn;
    TableView<TableViewAccounts> accountsTableView;
    TableColumn<TableViewAccounts, String> nameColumnAccounts;
    TableColumn<TableViewAccounts, String> emailColumnAccounts;
    TableColumn<TableViewAccounts, String> phoneColumnAccounts;

    //Statistics table
    TableView<TableViewStatistics> overDueProjectsTableView;
    TableColumn<TableViewStatistics, String> projectNameStatisticsColumn;
    TableColumn<TableViewStatistics, String> projectDescriptionStatisticsColumn;
    TableColumn<TableViewStatistics, String> projectDeadlineStatisticsColumn;
    TableColumn<TableViewStatistics, String> projectStatusStatisticsColumn;
    ArrayList<TableViewStatistics> overdueProjectsArrayList;



    TableColumn<TableViewAccounts, TableColumn<TableViewAccounts, Button>> nestedColumnAccounts;
    TableColumn<TableViewAccounts, String> typeColumnAccounts;
    TableColumn<TableViewAccounts, String> officeAddressColumnAccounts;
    TableColumn<TableViewAccounts, Button> deleteButtonColumnAccounts;
    TableColumn<TableViewAccounts, Button> editButtonColumnAccounts;

    //Dynamically added nodes for project creation
    TextArea inputProjectDescription;
    TextField inputProjectName;
    TextField inputProjectDate;
    Button createProjectButton;

    Button createProjectPopOutButton;//THIS ONE

    public static HashSet<String> hashSetOfExistingProjects;

    //TextFields and button for account creation
    TextField inputAccountEmail;
    TextField inputAccountFirstName;
    TextField inputAccountLastName;
    TextField inputAccountPhone;
    TextField inputAccountZip;
    TextField inputAccountAddress;
    ComboBox<String> inputAccountOffice;
    ComboBox<String> inputAccountType;
    Button createAccountButton;

    //Warning generated if project name exists
    Stage stageAlert;
    boolean projectNameExists;

    Stage createProjectStage;
    VBox createProjectVBox;
    BarChart<String, Number> estimatedTimeChartBarChart;
    BarChart<String, Number> workTimesChartBarChart;
    BarChart<String, Number> efficiencyBarChart;
    StackedBarChart<String, Number> taskStatsStackedChart;
    ArrayList<ProjectEstimatedTimes> projectEstimatedTimesArrayList;
    ArrayList<WorkTimeOfConsultant> workTimeOfConsultantArrayList;
    EstimatedTimeChart estimatedTimeChart;
    EfficiencyChart efficiencyChart;
    WorkTimesChart workTimesChart;

    ExportType exportType;

    /**
     * Method to determine whether a date is valid. uses a Matcher and a pattern.
     */
    public static boolean dateisValid(String date) {
        //Define a weak pattern
        String regex = "[0-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(date);
        //If the weak pattern matches, then we check in more detail
        if (matcher.matches()) {
            int year = Integer.parseInt(date.substring(0, 4));
            int month = Integer.parseInt(date.substring(5, 7));
            int day = Integer.parseInt(date.substring(8, 10));
            return (year >= 1900 && year <= 2050) && (month >= 0 && month <= 12) && (day >= 0 && day <= 31);
        } else {
            return false;
        }
    }

    /**
     * Initialize For the first scene in admin. Set up the DB object and combo box. By default, it shows the accounts.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //First get instance of DB
        observableProjectList = FXCollections.observableArrayList();
        observableAccountsList = FXCollections.observableArrayList();
        logoView.setOnMouseClicked(mouseEvent -> returnToAdminHome());

        //Set up the combo box
        ObservableList<String> menuComboBoxOptions = FXCollections.observableArrayList("Accounts", "Projects", "Statistics");
        menuComboBox.getItems().addAll(menuComboBoxOptions);

        if(initialiseType == AdminSection.Accounts){
            menuComboBox.getSelectionModel().selectFirst();
            selectedAccounts();
        }else if(initialiseType == AdminSection.Projects){
            menuComboBox.getSelectionModel().select(1);
            selectedProjects();
        } else if(initialiseType == AdminSection.Statistics){
            menuComboBox.getSelectionModel().select(2);
            selectedStatistics();
        }
        //Combo box event handler
        menuComboBox.setOnAction(actionEvent -> {
            String selectedOption = menuComboBox.getValue();
            switch (selectedOption) {
                case "Accounts" -> {
                    clearStatisticsNodes();
                    returnAnchorPaneHeightsForSelectionChange();
                    selectedAccounts();
                }
                case "Projects" -> {
                    clearStatisticsNodes();
                    selectedProjects();
                }
                case "Statistics" -> {
                    clearStatisticsNodes();
                    returnAnchorPaneHeightsForSelectionChange();
                    selectedStatistics();
                }
            }
        });



    }

    /**
     * Call this method when projects is selected from the combo box
     * */
    public void selectedProjects() {
        removeNodesForSelectionChange();
        setUpSearchAndTableView("Projects");
        changeAnchorPaneHeightsForProjectSelection();

        //Set up the export button
       exportProjectButton = new Button("Export projects");
       exportProjectButton.setLayoutX(975);
       exportProjectButton.setLayoutY(37);

       topAnchorPane.getChildren().add(exportProjectButton);

       exportProjectButton.setOnMouseClicked(mouseEvent -> {
           PDFGenerator pdfGeneratorProjects = new PdfGeneratorProjects(observableProjectList);
           pdfGeneratorProjects.generateTheReport();
       });


        hashSetOfExistingProjects = new HashSet<>();

        //Set up the lists for the table view
        observableProjectList.clear();
        projectsTableView.setItems(observableProjectList);
        queriedList = db.getAllProjects();
        observableProjectList.addAll(queriedList);

        //Set up the hashset for checking existing projects by names
        queriedList.forEach((project) -> hashSetOfExistingProjects.add(project.getName()));


        //clear bottom anchor pane
        bottomAnchorPane.getChildren().clear();

        observableProjectList.addListener((ListChangeListener<TableViewProjects>) change ->
                projectsTableView.setItems(observableProjectList));

        createAndSetTableColumnsForProjectsTableView();

        //Define the size of the columns
        columnDesignForProjectsTableView();

        //Create a + button at the bottom for a pop out dialog box
        setUpCreateProjectButton();
        positionCreateProjectButton();

        //Populate the dialog box

        //Create a method to close the dialog box

        //Set up bottom AnchorPane

    }



    /**
     * Call this method when the user clicks on the 'create account' button. It uses values from the textfields and then executes a stored procedure
     * */
    public void createAccount() {
        //get the values from the text fields
        String email = inputAccountEmail.getText();
        String firstName = inputAccountFirstName.getText();
        String lastName = inputAccountLastName.getText();
        String phone = inputAccountPhone.getText();
        String zip = inputAccountZip.getText();
        String address = inputAccountAddress.getText();
        int office = Integer.parseInt(inputAccountOffice.getValue());
        int type = Integer.parseInt(inputAccountType.getValue());

        //check if the user has entered all the fields
            if (email.equals("") || firstName.equals("") || lastName.equals("") || phone.equals("") || zip.equals("") || address.equals("") || office == 0 || type == 0) {
                //if not, create a label under the button saying 'please fill in all fields'
                Label errorLabel = new Label("Please fill in all fields");
                errorLabel.setLayoutX(950);
                errorLabel.setLayoutY(50);
                // set the label to red and add it to the bottom anchor pane
                errorLabel.setStyle("-fx-background-color: RED");
                bottomAnchorPane.getChildren().add(errorLabel);
            } else {
                //if so, create the account
                db.insertAccount(email, firstName, lastName, phone, zip, address, office, type);
                //clear the text fields
                inputAccountEmail.clear();
                inputAccountFirstName.clear();
                inputAccountLastName.clear();
                inputAccountPhone.clear();
                inputAccountZip.clear();
                inputAccountAddress.clear();
                inputAccountOffice.setValue("");
                inputAccountType.setValue("");

                //update the table view
                observableAccountsList.clear();
                observableAccountsList.addAll(db.getAccountsForAdminTableView());

        }

    }



    /**
     * Call this method when statistics is chosen from the combo box. It changes the nodes in the scene.
     * A new table view is displayed and the buttons in the bottom flowpane are also different.
     * */
    public void selectedStatistics(){
        returnAnchorPaneHeightsForSelectionChange();
        removeNodesForSelectionChange();



        setUpTableViewColumnsForStatistics();
        changeAnchorPaneHeightsForStatsSelection();
        clearStatisticsNodes();
        SetupStatisticsScene();

    }

    /**
     *
     * @param textFieldPrompt The textfield prompt is set to this value. It has a different value, depending on which option from the combo box was selected.
     * */
    public void setUpSearchAndTableView(String textFieldPrompt){
        //Set up the search button


        //Textfield and search button
        // set up
        searchField = new TextField();
        searchField.setPromptText(textFieldPrompt);
        searchButton = imageHandler.getButtonWithImageView("src/main/resources/search.png", 20);

        searchButton.setOnMouseClicked(mouseEvent -> {
            observableProjectList.clear();

            String[] name = new String[1];
            name[0] = searchField.getText();
            ArrayList<TableViewProjects> searchedList = db.searchProjectsWithAnyParameters(name);
            setObservableProjectList(searchedList);
        });

        searchField.setLayoutX(800);
        searchField.setLayoutY(37);
        searchField.setPrefWidth(100);
        searchField.setPrefHeight(30);

        //Button set up
        searchButton.setLayoutX(915);
        searchButton.setPrefWidth(30);
        searchButton.setPrefHeight(30);
        searchButton.setLayoutY(37);


        //Add them to the anchor-pane
        topAnchorPane.getChildren().addAll(searchButton, searchField);

        //set up the tableview
        projectsTableView = new TableView<>();
        projectsTableView.setLayoutX(50);
        projectsTableView.setPrefWidth(1100);
        projectsTableView.setLayoutY(0);
        projectsTableView.setPrefHeight(440);
        middleAnchorPane.getChildren().add(projectsTableView);

    }

    /**
     * When changing between the options in the combo box, we remove all nodes and only add the relevant ones
     * */
    public void removeNodesForSelectionChange(){
        middleAnchorPane.getChildren().remove(projectsTableView);
        middleAnchorPane.getChildren().remove(accountsTableView);
        middleAnchorPane.getChildren().remove(overDueProjectsTableView);
        topAnchorPane.getChildren().removeAll(exportProjectButton);
        bottomAnchorPane.getChildren().removeAll(inputProjectDescription, inputProjectDate, inputProjectName, createProjectButton, inputAccountEmail, inputAccountFirstName, inputAccountLastName, inputAccountPhone, inputAccountZip, createAccountButton, inputAccountAddress, inputAccountOffice, inputAccountType, createProjectPopOutButton);


    }

    /**
     * @param listFromDB Given an arraylist, set the observable list to that list. It will be displayed in the table view
     * */
    public void setObservableProjectList(ArrayList<TableViewProjects> listFromDB) {
        observableProjectList.addAll(listFromDB);
    }

    public void setObservableAccountsList(ArrayList<TableViewAccounts> ListFromDB) {
        observableAccountsList.addAll(ListFromDB);
    }


    /**
     * When clicking on the logo, call this method to return to the home page of admin. And by default, show accounts
     * */
    public void returnToAdminHome(){
        initialiseType = AdminSection.Accounts;
        Controller.switchTo("admin.fxml", admin.Controller.class);
    }

    public void positionAndSizeCreateNodesForProjectsInBottomAnchorPane(){
        //Describe each node
        inputProjectDescription.setPrefWidth(200);
        inputProjectDescription.setPrefHeight(100);
        inputProjectDescription.setPromptText("Enter the description of the project");
        inputProjectDescription.setLayoutX(50);
        inputProjectDescription.setLayoutY(20);


        inputProjectName.setPrefWidth(150);
        inputProjectName.setPrefHeight(30);
        inputProjectName.setPromptText("Project name");
        inputProjectName.setLayoutX(275);
        inputProjectName.setLayoutY(20);


        inputProjectDate.setPrefWidth(150);
        inputProjectDate.setPrefHeight(30);
        inputProjectDate.setPromptText("yyyy-mm-dd");
        inputProjectDate.setLayoutX(275);
        inputProjectDate.setLayoutY(55);

        createProjectButton.setPrefWidth(150);
        createProjectButton.setPrefHeight(30);
        createProjectButton.setLayoutX(275);
        createProjectButton.setLayoutY(90);
    }


    public void addCreateNodesToBottomPane(){
        //add them to the bottom anchorpan
        bottomAnchorPane.getChildren().addAll(inputProjectDescription, inputProjectDate, inputProjectName, createProjectButton);
    }

    /**
     * The button and two textfields need event handlers. The button to call a method which creates a project in the data base.
     * The two textfields need validators. One shows a warning, and the other will disable the button to prevent project creation
     * */
    public void setUpListenersAndValidatorsForCreateProject(){
        //Functionality for button
        createProjectButton.setOnMouseClicked(mouseEvent -> {

            if(projectNameExists){
                displayAlertBox(inputProjectName.getText().trim());
            }else{
                createTheProject();
                closeCreateProjectDialogBox();
            }
        });

        //Warning if entering an existing project name
        inputProjectName.setOnKeyTyped(keyEvent -> {
            //Get the list of all the currently existing project names
            if(projectNameCheck(inputProjectName.getText().trim(), hashSetOfExistingProjects)){
                inputProjectName.setStyle("-fx-background-color: YELLOW");
                projectNameExists = true;
            }else{
                inputProjectName.setStyle("");
                projectNameExists = false;
            }
        });

        //Date validation
        inputProjectDate.setOnKeyTyped(keyEvent -> {
            String date = inputProjectDate.getText().trim();
            if(!dateisValid(date)){
                inputProjectDate.setStyle("-fx-background-color: RED");
                createProjectButton.setDisable(true);
            }else{
                inputProjectDate.setStyle("");
                createProjectButton.setDisable(false);
            }
            if(date.length() == 0){
                inputProjectDate.setStyle("");
                createProjectButton.setDisable(false);
            }
        });
    }

    /**
     * Static method to check if the project name exists in the database. Static to use it in unit tests
     * */
    public static boolean projectNameCheck(String potentialName, HashSet hashSet){
        return hashSet.contains(potentialName);
    }

    /**
     * when selecting the statistics call this method to fill the statistics with columns and rows
     * pull the data from the database and fill the tableview
     * */
    public void setUpTableViewColumnsForStatistics(){
        //Set up the tableview
        overDueProjectsTableView = new TableView<>();

        //Set up the columns
        projectNameStatisticsColumn = new TableColumn<>("Project Name");
        projectNameStatisticsColumn.setPrefWidth(300);
        projectNameStatisticsColumn.setStyle("-fx-alignment: CENTER;");

        projectDescriptionStatisticsColumn = new TableColumn<>("Project Description");
        projectDescriptionStatisticsColumn.setPrefWidth(300);
        projectDescriptionStatisticsColumn.setStyle("-fx-alignment: CENTER;");

        projectDeadlineStatisticsColumn = new TableColumn<>("Project Deadline");
        projectDeadlineStatisticsColumn.setPrefWidth(300);
        projectDeadlineStatisticsColumn.setStyle("-fx-alignment: CENTER;");

        projectStatusStatisticsColumn = new TableColumn<>("Project Status");
        projectStatusStatisticsColumn.setPrefWidth(300);
        projectStatusStatisticsColumn.setStyle("-fx-alignment: CENTER;");

        //Assign the columns to attributes in the class
        projectNameStatisticsColumn.setCellValueFactory(new PropertyValueFactory<>("projectName"));
        projectDescriptionStatisticsColumn.setCellValueFactory(new PropertyValueFactory<>("projectDescription"));
        projectDeadlineStatisticsColumn.setCellValueFactory(new PropertyValueFactory<>("projectDeadline"));
        projectStatusStatisticsColumn.setCellValueFactory(new PropertyValueFactory<>("projectStatus"));

        overDueProjectsTableView.getColumns().addAll(projectNameStatisticsColumn, projectDescriptionStatisticsColumn, projectDeadlineStatisticsColumn, projectStatusStatisticsColumn);
        ObservableList<TableViewStatistics> overdueProjectsList = FXCollections.observableArrayList();
        overdueProjectsArrayList = db.getAllProjectsStatistics();
        overdueProjectsList.addAll(overdueProjectsArrayList);
        for(TableViewStatistics project : overdueProjectsList){
            if(db.getAllOverDueProjects().contains(project.getProjectID())){
                project.setProjectStatus("Overdue");
            }else{
                project.setProjectStatus("Not Overdue");
            }
        }
        overDueProjectsTableView.setItems(overdueProjectsList);
    }

    /**
     * Construct the columns of the tableview for projects. Also set the columns to the class attributes.
     * */
    public void createAndSetTableColumnsForProjectsTableView(){
        //Construct the columns
        idColumn = new TableColumn<>("ID");
        descriptionColumn = new TableColumn<>("Description");
        deadlineColumn = new TableColumn<>("Deadline");
        nameColumn = new TableColumn<>("Name");
        deleteButtonColumn = new TableColumn<>("Delete");
        editButtonColumn = new TableColumn<>("Edit");
        nestedColumnProjects = new TableColumn<>("Control");
        List<TableColumn<TableViewProjects, ?>> projectColumns = List.of(
                idColumn, descriptionColumn, deadlineColumn, nameColumn
        );
        projectsTableView.getColumns().addAll(projectColumns);
        List<TableColumn<TableViewProjects, Button>> buttonColumns = List.of(editButtonColumn, deleteButtonColumn);
        nestedColumnProjects.getColumns().addAll(buttonColumns);
        projectsTableView.getColumns().add(nestedColumnProjects);

        //Connect the columns to the class.
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        deadlineColumn.setCellValueFactory(new PropertyValueFactory<>("deadline"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        deleteButtonColumn.setCellValueFactory(new PropertyValueFactory<>("deleteButton"));
        editButtonColumn.setCellValueFactory(new PropertyValueFactory<>("editButton"));
    }

    /**
     * Set column widths and CSS for centering Strings inside the columns
     * */
    public void columnDesignForProjectsTableView(){
        //Define the size of the columns
        idColumn.setPrefWidth(50);
        descriptionColumn.setPrefWidth(390);
        deadlineColumn.setPrefWidth(220);
        nameColumn.setPrefWidth(220);
        deleteButtonColumn.setPrefWidth(110);
        editButtonColumn.setPrefWidth(110);
        idColumn.setStyle("-fx-alignment: CENTER;");
        descriptionColumn.setStyle("-fx-alignment: CENTER;");
        deadlineColumn.setStyle("-fx-alignment: CENTER;");
        nameColumn.setStyle("-fx-alignment: CENTER;");
        deleteButtonColumn.setStyle("-fx-alignment: CENTER;");
        editButtonColumn.setStyle("-fx-alignment: CENTER;");
    }

    //Set up the search for account
    public void setUpSearchForAccount(String textFieldPrompt){
        //Constructing the search bar
        searchField = new TextField();
        searchField.setPromptText(textFieldPrompt);
        searchButton = new Button();
        searchButton = imageHandler.getButtonWithImageView("src/main/resources/image/search.png", 20);

        //set the functionality of the search button
        searchButton.setOnMouseClicked(mouseEvent -> {
            observableAccountsList.clear();
            observableAccountsList.addAll(db.searchAccountByString(searchField.getText()));
        });
        //TextField set up
        searchField.setLayoutX(800);
        searchField.setLayoutY(37);
        searchField.setPrefWidth(100);
        searchField.setPrefHeight(30);

        //Button set up
        searchButton.setLayoutX(915);
        searchButton.setPrefWidth(30);
        searchButton.setPrefHeight(30);
        searchButton.setLayoutY(37);


        //Add them to the anchorPane
        topAnchorPane.getChildren().addAll(searchButton, searchField);

    }

    /**
     * Constructing the tableView, the columns and the textFields for creating a new Account.
     * */
    public void selectedAccounts() {
        removeNodesForSelectionChange();

        setUpSearchAndTableView("Accounts"); //Without this line, the code breaks
        setUpSearchForAccount("Accounts");

        //Set up the tableview for accounts
        accountsTableView = new TableView<>();
        accountsTableView.setLayoutX(50);
        accountsTableView.setPrefWidth(1100);
        accountsTableView.setLayoutY(0);
        accountsTableView.setPrefHeight(440);
        middleAnchorPane.getChildren().add(accountsTableView);

        observableAccountsList.clear();
        accountsTableView.setItems(observableAccountsList);
        queriedAccountsList = db.getAccountsForAdminTableView();
        observableAccountsList.addAll(queriedAccountsList);

        observableAccountsList.addListener((ListChangeListener<TableViewAccounts>) change ->
                accountsTableView.setItems(observableAccountsList));

        //Set up bottom AnchorPane
        //Constructors for the bottom anchor pane
        inputAccountEmail = new TextField();
        inputAccountFirstName = new TextField();
        inputAccountLastName = new TextField();
        inputAccountPhone = new TextField();
        inputAccountZip = new TextField();
        inputAccountAddress = new TextField();
        inputAccountOffice = new ComboBox<>();
        inputAccountType = new ComboBox<>();
        createAccountButton = new Button("Create account");

        //calls the method to create an account if button is clicked
        createAccountButton.setOnMouseClicked(mouseEvent ->
                createAccount());


        //add them to the bottom anchor-pane
        bottomAnchorPane.getChildren().addAll(inputAccountEmail, inputAccountFirstName, inputAccountLastName, inputAccountPhone, inputAccountZip, inputAccountAddress, createAccountButton, inputAccountOffice, inputAccountType);

        //set each text field to a certain size and next to each other
        inputAccountEmail.setPrefWidth(150);
        inputAccountEmail.setPrefHeight(30);
        inputAccountEmail.setPromptText("Account email");
        inputAccountEmail.setLayoutX(50);
        inputAccountEmail.setLayoutY(30);

        inputAccountFirstName.setPrefWidth(150);
        inputAccountFirstName.setPrefHeight(30);
        inputAccountFirstName.setPromptText("Fist name");
        inputAccountFirstName.setLayoutX(200);
        inputAccountFirstName.setLayoutY(30);

        inputAccountLastName.setPrefWidth(150);
        inputAccountLastName.setPrefHeight(30);
        inputAccountLastName.setPromptText("Last name");
        inputAccountLastName.setLayoutX(350);
        inputAccountLastName.setLayoutY(30);

        inputAccountPhone.setPrefWidth(150);
        inputAccountPhone.setPrefHeight(30);
        inputAccountPhone.setPromptText("Phone number");
        inputAccountPhone.setLayoutX(500);
        inputAccountPhone.setLayoutY(30);

        inputAccountZip.setPrefWidth(150);
        inputAccountZip.setPrefHeight(30);
        inputAccountZip.setPromptText("Zip code");
        inputAccountZip.setLayoutX(650);
        inputAccountZip.setLayoutY(30);

        inputAccountAddress.setPrefWidth(150);
        inputAccountAddress.setPrefHeight(30);
        inputAccountAddress.setPromptText("address");
        inputAccountAddress.setLayoutX(800);
        inputAccountAddress.setLayoutY(30);

        //set the create button
        createAccountButton.setPrefWidth(150);
        createAccountButton.setPrefHeight(30);
        createAccountButton.setLayoutX(950);
        createAccountButton.setLayoutY(30);

        //set the office and type combo boxes
        inputAccountOffice.setPrefWidth(150);
        inputAccountOffice.setPrefHeight(30);
        inputAccountOffice.setPromptText("Office");
        inputAccountOffice.setLayoutX(50);
        inputAccountOffice.setLayoutY(70);

        //puts the officeID in the combo box
        db.getAllOfficeIDs().forEach(id -> inputAccountOffice.getItems().add(Integer.toString(id)));

        inputAccountType.setPrefWidth(150);
        inputAccountType.setPrefHeight(30);
        inputAccountType.setPromptText("Type");
        inputAccountType.setLayoutX(200);
        inputAccountType.setLayoutY(70);

        //puts the typeID in the combo box
        db.getAllTypeIDs().forEach(id -> inputAccountType.getItems().add(Integer.toString(id)));


        //table column constructors
        nameColumnAccounts = new TableColumn<>("Name");
        emailColumnAccounts = new TableColumn<>("Email");
        phoneColumnAccounts = new TableColumn<>("PhoneNumber");
        typeColumnAccounts = new TableColumn<>("Role");
        officeAddressColumnAccounts = new TableColumn<>("Office address");
        deleteButtonColumnAccounts = new TableColumn<>("Delete");
        editButtonColumnAccounts = new TableColumn<>("Edit");
        nestedColumnAccounts = new TableColumn<>("Edit/Delete");

        //add them to the tableview
        List<TableColumn<TableViewAccounts, ?>> accountColumns = List.of(
                nameColumnAccounts, emailColumnAccounts, phoneColumnAccounts, typeColumnAccounts, officeAddressColumnAccounts
        );
        accountsTableView.getColumns().addAll(accountColumns);
        List<TableColumn<TableViewAccounts, Button>> buttonColumns = List.of(
                editButtonColumnAccounts, deleteButtonColumnAccounts
        );
        nestedColumnAccounts.getColumns().addAll(buttonColumns);
        accountsTableView.getColumns().add(nestedColumnAccounts);

        //Describe each node
        nameColumnAccounts.setCellValueFactory(new PropertyValueFactory<>("name"));
        emailColumnAccounts.setCellValueFactory(new PropertyValueFactory<>("email"));
        phoneColumnAccounts.setCellValueFactory(new PropertyValueFactory<>("phone"));
        typeColumnAccounts.setCellValueFactory(new PropertyValueFactory<>("roleDescription"));
        officeAddressColumnAccounts.setCellValueFactory(new PropertyValueFactory<>("officeAddress"));
        deleteButtonColumnAccounts.setCellValueFactory(new PropertyValueFactory<>("buttonDelete"));
        editButtonColumnAccounts.setCellValueFactory(new PropertyValueFactory<>("buttonEdit"));

        //Define the size of the columns
        nameColumnAccounts.setPrefWidth(190);
        emailColumnAccounts.setPrefWidth(200);
        phoneColumnAccounts.setPrefWidth(150);
        typeColumnAccounts.setPrefWidth(150);
        officeAddressColumnAccounts.setPrefWidth(200);
        deleteButtonColumnAccounts.setPrefWidth(110);
        editButtonColumnAccounts.setPrefWidth(110);
        phoneColumnAccounts.setStyle("-fx-alignment: CENTER;");
        typeColumnAccounts.setStyle("-fx-alignment: CENTER;");
        officeAddressColumnAccounts.setStyle("-fx-alignment: CENTER;");
        deleteButtonColumnAccounts.setStyle("-fx-alignment: CENTER;");
        editButtonColumnAccounts.setStyle("-fx-alignment: CENTER;");


    }

    // CAN WE ADD AN EXCLAMATION MARK TO THE ALERT
    /**
     * This method displays an alert box when the user tries to create a new project with the same name as an existing one.
     * It contains a button which can create a project.
     * @param projectName Displays the alert box with this project name
     * */
    public void displayAlertBox(String projectName) {
        //Set up the window
        stageAlert = new Stage();
        AnchorPane anchorPane = new AnchorPane();
        Scene scene = new Scene(anchorPane);
        anchorPane.setPrefWidth(300);
        anchorPane.setPrefHeight(150);
        stageAlert.setScene(scene);
        stageAlert.setTitle("Warning!");
        stageAlert.show();
        stageAlert.setResizable(false);

        //Set up the Label
        Label labelWarning = new Label("A project with the name, " + projectName + " already exists. Do you wish to create a project anyway?");
        labelWarning.setWrapText(true);
        labelWarning.setLayoutX(30);
        labelWarning.setLayoutY(30);
        labelWarning.setPrefWidth(240);

        //Add the label to the anchorpane
        anchorPane.getChildren().add(labelWarning);

        //Set up a button
        Button buttonyes = new Button("Yes");
        buttonyes.setPrefWidth(50);
        buttonyes.setPrefHeight(20);
        buttonyes.setLayoutX(200);
        buttonyes.setLayoutY(120);

        Button buttonNo = new Button("No");
        buttonNo.setPrefWidth(50);
        buttonNo.setPrefHeight(20);
        buttonNo.setLayoutX(100);
        buttonNo.setLayoutY(120);

        //Add both buttons to the anchorpane
        anchorPane.getChildren().addAll(buttonyes, buttonNo);

        //button's event listener
        buttonyes.setOnMouseClicked(mouseEvent -> {
            createTheProject();
            closeCreateProjectDialogBox();
            closeAlert();
        });

        buttonNo.setOnMouseClicked(mouseEvent ->
                closeAlert());
    }


    /**
     * Closes the alert box
     * */
    public void closeAlert(){
        stageAlert.close();
    }

    /**
     * Call this method to create a project with the values from the text fields.
     * */
    public void createTheProject(){
        //Clear table before adding more items to it
        observableProjectList.clear();

        String description = inputProjectDescription.getText();
        String deadline = inputProjectDate.getText();
        String name = inputProjectName.getText();

        //Refresh the table
        db.insertProject(new TableViewProjects(description, deadline, name));
        queriedList = db.getAllProjects();
        setObservableProjectList(queriedList);

        //Update the hashset
        queriedList.forEach((project) -> hashSetOfExistingProjects.add(project.getName()));

        //Clear the fields
        inputProjectDescription.setText("");
        inputProjectDate.setText("");
        inputProjectName.setText("");
    }

    /**
     * Construct the button to create projects. And add it to the bottom anchorpane
     * */
    public void setUpCreateProjectButton(){
        createProjectPopOutButton = new Button("+");
        bottomAnchorPane.getChildren().add(createProjectPopOutButton);

        createProjectPopOutButton.setOnMouseClicked(mouseEvent -> {
            setUpCreateProjectDialogBox();
            showCreateProjectDialogBox();
        });

    }

    /**
     * Position the button to create projects
     * */
    public void positionCreateProjectButton(){
        AnchorPane.setBottomAnchor(createProjectPopOutButton, 10.0);
        AnchorPane.setRightAnchor(createProjectPopOutButton, 10.0);
    }
    /**
     * Construct the nodes which are needed to create a new project
     * */
    public void constructCreatenodesForProjects(){
        inputProjectDescription = new TextArea();
        inputProjectDescription.setPromptText("Enter Project Description");
        inputProjectDate = new TextField();
        inputProjectDate.setPromptText("Project deadline");
        inputProjectName = new TextField();
        inputProjectName.setPromptText("Project Name");
        createProjectButton = new Button("Create project");
    }

    /**
     * Construct the stage and nodes for the dialog box where a project can be created.
     * */
    public void setUpCreateProjectDialogBox(){
        //Construct the nodes
        constructCreatenodesForProjects();

        //Set the scene of the dialog box
        createProjectStage = new Stage();
        createProjectVBox = new VBox();
        createProjectVBox.setSpacing(30);
        createProjectVBox.setAlignment(Pos.CENTER);
        createProjectVBox.setPadding(new Insets(30,30,30,30));
        Scene scene = new Scene(createProjectVBox, 300, 400);
        createProjectStage.setScene(scene);
        createProjectStage.setTitle("Project Creation");
        createProjectStage .setResizable(false);

        //Add the nodes in the dialog box
        addNodesToCreateProjectVBox();

        //Position the nodes in the dialogue box?

        //Add a listener to the button
        setUpListenersAndValidatorsForCreateProject();
    }

    /**
     * Show the dialog box to create projects
     * */
    public void showCreateProjectDialogBox(){
        createProjectStage.show();
    }

    /**
     * Close the dialog box to create projects
     * */
    public void closeCreateProjectDialogBox(){
        createProjectStage.close();
    }

    /**
     * Add all the nodes to the dialog box to create a project. Includes the buttons and text fields.
     * */
    public void addNodesToCreateProjectVBox(){
        createProjectVBox.getChildren().addAll(inputProjectName, inputProjectDate, inputProjectDescription, createProjectButton);
    }

    /**
     * Resize the anchor panes and tableview heights when selecting projects.
     * */
    public void changeAnchorPaneHeightsForProjectSelection(){
        middleAnchorPane.setPrefHeight(540);
        bottomAnchorPane.setPrefHeight(80);
        projectsTableView.setPrefHeight(540);
        //table view height too
    }

    /**
     * Resize the anchor panes and table view when selecing statistics
     * */
    public void changeAnchorPaneHeightsForStatsSelection(){
        middleAnchorPane.setPrefHeight(540);
        bottomAnchorPane.setPrefHeight(80);
        overDueProjectsTableView.setPrefHeight(540);
    }

    /**
     * Change the anchorpane and table view heights to their original sizes
     * */
    public void returnAnchorPaneHeightsForSelectionChange(){
        middleAnchorPane.setPrefHeight(440);
        bottomAnchorPane.getChildren().remove(bottomFlowPane);
        bottomAnchorPane.setPrefHeight(180);
        projectsTableView.setPrefHeight(440);
    }

    /**
     * Construct the bottom flow pane when selecting statistics. Size and position it too.
     * */
    public void setUpBottomFlowPane(){
        bottomFlowPane = new FlowPane();
        bottomFlowPane.setPrefWidth(1200);
        bottomFlowPane.setPrefHeight(80);
        bottomFlowPane.setLayoutY(0);
        bottomFlowPane.setLayoutX(0);
        bottomAnchorPane.getChildren().add(bottomFlowPane);
        bottomFlowPane.setHgap(10);
        bottomFlowPane.setPadding(new Insets(30, 30, 30, 30));
    }

    /**
     * Construct the button to display work time statistics. Add it to the flowplane and add an event handler
     * */
    public void addEstimatedTimeStatsButton(){

        Button showEstimatedTimeStats = new Button("Projects estimated time");
        bottomFlowPane.getChildren().add(showEstimatedTimeStats);


        showEstimatedTimeStats.setOnMouseClicked(mouseEvent -> {
            exportType = ExportType.ProjectLength;
            //Get data from the db
            //Construct the barchart with that data
            //remove the table from the middle anchor pane
            //Put the bar chart into the anchor pane
            clearStatisticsNodes();
            middleAnchorPane.getChildren().add(estimatedTimeChartBarChart);
        });
    }

    /**
     * Construct a barchart showing project estimated times
     * */
    public void setUpEstimatedTimeChart(){
        projectEstimatedTimesArrayList = db.getAllProjectEstimatedTimes();
        ChartGenerator chartGenerator = new ChartGenerator(new EstimatedTimeChart());
        chartGenerator.executeStrategy();
        estimatedTimeChartBarChart = chartGenerator.getBarChart();
        estimatedTimeChartBarChart.setLayoutX(100);
        estimatedTimeChartBarChart.setPrefHeight(500);
        estimatedTimeChartBarChart.setPrefWidth(1000);
    }

    /**
     * Construct and position the barchart showing work times of consultants
     * */
    public void setupWorkTImeBarChart(){
        //workTimesChart = new WorkTimesChart();
        //set the strategy
        ChartGenerator chartGenerator = new ChartGenerator(new WorkTimesChart());
        //generate the chart
        chartGenerator.executeStrategy();
        //get the chart
        workTimesChartBarChart = chartGenerator.getBarChart();
        workTimesChartBarChart.setLayoutX(100);
        workTimesChartBarChart.setPrefWidth(1000);
        workTimesChartBarChart.setPrefHeight(500);
    }

    /**
     * Construct and position the barchart showing consultant efficiencies
     * */
    public void setUpEfficiencyBarChart(){
        //efficiencyChart = new EfficiencyChart();
        ChartGenerator chartGenerator = new ChartGenerator(new EfficiencyChart());
        chartGenerator.executeStrategy();
        efficiencyBarChart = chartGenerator.getBarChart();
        efficiencyBarChart.setLayoutX(100);
        efficiencyBarChart.setPrefWidth(1000);
        efficiencyBarChart.setPrefHeight(500);
    }

    /**
     * Construct a chart generator object. Set the stratetgy and then exeute the strategy to generate the chart
     * Then get the bar chart from the chart generator class
     * */
    public void setuptasksStackedBarChart(){
        //OverdueTasksChart overdueTasksChart = new OverdueTasksChart();
        //taskStatsStackedChart = overdueTasksChart.getStackedBarChart();
        ChartGenerator chartGenerator = new ChartGenerator(new OverdueTasksChart());
        chartGenerator.executeStrategy();
        taskStatsStackedChart = chartGenerator.getStackedBarChart();

        taskStatsStackedChart.setLayoutX(100);
        taskStatsStackedChart.setPrefWidth(1000);
        taskStatsStackedChart.setPrefHeight(500);
    }

    /**
     * Construct a button to show the work times barchart. Add it to the flowpane and add an event handler
     * */
    public void setUpWorkTimeButton(){

        Button showWorkTimesOfConsultants = new Button("Consultant WorkTimes");
        bottomFlowPane.getChildren().add(showWorkTimesOfConsultants);

        showWorkTimesOfConsultants.setOnMouseClicked(mouseEvent -> {
            exportType = ExportType.WorkTimes;
            clearStatisticsNodes();
            middleAnchorPane.getChildren().add(workTimesChartBarChart);
        });
    }

    /**
     * Construct a button to show consultant efficiencies. Add it to the bottomwflowpane and add an event handler
     * */
    public void setUpEfficiencyButton(){

        Button showEfficiencies = new Button("Consultant efficiency");
        bottomFlowPane.getChildren().add(showEfficiencies);

        showEfficiencies.setOnMouseClicked(mouseEvent -> {
            exportType = ExportType.Efficiency;
            clearStatisticsNodes();
            middleAnchorPane.getChildren().add(efficiencyBarChart);
        });
    }

    /**
     * Construct a button to show overdue statistics. Add it to the flow pane
     * */
    public void setUpTaskStatisticsButton(){
        Button showOverdueTasks = new Button("Overdue tasks");
        bottomFlowPane.getChildren().add(showOverdueTasks);

        showOverdueTasks.setOnMouseClicked(mouseEvent ->{

            exportType = ExportType.OverDueTasks;
            clearStatisticsNodes();
            middleAnchorPane.getChildren().add(taskStatsStackedChart);
        });
    }

    /**
     * Construct a button to export the barchart statistics in a spreadsheet. It will export the barchart which is currently showing.
     * */
    public void setUpExportButton(){
        Button exportSpreadsheetButton = new Button("Export SpreadSheet");
        bottomFlowPane.getChildren().add(exportSpreadsheetButton);

        exportSpreadsheetButton.setOnMouseClicked(mouseEvent -> {
            if(exportType == ExportType.WorkTimes){
                WorkTimesSpreadSheet generateWorkTimesSpreadSheet = new WorkTimesSpreadSheet(4, workTimeOfConsultantArrayList, ExportType.WorkTimes);
                generateWorkTimesSpreadSheet.createWorkBook();
                generateWorkTimesSpreadSheet.printWorkBook();
            }else if(exportType == ExportType.Efficiency){
                WorkTimesSpreadSheet generateWorkTimesSpreadSheet = new WorkTimesSpreadSheet(4, workTimeOfConsultantArrayList, ExportType.Efficiency);
                generateWorkTimesSpreadSheet.createWorkBook();
                generateWorkTimesSpreadSheet.printWorkBook();
            }else if(exportType == ExportType.ProjectLength){
                ProjectLengthSpreadSheet generateProjectLengthSpreadSheet = new ProjectLengthSpreadSheet(projectEstimatedTimesArrayList, 2);
                generateProjectLengthSpreadSheet.createWorkBook();
                generateProjectLengthSpreadSheet.printWorkBook();
            }else if(exportType == ExportType.OverDueProjects){
                GenerateOverdueProjectSpreadSheet generateOverdueProjectsSpreadSheet = new GenerateOverdueProjectSpreadSheet(5, overdueProjectsArrayList);
                generateOverdueProjectsSpreadSheet.createWorkBook();
                generateOverdueProjectsSpreadSheet.printWorkBook();
            }else if(exportType == ExportType.OverDueTasks){
                System.out.println("Oliver generate a spreadsheet here"); //TODO Oliver
                GenerateOverdueTasksSpreadSheet generateOverdueTasksSpreadSheet = new GenerateOverdueTasksSpreadSheet(4, OverdueTasksChart.totalCompletedTasks, OverdueTasksChart.overdueTasks, OverdueTasksChart.notOverdueTasks);
                generateOverdueTasksSpreadSheet.createWorkBook();
                generateOverdueTasksSpreadSheet.printWorkBook();
            }

        });
    }

    /**
     * Set up the bottom flowpane and the barcharts. The barcharts will only actually be added when a button is pressed.
     * */
    public void SetupStatisticsScene(){
        //Loading this scene is taking too long. Use multi threading to speed it up
        Runnable task1 = () ->{
            workTimeOfConsultantArrayList = db.getAllConsultantWorkTimes();
        };
        //set up the flow pane
        setUpBottomFlowPane();

        //Set up the button to show the barchart about project estimated lengths
        addEstimatedTimeStatsButton();

        //Estimated times chart
        Runnable task2 = this::setUpEstimatedTimeChart;

        //Work times chart
        setupWorkTImeBarChart();

        //efficiency chart
        setUpEfficiencyBarChart();

        //Set up the button to show the all times of every consultant
        setUpWorkTimeButton();

        //Set up task statistics button and chart
        setUpTaskStatisticsButton();
        Runnable task6 = this::setuptasksStackedBarChart;
        new Thread(task1).start();
        new Thread(task2).start();
        new Thread(task6).start();

        //OverdueProjects button
        Button showOverDueProjects = new Button("Overdue Projects");
        bottomFlowPane.getChildren().add(showOverDueProjects);

        showOverDueProjects.setOnMouseClicked(mouseEvent -> {
            exportType = ExportType.OverDueProjects;
            clearStatisticsNodes();
            middleAnchorPane.getChildren().add(overDueProjectsTableView);
        });

        //Efficiency button
        setUpEfficiencyButton();

        //Export button
        setUpExportButton();
    }

    /**
     * Whenever a button is pressed, and the barchart needs to change to another one, all the nodes are removed from the middle anchor pane and the required chart is added.
     * */
    public void clearStatisticsNodes(){
        middleAnchorPane.getChildren().remove(overDueProjectsTableView);
        middleAnchorPane.getChildren().remove(estimatedTimeChartBarChart);
        middleAnchorPane.getChildren().remove(workTimesChartBarChart);
        middleAnchorPane.getChildren().remove(efficiencyBarChart);
        middleAnchorPane.getChildren().remove(taskStatsStackedChart);
    }

}
