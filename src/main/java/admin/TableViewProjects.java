package admin;

import admin.records.AdminProject;
import database.DatabaseHandlerAdmin;
import image.ImageHandler;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Button;
import stage.StageSwitcher;

public class TableViewProjects extends StageSwitcher {
    private final ImageHandler imageHandler = new ImageHandler();
    private SimpleIntegerProperty id;
    private final SimpleStringProperty description;
    private final SimpleStringProperty name;
    private final SimpleStringProperty deadline;
    private Button editButton;
    private Button deleteButton;
    final DatabaseHandlerAdmin db = DatabaseHandlerAdmin.getInstance();

    public void setEditButton(Button editButton) {
        this.editButton = editButton;
    }

    public void setDeleteButton(Button deleteButton) {
        this.deleteButton = deleteButton;
    }

    /**
     * @param adminProject Given a project, constructor for a class which is to be used in a table view
     * */
    public TableViewProjects(AdminProject adminProject) {
        this(adminProject.description(), adminProject.deadline(), adminProject.name());
        this.id = new SimpleIntegerProperty(adminProject.id());
    }

    /**
     * @param description Project description
     * @param deadline  Project deadline
     * @param name Project name
     *
     * Overloaded constructor because sometimes an object is constructed with additional information
     * */
    public TableViewProjects(String description, String deadline, String name) {
        this.description = new SimpleStringProperty(description);
        this.name = new SimpleStringProperty(name);
        this.deadline = new SimpleStringProperty(deadline);

        this.deleteButton = imageHandler.getButtonWithImageView("src/main/resources/image/delete.png", 20);
        this.editButton = imageHandler.getButtonWithImageView("src/main/resources/image/edit.png", 20);

        //When the delete button is pressed, delete the item from the database.
        // Query the database again for the whole list.
        //The list then changes and updates the table, using the change listener
        this.deleteButton.setOnMouseClicked(mouseEvent -> {
            Controller.observableProjectList.clear();
            db.deleteProject(id.get() + "");
            Controller.queriedList = db.getAllProjects();
            Controller.observableProjectList.addAll(Controller.queriedList);
        });

        //Scene change when you want to edit a project
        this.editButton.setOnMouseClicked(mouseEvent -> {
            ControllerSelectedProject.projectID = id.get();
            TableViewProjects.switchTo("adminSelectedProject.fxml", ControllerSelectedProject.class);
        });

    }

    public Button getEditButton() {
        return editButton;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getDescription() {
        return description.get();
    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getDeadline() {
        return deadline.get();
    }

    public SimpleStringProperty deadlineProperty() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline.set(deadline);
    }
}
