package admin.chartGenerators;

import admin.TaskStatistics;
import database.DatabaseHandlerAdmin;
import javafx.scene.chart.*;

import java.util.ArrayList;

/**
 * Class to generate a chart for over due tasks
 * */
public class OverdueTasksChart implements StrategyChartGenerator {

    DatabaseHandlerAdmin db = DatabaseHandlerAdmin.getInstance();

    public static ArrayList<TaskStatistics> overdueTasks;
    public static ArrayList<TaskStatistics> notOverdueTasks;
    public static ArrayList<TaskStatistics> totalCompletedTasks;
    CategoryAxis xAxis = new CategoryAxis();
    NumberAxis yAxis = new NumberAxis();
    StackedBarChart<String, Number> tasksStackedBarChart = new StackedBarChart<>(xAxis, yAxis);


    /**
     * Constructor which gets data from the DB and into arraylists
     * */
    public OverdueTasksChart() {
        overdueTasks = db.getAllOverdueTasksForAllConsultants();
        notOverdueTasks = db.getAllNotOverdueTasksForAllConsultants();
        totalCompletedTasks = db.getAllCompletedTasksForAllConsultants();
    }


    /**
     * @return StackedBarChart: returns the stacked bar chart which was generated
     * */
    public StackedBarChart<String, Number> getStackedBarChart() {
        return this.tasksStackedBarChart;
    }

    /**
     * Create the bar chart using the arraylists. Execute the strategy
     * */
    @Override
    public void generateTheChart() {
        tasksStackedBarChart.setTitle("Overdue tasks");
        xAxis.setLabel("Consultants");
        yAxis.setLabel("Completed tasks");

        XYChart.Series overdue = new XYChart.Series<>();
        overdue.setName("Overdue tasks");
        for (int i = 0; i != overdueTasks.size(); i++) {
            overdue.getData().add(new XYChart.Data<>(overdueTasks.get(i).getEmail(), overdueTasks.get(i).getAmountOfTasks()));
        }

        XYChart.Series notOverdue = new XYChart.Series<>();
        notOverdue.setName("Not overdue tasks");
        for (int i = 0; i != notOverdueTasks.size(); i++) {
            notOverdue.getData().add(new XYChart.Data<>(notOverdueTasks.get(i).getEmail(), notOverdueTasks.get(i).getAmountOfTasks()));
        }

        tasksStackedBarChart.getData().add(overdue);
        tasksStackedBarChart.getData().add(notOverdue);
    }

    /**
     * Empty method because of the interface
     * */
    @Override
    public BarChart<String, Number> getBarChart() {
        return null;
    }
}
