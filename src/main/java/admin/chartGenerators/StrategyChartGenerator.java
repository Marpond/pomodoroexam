package admin.chartGenerators;

import javafx.scene.chart.BarChart;
import javafx.scene.chart.StackedBarChart;

/**
 *Interface which is implemented by the different strategies.
 * */
public interface StrategyChartGenerator {
    void generateTheChart();
    BarChart<String, Number> getBarChart();
    StackedBarChart<String, Number> getStackedBarChart();
}
