package admin.chartGenerators;

import admin.WorkTimeOfConsultant;
import database.DatabaseHandlerAdmin;
import javafx.scene.chart.*;

import java.util.ArrayList;

public class WorkTimesChart implements StrategyChartGenerator {
    ArrayList<WorkTimeOfConsultant> listOfWorkTimes;
    CategoryAxis xAxis;
    NumberAxis yAxis;
    BarChart<String, Number> barChart;
    XYChart.Series workTimeSeries;
    XYChart.Series shortBreakSeries;
    XYChart.Series longBreakSeries;
    DatabaseHandlerAdmin db = DatabaseHandlerAdmin.getInstance();


    /**
     *Constructor. Get the list of work times from the database
     * */
    public WorkTimesChart(){
        this.listOfWorkTimes = db.getAllConsultantWorkTimes();

    }


    /**
     * @return BarChart Returns the barchart which was constructed.
     * */
    @Override
    public BarChart<String, Number> getBarChart(){
        return barChart;
    }

    @Override
    public StackedBarChart<String, Number> getStackedBarChart() {
        return null;
    }


    /**
     * Execute the strategy
     * */
    @Override
    public void generateTheChart() {
        System.out.println("Generating the chart");
        //Set up the x axis
        xAxis = new CategoryAxis();
        xAxis.setLabel("Consultant");

        //Set up the y axis
        yAxis = new NumberAxis();
        yAxis.setLabel("Time/Minutes");

        barChart = new BarChart<>(xAxis, yAxis);;
        barChart.setTitle("Consultant work times");

        //Set up each series
        workTimeSeries = new XYChart.Series();
        listOfWorkTimes.forEach((i) -> workTimeSeries.getData().add(new XYChart.Data<>(i.getName(), i.getWorkTime())));
        workTimeSeries.setName("Work time");


        shortBreakSeries = new XYChart.Series();
        listOfWorkTimes.forEach((i)-> shortBreakSeries.getData().add(new XYChart.Data<>(i.getName(), i.getShortBreakTime())));
        shortBreakSeries.setName("Short Break time");

        longBreakSeries = new XYChart.Series();
        listOfWorkTimes.forEach((i)-> longBreakSeries.getData().add(new XYChart.Data<>(i.getName(), i.getLongBreakTime())));
        longBreakSeries.setName("Long Break time");

        //Add them to the barchart
        barChart.getData().addAll(workTimeSeries, shortBreakSeries, longBreakSeries);
    }
}
