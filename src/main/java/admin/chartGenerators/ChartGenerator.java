package admin.chartGenerators;

import javafx.scene.chart.BarChart;
import javafx.scene.chart.StackedBarChart;

/**
 * Construct an object of this class to create a chart
 * */
public class ChartGenerator {
    private StrategyChartGenerator strategy;

    /**
     * @param strategy Sets the strategy here
     * */
    public ChartGenerator(StrategyChartGenerator strategy){
        this.strategy = strategy;
    }

    /**
     * Execute the strategy, which just generates the chart
     * */
    public void executeStrategy(){
        strategy.generateTheChart();
    }

    /**
     * @return barchart: Get the bar chart from the strategy class
     * */
    public BarChart<String, Number> getBarChart(){
        return strategy.getBarChart();
    }

    /**
     * @return StackedBarChart: Get the Stacked bar chart from the strategy class
     * */
    public StackedBarChart<String, Number> getStackedBarChart(){
        return strategy.getStackedBarChart();
    }
}
