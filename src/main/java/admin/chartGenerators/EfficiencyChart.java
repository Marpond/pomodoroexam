package admin.chartGenerators;

import admin.WorkTimeOfConsultant;
import database.DatabaseHandlerAdmin;
import javafx.scene.chart.*;

import java.util.ArrayList;

public class EfficiencyChart implements StrategyChartGenerator {
    ArrayList<WorkTimeOfConsultant> listOfWorkTimes;
    CategoryAxis xAxis;
    NumberAxis yAxis;
    BarChart<String, Number> barChart;
    XYChart.Series efficiencySeries;
    DatabaseHandlerAdmin db = DatabaseHandlerAdmin.getInstance();

    /**
     * Construct the chart showing consultant efficiencies
     *
     * */
    public EfficiencyChart(){
        this.listOfWorkTimes = db.getAllConsultantWorkTimes();
    }

    /**
     * Get the chart, which will be used in the main controller
     * */
    @Override
    public BarChart<String, Number> getBarChart(){
        return barChart;
    }

    /**
     * Empty method which had to be implemented because of interface
     * */
    @Override
    public StackedBarChart<String, Number> getStackedBarChart() {
        return null;
    }


    /**
     * Execute the strategy
     * */
    @Override
    public void generateTheChart() {
        xAxis = new CategoryAxis();
        xAxis.setLabel("Consultants");

        yAxis = new NumberAxis();
        yAxis.setLabel("Efficiencies/ % ");

        barChart = new BarChart<>(xAxis, yAxis);
        barChart.setTitle("Consultant Efficiencies");

        efficiencySeries = new XYChart.Series();
        listOfWorkTimes.forEach((i)-> efficiencySeries.getData().add(new XYChart.Data<>(i.getName(), i.getEfficiency())));
        barChart.setLegendVisible(false);

        barChart.getData().add(efficiencySeries);
    }


}
