package admin.chartGenerators;

import admin.ProjectEstimatedTimes;
import database.DatabaseHandlerAdmin;
import javafx.scene.chart.*;

import java.util.ArrayList;

public class EstimatedTimeChart implements StrategyChartGenerator {
    ArrayList<ProjectEstimatedTimes> listOfEstimatedTimes;
    CategoryAxis xAxis;
    NumberAxis yAxis;
    BarChart<String, Number> barChart;
    XYChart.Series series;
    DatabaseHandlerAdmin db = DatabaseHandlerAdmin.getInstance();

    /**
     * Constructs the barchart using that list
     * */
    public EstimatedTimeChart(){
        this.listOfEstimatedTimes = db.getAllProjectEstimatedTimes();

    }

    /**
     * @return barChart
     * Returns the barchart
     * */
    @Override
    public BarChart<String, Number> getBarChart(){
        return barChart;
    }

    /**
     * Empty method because of the interface
     * */
    @Override
    public StackedBarChart<String, Number> getStackedBarChart() {
        return null;
    }

    /**
     * Execute the strategy
     * */
    @Override
    public void generateTheChart() {
        //Set up the axis
        xAxis = new CategoryAxis();
        xAxis.setLabel("Projects");
        yAxis = new NumberAxis();
        yAxis.setLabel("Estimated time/Pomodoroz");
        //Set up the barchart
        barChart = new BarChart<>(xAxis, yAxis);
        barChart.setTitle("Project length");

        //Add them to the series
        series = new XYChart.Series();
        listOfEstimatedTimes.forEach((i) -> series.getData().add(new XYChart.Data<>(i.getName(), i.getEstTime())));
        barChart.getData().add(series);
        barChart.setLegendVisible(false);
    }
}
