package admin;

import admin.records.AdminAccount;
import database.DatabaseHandlerAdmin;
import image.ImageHandler;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Button;
import stage.StageSwitcher;

public class TableViewAccounts extends StageSwitcher {
    private final ImageHandler imageHandler = new ImageHandler();
    SimpleStringProperty name;
    SimpleStringProperty email;
    SimpleStringProperty phone;
    public Button buttonEdit;
    SimpleStringProperty officeAddress;
    public Button buttonDelete;
    SimpleStringProperty roleDescription;

    DatabaseHandlerAdmin db = DatabaseHandlerAdmin.getInstance();

    /**
     * @param adminAccount Given an account, constructor for a class which is to be used in a table view
     * */
    public TableViewAccounts(AdminAccount adminAccount) {
        this.name = new SimpleStringProperty(adminAccount.firstName() + " " + adminAccount.lastName());
        this.email = new SimpleStringProperty(adminAccount.email());
        this.phone = new SimpleStringProperty(adminAccount.phoneNumber());
        this.roleDescription = new SimpleStringProperty(adminAccount.roleDescription());
        this.officeAddress = new SimpleStringProperty(adminAccount.officeAddress());

        this.buttonDelete = imageHandler.getButtonWithImageView("src/main/resources/image/delete.png", 20);
        this.buttonEdit = imageHandler.getButtonWithImageView("src/main/resources/image/edit.png", 20);

        this.buttonDelete.setOnMouseClicked(mouseEvent -> {
            Controller.observableAccountsList.clear();
            db.deleteAccountByEmail(getEmail() + "");
            Controller.queriedAccountsList = db.getAccountsForAdminTableView();
            Controller.observableAccountsList.addAll(Controller.queriedAccountsList);
        });

        this.buttonEdit.setOnMouseClicked(mouseEvent -> {
            ControllerSelectedAccount.email = getEmail();
            TableViewAccounts.switchTo("adminSelectedAccount.fxml", ControllerSelectedAccount.class);
        });
    }



    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public String getEmail() {
        return email.get();
    }

    public SimpleStringProperty emailProperty() {
        return email;
    }

    public String getPhone() {
        return phone.get();
    }

    public SimpleStringProperty phoneProperty() {
        return phone;
    }

    public String getRoleDescription() {
        return roleDescription.get();
    }

    public SimpleStringProperty roleDescriptionProperty() {
        return roleDescription;
    }

    public String getOfficeAddress() {
        return officeAddress.get();
    }

    public SimpleStringProperty officeAddressProperty() {
        return officeAddress;
    }

    public Button getButtonEdit() {
        return buttonEdit;
    }

    public Button getButtonDelete() {
        return buttonDelete;
    }
}
