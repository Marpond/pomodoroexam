package admin.enumerators;

public enum AdminSection {
    Accounts,
    Projects,
    Statistics
}
