package admin.enumerators;

/**
 * The 5 different statistics which the user can export.
 * */
public enum ExportType {
    ProjectLength,
    WorkTimes,
    Efficiency,
    OverDueProjects,

    OverDueTasks
}
