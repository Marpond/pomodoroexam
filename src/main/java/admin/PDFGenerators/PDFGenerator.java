package admin.PDFGenerators;

/**
 * Interface for all classes which generate PDFs
 * */
public interface PDFGenerator {
    void generateTheReport();
    void addTitle();
    void addDocumentDescription();
    void addEmptyLines(int numOfLines);
}
