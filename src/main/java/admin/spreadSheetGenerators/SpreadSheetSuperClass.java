package admin.spreadSheetGenerators;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;

public abstract class SpreadSheetSuperClass {

    int columnCount;
    XSSFWorkbook workbook;
    XSSFSheet sheet;
    XSSFRow row;
    Cell cell;

    /**
     * @param numOfColumns Number of columns of data.
     * Construct the workbook and sheet.
     * */
    public SpreadSheetSuperClass(int numOfColumns){
        this.columnCount = numOfColumns;
        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("Results");
    }

    public void createWorkBook(){
    }

    /**
     * @param headers Array of headers
     * */
    public void setColumnHeaders(String[] headers){
        row = sheet.createRow(0);
        for (int i = 0; i < headers.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(headers[i]);

        }
    }

    /**
     * Resize all columns because the headers are too long for the cells.
     * */
    public void setAutoResizeColumns(){
        for (int i = 0; i < columnCount; i++) {
            sheet.autoSizeColumn(i);
        }
    }

    /**
     * @param destination given the destination, output the document.
     * */
    public void printWorkBook(String destination){

        try{
            FileOutputStream out = new FileOutputStream(destination);
            workbook.write(out);
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void createChart(){
    }
}
