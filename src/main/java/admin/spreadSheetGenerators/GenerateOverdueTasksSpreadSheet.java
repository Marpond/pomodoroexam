package admin.spreadSheetGenerators;

import admin.TaskStatistics;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.XSSFChart;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class GenerateOverdueTasksSpreadSheet extends SpreadSheetSuperClass{

    ArrayList<TaskStatistics> listOfCompletedTasks;
    ArrayList<TaskStatistics> listOfOverdueTasks;
    ArrayList<TaskStatistics> listOfNotOverdueTasks;

    /**
     * @param numOfColumns Number of columns of data.
     *                     Construct the workbook and sheet.
     */
    public GenerateOverdueTasksSpreadSheet(int numOfColumns,
                                           ArrayList<TaskStatistics> listOfCompletedTasks,
                                           ArrayList<TaskStatistics> listOfOverdueTasks,
                                           ArrayList<TaskStatistics> listOfNotOverdueTasks) {
        super(numOfColumns);
        this.listOfCompletedTasks = listOfCompletedTasks;
        this.listOfOverdueTasks = listOfOverdueTasks;
        this.listOfNotOverdueTasks = listOfNotOverdueTasks;
    }

    public void createWorkBook(){
        System.out.println(listOfOverdueTasks.size());
        System.out.println(listOfNotOverdueTasks.size());
        System.out.println(listOfCompletedTasks.size());
        //Create headers
        String[] headers = {"Email", "Overdue tasks", "Not overdue tasks", "Total tasks"};
        super.setColumnHeaders(headers);
        //Create body
        for (int i = 0; i < listOfCompletedTasks.size(); i++) {
            row = sheet.createRow(i+1);
            cell = row.createCell(0);
            cell.setCellValue(listOfCompletedTasks.get(i).getEmail());
            cell = row.createCell(1);
            cell.setCellValue(listOfOverdueTasks.get(i).getAmountOfTasks());
            cell = row.createCell(2);
            cell.setCellValue(listOfNotOverdueTasks.get(i).getAmountOfTasks());
            cell = row.createCell(3);
            cell.setCellValue(listOfCompletedTasks.get(i).getAmountOfTasks());
        }
        super.setAutoResizeColumns();

        createChart();
    }

    public void printWorkBook(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String todaysDayString = sdf.format(date);

        String output = "src/main/output_Files/overdueTasks" + todaysDayString + ".xlsx";

        //Get an output string and then use the line below to print the workbook
        super.printWorkBook(output);
    }

    public void createChart() {

        // Create the chart
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 5, 5, 25, 35);
        XSSFChart chart = drawing.createChart(anchor);

        //Chart title
        chart.setTitleText("Overdue tasks");
        chart.setTitleOverlay(false);

        //Create each axis
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.BOTTOM);
        bottomAxis.setTitle("Consultants");
        XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
        leftAxis.setTitle("Completed tasks");

        //Define the legends
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(LegendPosition.TOP_RIGHT);

        //Create data sources
        XDDFDataSource<String> emails = XDDFDataSourcesFactory.fromStringCellRange(sheet, new CellRangeAddress(0, listOfCompletedTasks.size(), 0, 0));
        XDDFNumericalDataSource<Double> overdueTasks = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(0, listOfCompletedTasks.size(), 1, 1));
        XDDFNumericalDataSource<Double> notOverdueTasks = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(0, listOfCompletedTasks.size(), 2, 2));
        XDDFNumericalDataSource<Double> completedTasks = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(0, listOfCompletedTasks.size(), 3, 3));

        //Bind the datasources to the data
        XDDFLineChartData data = (XDDFLineChartData) chart.createData(ChartTypes.LINE, bottomAxis, leftAxis);

        //Create a series with that data
        XDDFLineChartData.Series series1 = (XDDFLineChartData.Series)data.addSeries(emails, overdueTasks);
        series1.setTitle("Overdue tasks", null);
        series1.setSmooth(false);
        series1.setMarkerStyle(MarkerStyle.STAR);

        XDDFLineChartData.Series series2 = (XDDFLineChartData.Series)data.addSeries(emails, notOverdueTasks);
        series2.setTitle("Not overdue tasks", null);
        series2.setSmooth(false);
        series2.setMarkerStyle(MarkerStyle.STAR);

        XDDFLineChartData.Series series3 = (XDDFLineChartData.Series)data.addSeries(emails, completedTasks);
        series3.setTitle("Completed tasks", null);
        series3.setSmooth(false);
        series3.setMarkerStyle(MarkerStyle.STAR);

        //Add the plot to the chart
        chart.plot(data);
    }
}
