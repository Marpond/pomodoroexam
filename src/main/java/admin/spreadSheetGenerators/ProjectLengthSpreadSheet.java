package admin.spreadSheetGenerators;

import admin.ProjectEstimatedTimes;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ProjectLengthSpreadSheet extends SpreadSheetSuperClass {
    ArrayList<ProjectEstimatedTimes> listOfProjectTimes;

    /**
     * @param listOfProjectTimes The list which should be added to the spreadsheet
     * @param numofColumns Number of columns of data.
     * Initialises the two input parameters
     * */
    public ProjectLengthSpreadSheet(ArrayList<ProjectEstimatedTimes> listOfProjectTimes, int numofColumns){
        super(numofColumns);
        this.listOfProjectTimes = listOfProjectTimes;
    }

    /**
     * Create the contents of the workbook
     * */
    @Override
    public void createWorkBook(){

        //Column headers
        String[] headers = {"Project Name", "Estimated Time"};
        super.setColumnHeaders(headers);

        //Set column contents
        for (int i = 0; i < listOfProjectTimes.size(); i++) {
            //Starting from the second row
            row = sheet.createRow(i + 1);

            //First column is project names
            cell = row.createCell(0);
            cell.setCellValue(listOfProjectTimes.get(i).getName());

            //Second column is estimated times
            cell = row.createCell(1);
            cell.setCellValue(listOfProjectTimes.get(i).getEstTime());
        }

        //Autosize all columns
        setAutoResizeColumns();
        createChart();
    }


    /**
     * Using the destination, print the workbook and name it using the current date.
     * */
    public void printWorkBook(){

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date today = new Date();
        String dateString = simpleDateFormat.format(today) + "";

        String destination = "src/main/output_Files/estimatedTimes" + dateString + ".xlsx";

        super.printWorkBook(destination);
    }

    /**
     * Using the cells where the list was printed into the work book, create a chart of the data.
     * */
    @Override
    public void createChart(){
        //Create the chart
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 5, 5, 25, 35);
        XSSFChart chart = drawing.createChart(anchor);

        //Chart title
        chart.setTitleText("Project lengths");
        chart.setTitleOverlay(false);

        //XDDFChartLegend legend = chart.getOrAddLegend();
        //legend.setPosition(LegendPosition.TOP_RIGHT);

        //Create each axis
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.BOTTOM);
        bottomAxis.setTitle("Project");
        XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
        leftAxis.setTitle("Length");

        //Create data sources
        XDDFDataSource<String> projects = XDDFDataSourcesFactory.fromStringCellRange(sheet, new CellRangeAddress(0, listOfProjectTimes.size(), 0, 0));
        XDDFNumericalDataSource<Double> estTime = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(0, listOfProjectTimes.size(), 1, 1));

        //Bind the datasources to the data
        XDDFLineChartData data = (XDDFLineChartData) chart.createData(ChartTypes.LINE, bottomAxis, leftAxis);
        //Create a series with that data
        XDDFLineChartData.Series series1 = (XDDFLineChartData.Series)data.addSeries(projects, estTime);
        series1.setTitle("Estimated Times", null);
        series1.setSmooth(false);
        series1.setMarkerStyle(MarkerStyle.STAR);

        //Add the plot to the chart

        chart.plot(data);
    }




}
