package admin.spreadSheetGenerators;

import admin.TableViewStatistics;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class GenerateOverdueProjectSpreadSheet extends SpreadSheetSuperClass {

    ArrayList<TableViewStatistics> overdueProjectsList;



    /**
     * @param numOfColumns Number of columns of data.
     *                     Construct the workbook and sheet.
     */
    public GenerateOverdueProjectSpreadSheet(int numOfColumns, ArrayList<TableViewStatistics> overdueProjectsList) {
        super(numOfColumns);
        this.overdueProjectsList = overdueProjectsList;
    }

    /**
     * Create the contents of the workbook
     * */
    @Override
    public void createWorkBook(){
        String[] headers = {"Project ID", "Project Name", "Project Description", "Project Deadline", "Project Status"};
        setColumnHeaders(headers);

        for (int i = 0; i < overdueProjectsList.size(); i++) {
            row = sheet.createRow(i + 1);
            cell = row.createCell(0);
            cell.setCellValue(overdueProjectsList.get(i).getProjectID());
            cell = row.createCell(1);
            cell.setCellValue(overdueProjectsList.get(i).getProjectName());
            cell = row.createCell(2);
            cell.setCellValue(overdueProjectsList.get(i).getProjectDescription());
            cell = row.createCell(3);
            cell.setCellValue(overdueProjectsList.get(i).getProjectDeadline());
            cell = row.createCell(4);
            cell.setCellValue(overdueProjectsList.get(i).getProjectStatus());
            for (int j = 0; j < columnCount; j++) {
                sheet.autoSizeColumn(j);
            }

        }
    }

    /**
     * Print workbook at a specific output destination
     * */
    public void printWorkBook(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String todaysDayString = sdf.format(date);

        String output = "src/main/output_Files/overdueProjects" + todaysDayString + ".xlsx";
        super.printWorkBook(output);
    }
}
