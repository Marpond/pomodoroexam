package admin.spreadSheetGenerators;


import admin.enumerators.ExportType;
import admin.WorkTimeOfConsultant;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.XSSFChart;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class WorkTimesSpreadSheet extends SpreadSheetSuperClass {
    ExportType exportType;
    ArrayList<WorkTimeOfConsultant> list;

    /**
     * @param numOfColumns Number of columns of data
     * @param list The list which will be represented in the spreadsheet columns
     * @param exportType Determines which type of spreadsheet should be created and using which data.
     * */
    public WorkTimesSpreadSheet(int numOfColumns, ArrayList<WorkTimeOfConsultant> list, ExportType exportType){
        super(numOfColumns);
        this.exportType = exportType;
        this.list = list;
    }



    /**
     * Create the columns in the workbook and the chart
     * */
    @Override
    public void createWorkBook(){
        //Column headers
        String[] headers = {"Cons. Name", "Work Time", "Short Break", "Long Break"};
        super.setColumnHeaders(headers);

        if(exportType == ExportType.Efficiency){
            cell = row.createCell(5);
            cell.setCellValue("Efficiency");
        }

        //Format the headers of each columns?

        //Column's contents
        for (int i = 0; i < list.size(); i++) {
            //Table is made of rows
            row = sheet.createRow(i+1);

            //Row is made of cells

            //First column is names
            cell = row.createCell(0);
            cell.setCellValue(list.get(i).getName());

            //Second column is work times
            cell = row.createCell(1);
            cell.setCellValue(list.get(i).getWorkTime());

            //Third column is short break time
            cell = row.createCell(2);
            cell.setCellValue(list.get(i).getShortBreakTime());

            //Fourth column is long break time
            cell = row.createCell(3);
            cell.setCellValue(list.get(i).getLongBreakTime());

            if(exportType == ExportType.Efficiency){
                cell = row.createCell(5);
                cell.setCellValue(list.get(i).getEfficiency());
            }

        }

        //Autosize all columns
        setAutoResizeColumns();

        //Create the chart
        createChart();
    }


    /**
     * Using the destination and date, create a string to output the workbook.
     * Use the superclass method to output it
     * */

    public void printWorkBook(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date today = new Date();
        String dateString = simpleDateFormat.format(today) + "";

        String type ="Null";
        if(exportType == ExportType.Efficiency){
            type = "efficiencyStats";
        }else if(exportType == ExportType.WorkTimes){
            type = "workTimes";
        }
        String workBookDestination = "src/main/output_Files/" + type  + dateString +".xlsx";
        super.printWorkBook(workBookDestination);
    }

    /**
     * Method to create the chart. It uses the cells of the columns which have data.
     * */
    @Override
    public void createChart(){
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 8, 5, 28, 35);
        XSSFChart chart = drawing.createChart(anchor);

        //Chart title
        chart.setTitleText("Durations");
        chart.setTitleOverlay(false);


        //Create each axis
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.BOTTOM);
        bottomAxis.setTitle("Consultant");
        XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
        leftAxis.setTitle("Duration");
        XDDFLineChartData data = (XDDFLineChartData) chart.createData(ChartTypes.LINE, bottomAxis, leftAxis);

        //common data source
        XDDFDataSource<String> consultants = XDDFDataSourcesFactory.fromStringCellRange(sheet, new CellRangeAddress(0, list.size(), 0, 0));


        if(exportType == ExportType.WorkTimes){
            //Define the legends
            XDDFChartLegend legend = chart.getOrAddLegend();
            legend.setPosition(LegendPosition.TOP_RIGHT);

            //Create data sources

            XDDFNumericalDataSource<Double> workDuration = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(0, list.size(), 1, 1));
            XDDFNumericalDataSource<Double> shortBreakDuration = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(0, list.size(), 2, 2));
            XDDFNumericalDataSource<Double> longBreakDuration = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(0, list.size(), 3, 3));

            //Bind the datasources to the data

            //Create a series with that data
            XDDFLineChartData.Series series1 = (XDDFLineChartData.Series)data.addSeries(consultants, workDuration);
            series1.setTitle("Work Duration", null);
            series1.setSmooth(false);
            series1.setMarkerStyle(MarkerStyle.STAR);

            XDDFLineChartData.Series series2 = (XDDFLineChartData.Series)data.addSeries(consultants, shortBreakDuration);
            series2.setTitle("Short Break Duration", null);
            series2.setSmooth(false);
            series2.setMarkerStyle(MarkerStyle.STAR);

            XDDFLineChartData.Series series3 = (XDDFLineChartData.Series)data.addSeries(consultants, longBreakDuration);
            series3.setTitle("Long Break Duration", null);
            series3.setSmooth(false);
            series3.setMarkerStyle(MarkerStyle.STAR);
        }else if(exportType == ExportType.Efficiency){
            //Data source
            XDDFNumericalDataSource<Double> efficiency = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(0, list.size(), 5, 5));

            //Create a series with that data
            XDDFLineChartData.Series series1 = (XDDFLineChartData.Series)data.addSeries(consultants, efficiency);
            series1.setTitle("Efficiency", null);
            series1.setSmooth(false);
            series1.setMarkerStyle(MarkerStyle.STAR);
        }


        chart.plot(data);
    }
}
