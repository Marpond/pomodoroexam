package admin;

import database.DatabaseHandlerAdmin;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import stage.StageSwitcher;

public class TableViewStatistics extends StageSwitcher {
    //Columns for statistics of projects
    private SimpleIntegerProperty projectId;
    private SimpleStringProperty projectDescription;
    private SimpleStringProperty projectName;
    private SimpleStringProperty projectDeadline;
    private SimpleStringProperty projectStatus;

    final DatabaseHandlerAdmin db = DatabaseHandlerAdmin.getInstance();

    /**
     * @param projectId Project id
     * @param description Project description
     * @param name Project name
     * @param deadline Project deadline
     * @param status Project status
     **/
    public TableViewStatistics(int projectId ,String name, String description,String deadline, String status) {
        this.projectId = new SimpleIntegerProperty(projectId);
        this.projectDescription = new SimpleStringProperty(description);
        this.projectName = new SimpleStringProperty(name);
        this.projectDeadline = new SimpleStringProperty(deadline);
        this.projectStatus = new SimpleStringProperty(status);
    }

    public int getProjectID() {
        return projectId.get();
    }

    public String getProjectDescription() {
        return projectDescription.get();
    }

    public SimpleStringProperty projectDescriptionProperty() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription.set(projectDescription);
    }

    public String getProjectName() {
        return projectName.get();
    }

    public SimpleStringProperty projectNameProperty() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName.set(projectName);
    }

    public String getProjectDeadline() {
        return projectDeadline.get();
    }

    public SimpleStringProperty projectDeadlineProperty() {
        return projectDeadline;
    }

    public void setProjectDeadline(String projectDeadline) {
        this.projectDeadline.set(projectDeadline);
    }

    public String getProjectStatus() {
        return projectStatus.get();
    }

    public SimpleStringProperty projectStatusProperty() {
        return projectStatus;
    }

    public void setProjectStatus(String projectStatus) {
        this.projectStatus.set(projectStatus);
    }

    public DatabaseHandlerAdmin getDb() {
        return db;
    }
}

