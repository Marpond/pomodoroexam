package admin;

public class WorkTimeOfConsultant {
    String email;
    String name;
    double workTime;
    double shortBreakTime;
    double longBreakTime;
    double longBreakInterval;
    double efficiency;

    /**
     * @param email Email of the consultant
     * @param name Name of the consultant
     * @param workTime Pomodoro length
     * @param shortBreakTime short break length
     * @param longBreakTime Long break length
     * @param longBreakInterval Number of pomodoros before the long break
     * Constructor to create an object of this class. Just used in statistics.
     * */
    public WorkTimeOfConsultant(String email,String name, int workTime, int shortBreakTime, int longBreakTime, int longBreakInterval) {
        this.email = email;
        this.name = name;
        this.workTime = workTime;
        this.shortBreakTime = shortBreakTime;
        this.longBreakTime = longBreakTime;
        this.longBreakInterval = longBreakInterval;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getWorkTime() {
        return workTime;
    }

    public void setWorkTime(int workTime) {
        this.workTime = workTime;
    }

    public double getShortBreakTime() {
        return shortBreakTime;
    }

    public void setShortBreakTime(int shortBreakTime) {
        this.shortBreakTime = shortBreakTime;
    }

    public double getLongBreakTime() {
        return longBreakTime;
    }

    public void setLongBreakTime(int longBreakTime) {
        this.longBreakTime = longBreakTime;
    }

    public double getLongBreakInterval() {
        return longBreakInterval;
    }

    public void setLongBreakInterval(int longBreakInterval) {
        this.longBreakInterval = longBreakInterval;
    }

    public static double efficiencyCalculator(double workTime, double shortBreakTime, double longBreakTime, double longBreakInterval){
        double totalWorkTimePerSegment = longBreakInterval*workTime;
        double totalBreakTimePerSegment = (longBreakInterval-1)*shortBreakTime + longBreakTime;
        double efficiency = totalWorkTimePerSegment*100/(totalWorkTimePerSegment + totalBreakTimePerSegment);
        return efficiency;
    }

    /**
     * @return efficiency: Calculate efficiency using work short break, long break, long break interval and work times
     * */
    public double getEfficiency(){
        return efficiencyCalculator(workTime, shortBreakTime, longBreakTime, longBreakInterval);
    }
}
