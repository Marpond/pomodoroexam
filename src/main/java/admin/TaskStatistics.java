package admin;

/**
 * Objects of this class are created when getting data from the database to display in the stacked bar chart
 * */
public class TaskStatistics {

    int amountOfTasks;
    String email;

    public TaskStatistics(int amountOfTasks, String email) {
        this.amountOfTasks = amountOfTasks;
        this.email = email;
    }

    public int getAmountOfTasks() {
        return this.amountOfTasks;
    }

    public String getEmail() {
        return this.email;
    }

    public void setAmountOfTasks(int amountOfTasks) {
        this.amountOfTasks = amountOfTasks;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
