package admin;

import admin.PDFGenerators.PDFGenerator;
import admin.PDFGenerators.PdfGeneratorTasks;
import admin.enumerators.AdminSection;
import database.DatabaseHandlerAdmin;
import image.ImageHandler;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import stage.StageSwitcher;

import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class ControllerSelectedProject extends StageSwitcher implements Initializable {
    public static int projectID;
    private final DatabaseHandlerAdmin db = DatabaseHandlerAdmin.getInstance();
    private final ImageHandler imageHandler = new ImageHandler();
    public static ObservableList<TableViewTasks> observableListTableView;

    public ObservableList<TableViewTasks> observableFilteredlist;


    @FXML
    Label titleHeading; //Font is 35
    @FXML
    ComboBox<String> editProjectComboBox;
    @FXML
    AnchorPane topAnchorPane, bottomAnchorPane;
    @FXML
    VBox vBoxCenter, vBoxLeft, vBoxRight;


    //Dynamically added
    //Search nodes
    TextField searchField;
    ImageView searchButtonIcon;
    Button searchButton;
    ComboBox<String> searchCriteria;
    TextField startDateFiler;
    TextField endDateFilter;
    TableViewProjects project;
    //Display task nodes
    TableView<TableViewTasks> tableView;
    TableColumn<TableViewTasks, Integer> tableColumnTaskID;
    TableColumn<TableViewTasks, TextField> tableColumnTaskName;
    TableColumn<TableViewTasks, TextField> tableColumnTaskDescription;
    TableColumn<TableViewTasks, String> tableColumnProjectName;
    TableColumn<TableViewTasks, TextField> tableColumnPriority;
    TableColumn<TableViewTasks, String> tableColumnEmail;
    TableColumn<TableViewTasks, TextField> tableColumnEstimatedTime;
    TableColumn<TableViewTasks, String> tableColumnReservationDate;
    TableColumn<TableViewTasks, Button> tableColumnEdit;
    TableColumn<TableViewTasks, Button> tableColumnSave;
    TableColumn<TableViewTasks, Button> tableColumnDelete;

    //Create Task nodes:
    TextField entertaskName;
    TextArea enterTaskDescription;
    TextField enterPriorityLevel;
    TextField enterEstimatedTime;
    boolean isValidTaskName = true;

    Button confirmCreation, buttonBack;
    Button createTaskPopOutButton;
    Stage createTaskStage;
    CheckBox showOnlyReservedCheckBox, showOnlyUnReservedCheckBox;
    Button exportToPDF;
    static HashSet<String> hashSetOfTaskNames;

    TableColumn<TableViewTasks, Button> tableColumnNestedButtons;


    /**
     * Initialise the SelectedProjects scene with a combobox and a back button. By default, so project details.
     * */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Set the title so you know which project you are editing
        project = db.getProjectByID(projectID + "");
        titleHeading.setText("Selected Project: " + project.getName());

        ObservableList<String> comboBoxOptions = FXCollections.observableArrayList("Details", "Tasks");
        editProjectComboBox.getItems().addAll(comboBoxOptions);

        EventHandler<ActionEvent> comboBoxEventhandler = actionEvent -> {
            String selection = editProjectComboBox.getValue();
            if (selection.equals("Details")) {
                displayProjectDetails();
            } else if (selection.equals("Tasks")) {
                displayTasksInsideProject();
            }
        };
        editProjectComboBox.setOnAction(comboBoxEventhandler);

        //Define the back button
        buttonBack = new Button("Back To projects");
        buttonBack.setOnMouseClicked(mouseEvent -> {
            backToProjects();
        });


        AnchorPane.setBottomAnchor(buttonBack, 10.0);
        AnchorPane.setLeftAnchor(buttonBack, 10.0);
        bottomAnchorPane.getChildren().add(buttonBack);

        //By default, show details of the task
        displayProjectDetails();
        editProjectComboBox.getSelectionModel().select(0);

        hashSetOfTaskNames = new HashSet<>();

        constructCheckBoxes();
    }

    /**
     * Sets up the whole scene to display tasks in a table view. Adds a table view, the search text fields, filters, buttons and eventhandlers
     * */
    public void displayTasksInsideProject(){

        //Clear the other stuff from the vboxes
        clearVBoxes();

        //Insert search textfield and a button in the top anchor pane

        setUpSearchButtonForTasks();
        setupSearchFieldForTasks();


        //Set up combo box and event handler
        setUpComboBoxForSearchCriteria();

        //Date filters
        setUpDateSearchFiltersForTaskSearch();

        setUpExportButton();
        setUpEventHandlerForExportButton();

        //Set up checkboxes for filters
        addCheckBoxesToTopAnchorPane();
        positionCheckBoxes();
        //reset check boxes
        resetCheckBoxes();


        //Event handlers for the 2 check boxes
        setupEventHandlersForCheckBoxes();

        //Event handler for the search button
        setUpSearchButtonListener();


        //resize the vboxes
        vBoxLeft.setPrefWidth(0);
        vBoxRight.setPrefWidth(0);
        vBoxCenter.setPrefWidth(1200);

        //Height and width are determined by the padding
        vBoxLeft.setPadding(new Insets(0,0,0,0));
        vBoxRight.setPadding(new Insets(0,0,0,0));
        vBoxCenter.setPadding(new Insets(0,50,0,50));


        //Insert the table view into the middle anchor pane
        setUpTableViewForTasks();
        resizePanesForTasks();
        //Set up button to create new tasks

        //Create new project button
        addCreateTaskPopOutButton();
        positionCreateTaskPopOutButton();
        addEventListenerToCreateTaskPopOutButton();

    }

    /**
     * Sets up the scene to display all the details of a project. Shows the name, description and deadline.
     * Allows the user to make changes and save those changes to the database.
     * */
    public void displayProjectDetails(){
        //Remove nodes from previous selection
        removeNodesForSelectionChange();
        resizePanesForDetails();


        //Clear the vboxes

        vBoxRight.setPrefWidth(600);
        vBoxCenter.setPrefWidth(325);
        vBoxLeft.setPrefWidth(225);

        //Nodes for the left vbox
        Label nameIndicator = new Label("Name:");
        Label descriptionIndicator = new Label("Description:");
        Label deadlineIndicator = new Label("Deadline:");
        Button cancelChanges = new Button("Cancel");
        Button saveChanges = new Button("Save");

        //nodes for the right vbox
        TextField enternewName = new TextField(project.getName());
        TextField enterNewDescription = new TextField(project.getDescription());
        TextField enternewDeadline = new TextField(project.getDeadline());


        clearVBoxes();
        //Set up the left vbox
        vBoxLeft.setSpacing(30);
        vBoxLeft.setPadding(new Insets(30, 30, 30, 30));
        vBoxLeft.getChildren().addAll(nameIndicator, descriptionIndicator, deadlineIndicator, cancelChanges);

        //Set up the right vbox
        vBoxCenter.setSpacing(15);
        vBoxCenter.setPadding(new Insets(25, 0, 25, 25));
        vBoxCenter.getChildren().addAll(enternewName, enterNewDescription, enternewDeadline, saveChanges);

        //What happens when you click the cancel button
        cancelChanges.setOnMouseClicked(mouseEvent -> {
            enternewName.setText(project.getName());
            enterNewDescription.setText(project.getDescription());
            enternewDeadline.setText(project.getDeadline());
        });

        saveChanges.setOnMouseClicked(mouseEvent -> {
            String newName = enternewName.getText().trim();
            String newDescription = enterNewDescription.getText().trim();
            String newDeadline = enternewDeadline.getText().trim();
            System.out.println(newDescription + ", " + newDeadline + ", " + newName);


            db.editProject((projectID + ""), newName, newDescription, newDeadline);
            returnToAdminHome();
        });
    }

    /**
     * Sets up the table view. Constructs the observable list, gets data from the database, constructs the columns, positions the columns, connects the columns to the class and adds the columns to the table.
     * */
    public void setUpTableViewForTasks() {
        observableListTableView = FXCollections.observableArrayList();
        tableView = new TableView<>();
        vBoxCenter.getChildren().add(tableView);

        //Size the tableView
        tableView.setPrefHeight(440);

        constructIndividualColumns();


        //Define the width of each column
        sizeAndStyleIndividualColumns();


        constructNestedColumns();

        sizeAndStyleNestedColumns();

        addColumnsToTable();

        connectColumnsToTheClass();

        //Get all items for the observable list
        //set the observable list
        observableListTableView.addAll(db.getAllTasksForTableView(projectID));
        tableView.setItems(observableListTableView);

        //Adding elements to the hashset for the first time
        observableListTableView.forEach((task) -> hashSetOfTaskNames.add(task.getNameTextField().getText()));

        //setUpCreateTasknodes();

        //If the list changes, update the table
        updateTableViewUnFiltered();

    }

    /**
     * Adds a change listener to the table view. We just have to update the observable list which will update the table automatically.
     * */
    public void updateTableViewUnFiltered(){
        observableListTableView.addListener(new ListChangeListener<TableViewTasks>() {
            @Override
            public void onChanged(Change<? extends TableViewTasks> change) {
                //tableView.getItems().clear();
                tableView.setItems(observableListTableView);
            }
        });
    }

    public void setUpCreateTasknodes(){
        //INFO TO ENTER
        /*
        TASK NAME
        TASK DESCRIPTION
        PRIORITY
        ESTIMATED TIME
        */
        //Get info from scene: project ID
        constructCreateTaskNodes();

        bottomAnchorPane.getChildren().addAll(entertaskName, enterTaskDescription, enterPriorityLevel, enterEstimatedTime, confirmCreation);

        enterTaskDescription.setLayoutX(50);
        enterTaskDescription.setLayoutY(10);
        enterTaskDescription.setPrefWidth(200);
        enterTaskDescription.setPrefHeight(110);
        enterTaskDescription.setPromptText("Enter Description");

        entertaskName.setLayoutX(280);
        entertaskName.setLayoutY(10);
        entertaskName.setPrefWidth(100);
        entertaskName.setPrefHeight(20);
        entertaskName.setPromptText("Enter Name");

        enterPriorityLevel.setLayoutX(280);
        enterPriorityLevel.setLayoutY(40);
        enterPriorityLevel.setPrefWidth(100);
        enterPriorityLevel.setPrefHeight(20);
        enterPriorityLevel.setPromptText("Enter priority level");

        enterEstimatedTime.setLayoutX(280);
        enterEstimatedTime.setLayoutY(70);
        enterEstimatedTime.setPrefWidth(100);
        enterEstimatedTime.setPrefHeight(20);
        enterEstimatedTime.setPromptText("Enter est. Time");

        confirmCreation.setLayoutX(280);
        confirmCreation.setLayoutY(100);
        confirmCreation.setPrefWidth(100);
        confirmCreation.setPrefHeight(20);


        setEventListenerForProjectCreateButton();


    }

    /**
     * Switch stage to the admin main page
     * */
    public void returnToAdminHome(){
        main.Controller.switchTo("admin.fxml",admin.Controller.class);
    }

    /**
     * Remove all nodes from top and bottom panes when switching between the tableview for tasks, and displaying details of the project
     * */
    public void removeNodesForSelectionChange(){
        bottomAnchorPane.getChildren().removeAll(entertaskName, enterTaskDescription, enterPriorityLevel, enterEstimatedTime, confirmCreation);
        topAnchorPane.getChildren().removeAll(searchField, searchButton, searchCriteria, startDateFiler, endDateFilter, showOnlyReservedCheckBox, showOnlyUnReservedCheckBox);
    }

    /**
     * Return to the previous scene, showing all the projects
     * */
    public void backToProjects(){
        Controller.initialiseType = AdminSection.Projects;
        returnToAdminHome();
    }

    /**
     * Return to the main page, showing all the accounts
     * */
    public void returnToAdminHomeAccounts(){
        Controller.initialiseType = AdminSection.Accounts;
        returnToAdminHome();
    }

    /**
     * Clear the Vboxes, which are all in the middle section of the border pane. Used when switching between the table view and the project details.
     * */
    public void clearVBoxes(){
        vBoxLeft.getChildren().clear();
        vBoxCenter.getChildren().clear();
        vBoxRight.getChildren().clear();
    }


    /**
     * Construct the search button and position it. Add it to the top anchor pane
     * */
    public void setUpSearchButtonForTasks(){
        searchButton = imageHandler.getButtonWithImageView("src/main/resources/image/search.png", 20);

        searchButton.setLayoutX(915);
        searchButton.setPrefWidth(30);
        searchButton.setPrefHeight(30);
        searchButton.setLayoutY(91);
        topAnchorPane.getChildren().add(searchButton);
    }

    /**
     * Construct the textfield for the search function. Position it and add it to the top anchor pane
     * */
    public void setupSearchFieldForTasks(){
        searchField = new TextField();
        searchField.setLayoutX(800);
        searchField.setLayoutY(91);
        searchField.setPrefWidth(100);
        searchField.setPrefHeight(30);
        searchField.setPromptText("Search tasks");
        topAnchorPane.getChildren().addAll(searchField);
    }

    /**
     * Construct a combo box. Construct an observable list. Add the observable list to the combo box. Position and size the combo box and finally add it to the top anchor pane.
     * */
    public void setUpComboBoxForSearchCriteria(){
        searchCriteria = new ComboBox<>();
        ObservableList<String> searchCriteriaOptions = FXCollections.observableArrayList();
        searchCriteriaOptions.addAll("Name", "Description", "Email");
        searchCriteria.setItems(searchCriteriaOptions);
        searchCriteria.setLayoutX(800);
        searchCriteria.setLayoutY(60);
        searchCriteria.setPrefWidth(120);
        searchCriteria.setPrefHeight(20);
        searchCriteria.setPromptText("Search Criteria");
        searchCriteria.getSelectionModel().selectFirst();
        topAnchorPane.getChildren().add(searchCriteria);
    }

    /**
     * Construct the two text fields which are the start and end date filters for the search period. Position them both and add them to the top anchor pane.
     * */
    public void setUpDateSearchFiltersForTaskSearch(){
        startDateFiler = new TextField();
        startDateFiler.setPromptText("Start time");
        startDateFiler.setLayoutX(800);
        startDateFiler.setLayoutY(130);
        startDateFiler.setPrefWidth(100);
        startDateFiler.setPrefHeight(30);

        endDateFilter = new TextField();
        endDateFilter.setPromptText("End time");
        endDateFilter.setLayoutX(915);
        endDateFilter.setLayoutY(130);
        endDateFilter.setPrefWidth(100);
        endDateFilter.setPrefHeight(30);

        topAnchorPane.getChildren().addAll(startDateFiler, endDateFilter);
    }

    /**
     * Construct the export button and add it to the top anchor pane
     * */
    public void setUpExportButton(){
        exportToPDF = new Button("Export to PDF");
        topAnchorPane.getChildren().add(exportToPDF);
        exportToPDF.setLayoutX(1020);
        exportToPDF.setLayoutY(130);
    }

    /**
     * Add an event handler to the exportToPDF button
     * */
    public void setUpEventHandlerForExportButton(){
        exportToPDF.setOnMouseClicked(mouseEvent -> {
            PDFGenerator pdfGeneratorTasks = new PdfGeneratorTasks(project, observableListTableView);
            pdfGeneratorTasks.generateTheReport();
        });
    }

    /**
     * Add an event handler to the searchButton.
     * There are many cases, of different combinations of inputs.
     * Sometimes the inputs are null, in which case we will call the stored procedure with default values.
     * */
    public void setUpSearchButtonListener(){
        searchButton.setOnMouseClicked(mouseEvent -> {
            String startDateInput;
            String endDateInput;

            //Default time if start time field is empty
            if(startDateFiler.getText().equals("")){
                startDateInput = "1900-01-01";
            }else{
                startDateInput = startDateFiler.getText();
            }

            //Default time if end time field is empty
            if(endDateFilter.getText().equals("")){
                endDateInput = "2050-01-01";
            }else{
                endDateInput = endDateFilter.getText();
            }

            //Set up the array for input parameters for the stored procedure
            String[] parameters = {"", "", "", startDateInput, endDateInput};
            if(searchCriteria.getValue().equals("Name")){
                parameters[0] = searchField.getText();
            }else if(searchCriteria.getValue().equals("Description")){
                parameters[1] = searchField.getText();
            }else if(searchCriteria.getValue().equals("Email")){
                parameters[2] = searchField.getText();
            }
            //Reset the list
            observableListTableView.clear();

            //Refresh the list which changes the table
            observableListTableView.addAll(db.universalSearchForTask(projectID, parameters));

        });
    }

    /**
     * Construct the columns for the table view
     * */
    public void constructIndividualColumns(){
        tableColumnTaskID = new TableColumn<>("TaskID");
        tableColumnTaskName = new TableColumn<>("Task Name");
        tableColumnTaskDescription = new TableColumn<>("Task Description");
        tableColumnProjectName = new TableColumn<>("Project Name");
        tableColumnPriority = new TableColumn<>("Priority");
        tableColumnEmail = new TableColumn<>("Account Email");
        tableColumnEstimatedTime = new TableColumn<>("Est. Time");
        tableColumnReservationDate = new TableColumn<>("Res. Date");
    }

    /**
     * Construct the nested columns
     * */
    public void constructNestedColumns(){
        tableColumnEdit = new TableColumn<>("Edit");
        tableColumnSave = new TableColumn<>("Save");
        tableColumnDelete = new TableColumn<>("Delete");
        tableColumnNestedButtons = new TableColumn<>("Control");
    }

    /**
     * Set the widths of the columns. And set them all to center the content
     * */
    public void sizeAndStyleIndividualColumns(){
        tableColumnTaskID.setPrefWidth(50);
        tableColumnTaskID.setStyle("-fx-alignment: CENTER;");

        tableColumnTaskName.setPrefWidth(100);
        tableColumnTaskID.setStyle("-fx-alignment: CENTER;");

        tableColumnTaskDescription.setPrefWidth(340);
        tableColumnTaskDescription.setStyle("-fx-alignment: CENTER;");

        tableColumnProjectName.setPrefWidth(100);
        tableColumnProjectName.setStyle("-fx-alignment: CENTER;");

        tableColumnPriority.setPrefWidth(50);
        tableColumnPriority.setStyle("-fx-alignment: CENTER;");

        tableColumnEmail.setPrefWidth(150);
        tableColumnEmail.setStyle("-fx-alignment: CENTER;");

        tableColumnEstimatedTime.setPrefWidth(60);
        tableColumnEstimatedTime.setStyle("-fx-alignment: CENTER;");

        tableColumnReservationDate.setPrefWidth(100);
        tableColumnReservationDate.setStyle("-fx-alignment: CENTER;");
    }

    /**
     * Set the widths of the nested columns and center the content
     * */
    public void sizeAndStyleNestedColumns(){
        tableColumnNestedButtons.setPrefWidth(150);

        tableColumnSave.setPrefWidth(50);
        tableColumnSave.setStyle("-fx-alignment: CENTER;");

        tableColumnDelete.setPrefWidth(50);
        tableColumnDelete.setStyle("-fx-alignment: CENTER;");

        tableColumnEdit.setPrefWidth(50);
        tableColumnEdit.setStyle("-fx-alignment: CENTER;");
    }

    /**
     * Add the nested columns to the main column. And then add the main column along with the rest of the columns to the table.
     * */
    public void addColumnsToTable(){
        tableColumnNestedButtons.getColumns().addAll(tableColumnEdit, tableColumnSave, tableColumnDelete);
        tableView.getColumns().addAll(tableColumnTaskID, tableColumnTaskName, tableColumnTaskDescription, tableColumnProjectName, tableColumnPriority, tableColumnEmail, tableColumnEstimatedTime, tableColumnReservationDate, tableColumnNestedButtons);

    }

    /**
     * Connect the table columns with the class attributes.
     * */
    public void connectColumnsToTheClass(){
        tableColumnTaskID.setCellValueFactory(new PropertyValueFactory<>("taskID"));
        tableColumnTaskName.setCellValueFactory(new PropertyValueFactory<>("nameTextField"));
        tableColumnTaskDescription.setCellValueFactory(new PropertyValueFactory<>("descriptionTextField"));
        tableColumnProjectName.setCellValueFactory(new PropertyValueFactory<>("projectName"));
        tableColumnPriority.setCellValueFactory(new PropertyValueFactory<>("priorityTextField"));
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<>("accountEmail"));
        tableColumnEstimatedTime.setCellValueFactory(new PropertyValueFactory<>("estimatedTimeTextField"));
        tableColumnReservationDate.setCellValueFactory(new PropertyValueFactory<>("reservationDate"));
        tableColumnEdit.setCellValueFactory(new PropertyValueFactory<>("editButton"));
        tableColumnSave.setCellValueFactory(new PropertyValueFactory<>("saveButton"));
        tableColumnDelete.setCellValueFactory(new PropertyValueFactory<>("deleteButton"));
    }

    /**
     * Construct all the textfields and buttons needed for a task creation.
     * */
    public void constructCreateTaskNodes(){
        entertaskName = new TextField();
        enterTaskDescription = new TextArea();
        enterPriorityLevel = new TextField();
        enterEstimatedTime = new TextField();
        confirmCreation = new Button("Create Task");
    }

    /**
     * Construct the button which is used to create projects. Add it to the bottom anchor pane
     * */
    public void addCreateTaskPopOutButton(){
        createTaskPopOutButton = new Button("+");
        bottomAnchorPane.getChildren().add(createTaskPopOutButton);
    }

    /**
     * Position the button needed to create projects
     * */
    public void positionCreateTaskPopOutButton(){
        AnchorPane.setRightAnchor(createTaskPopOutButton, 10.0);
        AnchorPane.setBottomAnchor(createTaskPopOutButton, 10.0);
    }

    /**
     * Add an event handler to the button to create a project. Clicking it will create a new stage to enter the details of the new task
     * */
    public void addEventListenerToCreateTaskPopOutButton(){
        createTaskPopOutButton.setOnMouseClicked(mouseEvent -> {
            setUpCreateTaskPopOutStage();
            showCreateTaskPopOutStage();
        });
    }

    /**
     *Set the prompt text of all the text fields used to crate a new task.
     * */
    public void setPromptTextsOfCreateTextFields(){
        entertaskName.setPromptText("Enter Task Name");
        enterTaskDescription.setPromptText("Enter Task Description");
        enterPriorityLevel.setPromptText("Enter Priority Level");
        enterEstimatedTime.setPromptText("Est. Time/minutes");
    }

    /**
     * Construct and set up the stage which is used to create a new project.
     * Add the text fields and buttons to it.
     * */
    public void setUpCreateTaskPopOutStage(){

        //Setting the stage and scene
        createTaskStage = new Stage();
        VBox vBox = new VBox();
        Scene scene = new Scene(vBox, 300, 400);
        createTaskStage.setScene(scene);
        createTaskStage.setTitle("Create Project");
        createTaskStage.setResizable(false);

        //Construct the project creation nodes
        constructCreateTaskNodes();

        //Prompt messages
        setPromptTextsOfCreateTextFields();

        //Add these nodes to the vbox
        vBox.getChildren().addAll(entertaskName,  enterPriorityLevel, enterEstimatedTime, enterTaskDescription, confirmCreation);

        //Spacing and padding
        vBox.setPadding(new Insets(30, 30, 30, 30));
        vBox.setSpacing(30);


        //Event listener for buttons and validation listeners for the labels
        setEventListenerForProjectCreateButton();

        setUpTaskNameValidator();
    }


    /**
     * Show the create project stage.
     * */
    public void showCreateTaskPopOutStage(){
        createTaskStage.show();
    }

    /**
     * Close the create project stage.
     * */
    public void closeCreateTaskPopOutStage(){
        createTaskStage.close();
    }

    /**
     * Add an event handler to the button to create projects. It accepts text from the text fields and executes a stored procedure to add the new project to the database.
     * It also adds the name to a hashset to prevent any more project to be created with the same name
     * */
    public void setEventListenerForProjectCreateButton(){
        confirmCreation.setOnMouseClicked(mouseEvent -> {
            int projID = projectID;
            String taskName = entertaskName.getText();
            String description = enterTaskDescription.getText();
            int priority = Integer.parseInt(enterPriorityLevel.getText());
            int estTime = Integer.parseInt(enterEstimatedTime.getText());
            db.createTaskInProject(projID, taskName, description, priority, estTime);
            observableListTableView.clear();
            observableListTableView.addAll(db.getAllTasksForTableView(projectID));
            hashSetOfTaskNames.add(taskName);
            closeCreateTaskPopOutStage();
        });
    }

    /**
     * Checks if a project with the name in the text field can be created. Uses a hashet for O(1) lookups.
     * If it is indeed contained in the hashset, the create button is disabled.
     * */
    public void setUpTaskNameValidator(){
        entertaskName.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                if(hashSetOfTaskNames.contains(entertaskName.getText())){
                    entertaskName.setStyle("-fx-background-color: RED");
                    confirmCreation.setDisable(true);
                }else{
                    entertaskName.setStyle("");
                    confirmCreation.setDisable(false);
                }
            }
        });
    }

    /**
     * When switching between details and tasks, resize the bottom anchor pane and center vbox in the center borderpane
     * */
    public void resizePanesForTasks(){
        vBoxCenter.setPrefHeight(540);
        bottomAnchorPane.setPrefHeight(80);
        tableView.setPrefHeight(540);
    }

    /**
     * Return the sizes back to normal when switching to details
     * */
    public void resizePanesForDetails(){
        vBoxCenter.setPrefHeight(440);
        bottomAnchorPane.setPrefHeight(180);

    }

    /**
     * Construct the checkboxes to show tasks filtered by reservations
     * */
    public void constructCheckBoxes(){
        showOnlyReservedCheckBox = new CheckBox("Filter reserved tasks");
        showOnlyUnReservedCheckBox = new CheckBox("Filter unreserved tasks");
    }

    /**
     * Add those check boxes to the anchor pane
     * */
    public void addCheckBoxesToTopAnchorPane(){
        topAnchorPane.getChildren().addAll(showOnlyReservedCheckBox, showOnlyUnReservedCheckBox);
    }

    /**
     * Position both checkboxes.
     * */
    public void positionCheckBoxes(){
        showOnlyUnReservedCheckBox.setLayoutX(1000);
        showOnlyUnReservedCheckBox.setLayoutY(60);
        showOnlyReservedCheckBox.setLayoutX(1000);
        showOnlyReservedCheckBox.setLayoutY(91);
    }

    /**
     * Add event listeners to the check boxes. Only 1 checkbox can be checked at any time. Also, both can be unchecked.
     * Using list.Stream().filter() to filter through the contents of the observable list
     * */
    public void setupEventHandlersForCheckBoxes(){ //Possible bindings here
        showOnlyReservedCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                if(showOnlyReservedCheckBox.isSelected()){
                    showOnlyUnReservedCheckBox.setSelected(false);

                    if(!showOnlyUnReservedCheckBox.isSelected()){

                        observableFilteredlist = observableListTableView.
                                stream().
                                filter(x -> x.getReservationDate() != null).
                                collect(Collectors.toCollection(FXCollections::observableArrayList));
                        tableView.setItems(observableFilteredlist);
                    }
                }else{
                    if(!showOnlyUnReservedCheckBox.isSelected()){
                        tableView.setItems(observableListTableView);
                    }
                }
                //if both unselected, query the db again
            }
        });

        showOnlyUnReservedCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                if(showOnlyUnReservedCheckBox.isSelected()){
                    showOnlyReservedCheckBox.setSelected(false);
                    if(!showOnlyReservedCheckBox.isSelected()){

                        observableFilteredlist = observableListTableView.stream().filter(x -> x.getReservationDate() == null).collect(Collectors.toCollection(FXCollections::observableArrayList));
                        tableView.setItems(observableFilteredlist);
                    }
                }else{
                    if(!showOnlyReservedCheckBox.isSelected()){
                        tableView.setItems(observableListTableView);
                    }
                }


            }
        });
    }

    /**
     * reset both checkboxes to false
     * */
    public void resetCheckBoxes(){
        showOnlyReservedCheckBox.setSelected(false);
        showOnlyUnReservedCheckBox.setSelected(false);
    }



}
