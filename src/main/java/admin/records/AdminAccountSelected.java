package admin.records;

public record AdminAccountSelected(String firstName, String lastName, String phone, String zip, String address, int officeID, int typeID) {
}