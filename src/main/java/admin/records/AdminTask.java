package admin.records;

public record AdminTask(int projectID, int taskID, String taskName, String taskDescription, String projectName,
                        int priorityLevel, String accountEmail, int estimatedPomodoros, String reservationDate) {
}