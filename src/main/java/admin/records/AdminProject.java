package admin.records;

public record AdminProject(int id, String description, String deadline, String name) {
}
