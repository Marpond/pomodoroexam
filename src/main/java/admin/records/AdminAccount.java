package admin.records;

public record AdminAccount(String firstName,
                           String lastName,
                           String email,
                           String phoneNumber,
                           String roleDescription,
                           String officeAddress) {
}
