package admin;

import stage.StageLoader;


public class MainForTest extends StageLoader {
    /**
     * Access point for just Admin. Mainly for testing
     * */
    public static void main(String[] args) {
        MainForTest.setFxml("admin.fxml");
        launch(args);
    }
}
