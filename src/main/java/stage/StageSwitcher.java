package stage;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import main.Main;

import java.io.IOException;
import java.util.Objects;

public abstract class StageSwitcher {

    /**
     * Switches to the given fxml file.
     *
     * @param fxml  the fxml file to switch to, the parameter should only contain the file name, and it's extension.
     *              For example: "main.fxml"
     * @param clazz the class of the fxml file's controller.
     */
    protected static void switchTo(String fxml, Class<?> clazz) {
        try {
            // Load fxml
            System.out.println(clazz);
            Parent root = FXMLLoader.load(Objects.requireNonNull(clazz.getResource(fxml)));
            // Change to new scene
            Main.stage.setScene(new Scene(root));
            // Request focus so stuff works
            root.requestFocus();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
