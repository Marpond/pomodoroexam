package stage;

import database.DatabaseHandlerAdmin;
import database.DatabaseHandlerConsultant;
import database.DatabaseHandlerMain;
import database.DatabaseHandlerOffice;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * This class is extended by the Main class of all the JavaFX applications.
 */
public abstract class StageLoader extends Application {
    private static String fxml;
    private static String title;

    protected static Stage stage;

    protected static void setFxml(String fxml) {
        StageLoader.fxml = fxml;
    }

    protected static void setTitle(String title) {
        StageLoader.title = title;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Scene scene = new Scene(new FXMLLoader(getClass().getResource(fxml)).load());
        stage = primaryStage;
        primaryStage.setScene(scene);
        primaryStage.setTitle(title);
        primaryStage.show();
        primaryStage.setResizable(false);
        primaryStage.onCloseRequestProperty().set(e -> {
            DatabaseHandlerAdmin.closeConnection();
            DatabaseHandlerConsultant.closeConnection();
            DatabaseHandlerOffice.closeConnection();
            DatabaseHandlerMain.closeConnection();
        });
    }
}