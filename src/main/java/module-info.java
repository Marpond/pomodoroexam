module easv.pomodoro.exampomodoro {
    requires javafx.controls;
    requires javafx.fxml;

    requires java.sql;
    requires junit;
    requires activation;
    requires javax.mail;
    requires com.microsoft.sqlserver.jdbc;
    requires itextpdf;
    requires org.apache.poi.poi;
    requires org.apache.poi.ooxml;
    requires java.desktop;
    requires javafx.media;


    opens consultant to javafx.fxml;
    opens admin to javafx.fxml;
    opens office to javafx.fxml;
    opens main to javafx.fxml;
    opens unittest to junit;
    opens sound to javafx.media;
    opens database to javafx.fxml;
    opens database.DAOInterfaces to javafx.fxml;
    opens date to javafx.fxml;

    exports consultant;
    exports admin;
    exports office;
    exports main;
    exports unittest;
    exports admin.records;
    exports database.DAOInterfaces;
    exports database;
    exports date;
    exports admin.chartGenerators;
    opens admin.chartGenerators to javafx.fxml;
    exports admin.spreadSheetGenerators;
    opens admin.spreadSheetGenerators to javafx.fxml;
    exports admin.PDFGenerators;
    opens admin.PDFGenerators to javafx.fxml;
    exports admin.enumerators;
    opens admin.enumerators to javafx.fxml;
}