package unittest;

import date.DateHandler;
import javafx.scene.layout.Pane;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestDateHandler {

    DateHandler dateHandler = new DateHandler(21, 22);

    @Test
    public void testFormatIntToTime1() {
        int time = 23;
        String timeString = dateHandler.formatIntMinuteToHHMM(time);
        System.out.println(time);
        System.out.println(timeString);
        System.out.println("\n");
        assert timeString.equals("00:23");
    }

    @Test
    public void testFormatIntToTime2() {
        int time = 0;
        String timeString = dateHandler.formatIntMinuteToHHMM(time);
        System.out.println(time);
        System.out.println(timeString);
        System.out.println("\n");
        assert timeString.equals("00:00");
    }

    @Test
    public void testFormatIntToTime3() {
        int time = 60;
        String timeString = dateHandler.formatIntMinuteToHHMM(time);
        System.out.println(time);
        System.out.println(timeString);
        System.out.println("\n");
        assert timeString.equals("01:00");
    }

    @Test
    public void testFormatIntToTime4() {
        int time = 60 * 24;
        String timeString = dateHandler.formatIntMinuteToHHMM(time);
        System.out.println(time);
        System.out.println(timeString);
        System.out.println("\n");
        assert timeString.equals("24:00");
    }

    @Test
    public void testFormatTimeToRelativePixelValue() {
        dateHandler = new DateHandler(21, 22);
        String time1 = "21:00:00";
        String time2 = "21:05:00";
        String time3 = "21:10:00";
        Pane pane = new Pane();
        pane.setPrefWidth(1000);
        int pixelValue1 = dateHandler.formatTimeToRelativePixelValueInt(pane, time1);
        int pixelValue2 = dateHandler.formatTimeToRelativePixelValueInt(pane, time2);
        int pixelValue3 = dateHandler.formatTimeToRelativePixelValueInt(pane, time3);
        int expectedPixelValue1 = 0;
        System.out.println("pixelValue1: " + pixelValue1 + " expectedPixelValue1: " + expectedPixelValue1);
        assertEquals(expectedPixelValue1, pixelValue1);
        int expectedPixelValue2 = (int) pane.getPrefWidth() / 12;
        System.out.println("pixelValue2: " + pixelValue2 + " expectedPixelValue2: " + expectedPixelValue2);
        assertEquals(expectedPixelValue2, pixelValue2, 1);
        int expectedPixelValue3 = (int) pane.getPrefWidth() / 12 * 2;
        System.out.println("pixelValue3: " + pixelValue3 + " expectedPixelValue3: " + expectedPixelValue3);
        assertEquals(expectedPixelValue3, pixelValue3, 1);
        System.out.println("\n");
    }

    @Test
    public void testGetDifferenceHHMMSS() throws InterruptedException {
        String time1 = dateHandler.getCurrentHHMMSS();
        Thread.sleep(1000);
        String time2 = dateHandler.getCurrentHHMMSS();
        String difference = dateHandler.getDifferenceHHMMSS(time1, time2);
        System.out.println(time1);
        System.out.println(time2);
        System.out.println(difference);
        assert difference.equals("00:00:01");
    }
}
