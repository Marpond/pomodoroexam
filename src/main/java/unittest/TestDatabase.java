package unittest;


import database.DatabaseHandlerAdmin;
import database.DatabaseHandlerConsultant;
import database.DatabaseHandlerMain;
import database.DatabaseHandlerOffice;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestDatabase {

    @Test
    public void testDatabaseHandlerAdmin() {
        DatabaseHandlerAdmin admin1 = DatabaseHandlerAdmin.getInstance();
        DatabaseHandlerAdmin admin2 = DatabaseHandlerAdmin.getInstance();
        assertNotNull(admin1);
        assertNotNull(admin2);
        assertEquals(admin1, admin2);
        DatabaseHandlerAdmin.closeConnection();
    }

    @Test
    public void testDatabaseHandlerConsultant() {
        DatabaseHandlerConsultant consultant1 = DatabaseHandlerConsultant.getInstance();
        DatabaseHandlerConsultant consultant2 = DatabaseHandlerConsultant.getInstance();
        assertNotNull(consultant1);
        assertNotNull(consultant2);
        assertEquals(consultant1, consultant2);
        DatabaseHandlerConsultant.closeConnection();
    }

    @Test
    public void testDatabaseHandlerOffice() {
        DatabaseHandlerOffice office1 = DatabaseHandlerOffice.getInstance();
        DatabaseHandlerOffice office2 = DatabaseHandlerOffice.getInstance();
        assertNotNull(office1);
        assertNotNull(office2);
        assertEquals(office1, office2);
        DatabaseHandlerOffice.closeConnection();
    }

    @Test
    public void testDataBaseHandlerMain() {
        DatabaseHandlerMain main1 = DatabaseHandlerMain.getInstance();
        DatabaseHandlerMain main2 = DatabaseHandlerMain.getInstance();
        assertNotNull(main1);
        assertNotNull(main2);
        assertNotNull(main1);
        assertNotNull(main2);
        assertEquals(main1, main2);
        DatabaseHandlerMain.closeConnection();
    }
}
