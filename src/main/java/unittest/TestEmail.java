package unittest;

import main.EmailHandler;
import main.TwoFactor;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class TestEmail {
    TwoFactor twoFactor = new TwoFactor();

    @Test
    public void testTwoFactor() {
        assertNotNull(twoFactor.getCode());
    }

    @Test(timeout = 15000)
    public void testEmail() {
        EmailHandler emailHandler = new EmailHandler();
        emailHandler.setRecipient("pomodorologin@gmail.com");
        emailHandler.send(twoFactor.getCode());
        System.out.println("Email sent");
    }
}
