package unittest;

import admin.Controller;
import admin.WorkTimeOfConsultant;
import database.DatabaseHandlerAdmin;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

public class AdminTests {
    private static DatabaseHandlerAdmin db;
    private static HashSet<String> testHashSet;

    @BeforeClass
    public static void setUp(){
        System.out.println("Before class");
        db = DatabaseHandlerAdmin.getInstance();
        testHashSet = new HashSet<>();
        testHashSet.add("Testname1");
        testHashSet.add("TestName2");
    }

    @AfterClass
    public static void tearDown(){
        System.out.println("After class");
    }

    @Test
    public void testDateValidator1(){
        System.out.println("Testing case 1 for date validator");
        boolean expected = Controller.dateisValid("2011-01-01");
        boolean actual = true;
        assertEquals(expected, actual);
    }

    @Test
    public void testValidator2(){
        System.out.println("Testing case 2 for date validator");
        boolean expected = Controller.dateisValid("abc");
        boolean actual = false;
        assertEquals(expected, actual);
    }

    @Test
    public void testValidator3(){
        System.out.println("Testing case 3 for date validator");
        boolean expected = Controller.dateisValid("2000-02-31");
        boolean actual = false;

        assertNotEquals(expected, actual);
    }

    @Test
    public void projectNameValidatorForTests(){
        boolean actual = Controller.projectNameCheck("Testname1", testHashSet);
        boolean predicted = true;
        assertEquals(predicted, actual);
    }

    @Test
    public void projectNameValidator(){
        boolean actual = Controller.projectNameCheck("Not in list", testHashSet);
        boolean predicted = false;
        assertEquals(predicted, actual);
    }

    @Test
    public void efficiencyTest1(){
        //testing realistic numbers
        double workTime = 30;
        double shortBreakTime = 5;
        double longBreakTime = 40;
        double longBreakInterval = 4;

        double predicted = 68.5;
        double actual = WorkTimeOfConsultant.efficiencyCalculator(workTime, shortBreakTime, longBreakTime, longBreakInterval);
        assertEquals(predicted, actual, 0.1);
    }

    @Test
    public void efficiencyTest2(){
        //testing unrealistic numbers
        double workTime = 100;
        double shortBreakTime = 0;
        double longBreakTime = 200;
        double longBreakInterval = 2;

        double predicted = 50;
        double actual = WorkTimeOfConsultant.efficiencyCalculator(workTime, shortBreakTime, longBreakTime, longBreakInterval);
        assertEquals(predicted, actual, 0.1);
    }




}
