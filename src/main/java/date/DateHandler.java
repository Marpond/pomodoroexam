package date;

import javafx.scene.layout.Pane;

import java.text.SimpleDateFormat;

public class DateHandler {

    private final SimpleDateFormat sdfHHMMSS = new SimpleDateFormat("HH:mm:ss");
    private final SimpleDateFormat sdfHHMM = new SimpleDateFormat("HH:mm");
    private int startHour;
    private int endHour;

    public DateHandler(int startHour, int endHour) {
        this.startHour = startHour;
        this.endHour = endHour;
    }

    public DateHandler() {
        this(0, 0);
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    /**
     * @return The current clock time as a String in HH:mm format.
     */
    public String getCurrentHHMM() {
        return sdfHHMM.format(new java.util.Date());
    }

    /**
     * @return The current clock time as a String in HH:mm:ss format.
     */
    public String getCurrentHHMMSS() {
        return sdfHHMMSS.format(new java.util.Date());
    }

    /**
     * Formats a given time in milliseconds to a String in HH:mm:ss format.
     *
     * @param time The time in milliseconds to convert.
     * @return The given time in milliseconds as a String in HH:mm:ss format.
     */
    public String formatMillisToHHMMSS(long time) {
        int hours = (int) (time / 3600000);
        int minutes = (int) (time / 60000) % 60;
        int seconds = (int) (time / 1000) % 60;
        return (hours > 9 ? hours : "0" + hours) +
                ":" +
                (minutes > 9 ? minutes : "0" + minutes) +
                ":" +
                (seconds > 9 ? seconds : "0" + seconds);
    }

    /**
     * Returns the difference between two HH:mm:ss formatted Strings in HH:mm:ss format.
     * Example: "13:00:00" - "12:30:00" = "00:30:00"
     *
     * @param startTime The start time in HH:mm:ss format.
     * @param endTime   The end time in HH:mm:ss format.
     * @return The difference between two HH:mm:ss formatted Strings in HH:mm:ss format.
     * @see #formatMillisToHHMMSS
     */
    public String getDifferenceHHMMSS(String startTime, String endTime) {
        long difference = 0;
        try {
            difference = sdfHHMMSS.parse(endTime).getTime() - sdfHHMMSS.parse(startTime).getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formatMillisToHHMMSS(difference);
    }

    /**
     * Returns a pixel value of a given time relative to a given pane's width.
     *
     * @param pane The pane of the horizontal line.
     * @param time The time needed to convert to a pixel value, accepts a String format of "HH:mm:ss"/ "HH:mm".
     * @return The pixel equivalent value of the given time inside the pane.
     */
    public int formatTimeToRelativePixelValueInt(Pane pane, String time) {
        double pixelPerHour = (pane.getPrefWidth() / (this.endHour - this.startHour));
        String[] timeSplit = time.split(":");
        int totalHours = Integer.parseInt(timeSplit[0]);
        int minutes = Integer.parseInt(timeSplit[1]);
        int realHours = totalHours - this.startHour;
        double pixelValue = (realHours * pixelPerHour) + (minutes * (pixelPerHour / 60));
        if (pixelValue > pane.getPrefWidth()) {
            return (int) pane.getPrefWidth();
        }
        // ceiling the pixel value to an integer.
        return (int) Math.ceil(pixelValue);
    }

    /**
     * @param time The wanted time (in minutes) to convert to a String in HH:mm format.
     *             1 will return "00:01"
     *             60 will return "01:00"
     * @return The given time as a String in HH:mm format.
     */
    public String formatIntMinuteToHHMM(int time) {
        int hours = time / 60;
        int minutes = time % 60;
        return (hours > 9 ? hours : "0" + hours) + ":" + (minutes > 9 ? minutes : "0" + minutes);
    }

    /**
     * Takes the hour values of an HH:MM string and returns them as an integer.
     *
     * @param time The time to convert to an integer.
     * @return The hours as an integer.
     */
    public int formatHHMMToIntHour(String time) {
        String[] timeSplit = time.split(":");
        return Integer.parseInt(timeSplit[0]);
    }

    /**
     * @return The start shift hour as a String in HH:mm format.
     * @see #getEndHourString()
     */
    public String getStartHourString() {
        return (this.startHour > 9 ? this.startHour : "0" + this.startHour) + ":00";
    }

    /**
     * @return The end shift hour as a String in HH:mm format.
     * @see #getStartHourString()
     */
    public String getEndHourString() {
        return (this.endHour > 9 ? endHour : "0" + endHour) + ":00";
    }
}
