package sound;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

public class SoundHandler {
    public SoundHandler() {
    }

    /**
     * Plays a sound file
     *
     * @param sound the sound file to play
     */
    private MediaPlayer mediaPlayer;

    public void play(String sound) {
        mediaPlayer = new MediaPlayer(new Media(new File("src/main/resources/sound/%s".formatted(sound)).toURI().toString()));
        mediaPlayer.play();
    }
}
