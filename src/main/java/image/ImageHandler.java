package image;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;

public class ImageHandler {

    public ImageHandler() {
    }

    public ImageView getImageView(String path, int size) {
        ImageView imageView = new ImageView(new Image(new File(path).toURI().toString()));
        imageView.setFitHeight(size);
        imageView.setFitWidth(size);
        imageView.setPreserveRatio(true);
        return imageView;
    }

    public Button getButtonWithImageView(String path, int size) {
        Button button = new Button();
        ImageView imageView = getImageView(path, size);
        imageView.setPreserveRatio(true);
        button.setGraphic(imageView);
        return button;
    }

    public Image getImage(String path) {
        return new Image(new File(path).toURI().toString());
    }
}
