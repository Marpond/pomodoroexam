package database;

import java.sql.*;
import java.util.Objects;
import java.util.Properties;

/**
 * A singleton class that connects to the database.
 */
abstract class DatabaseHandler {
    protected static Object instance;
    protected static Connection con;
    protected PreparedStatement ps;
    protected CallableStatement cs;
    protected ResultSet rs;

    /**
     * Private constructor that connects to the database.
     */
    protected DatabaseHandler() {
        try {
            // Load the properties file
            Properties properties = new Properties();
            properties.load(Objects.requireNonNull(DatabaseHandler.class.getResourceAsStream("database.properties")));
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            // Connect to the database
            con = DriverManager.getConnection(
                    """
                            jdbc:sqlserver://%s:1433;
                            trustServerCertificate=true;
                            database=%s;
                            user=%s;
                            password=%s
                            """.formatted(
                            properties.getProperty("host"),
                            properties.getProperty("database"),
                            properties.getProperty("user"),
                            properties.getProperty("password")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void closeConnection() {
        try {
            con.close();
            instance = null;
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Error closing connection");
        }
    }

}
