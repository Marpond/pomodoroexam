package database;

import admin.*;
import admin.records.AdminAccount;
import admin.records.AdminAccountSelected;
import admin.records.AdminProject;
import admin.records.AdminTask;
import database.DAOInterfaces.AccountsDAO;
import database.DAOInterfaces.ProjectDAO;
import database.DAOInterfaces.StatisticsDAO;
import database.DAOInterfaces.TaskViewDAO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHandlerAdmin extends DatabaseHandler implements ProjectDAO, TaskViewDAO, AccountsDAO, StatisticsDAO {

    /**
     * Constructs an object of the superclass
     * */
    DatabaseHandlerAdmin() {
        super();
    }

    /**
     * Get the singleton instance
     * */
    public static DatabaseHandlerAdmin getInstance() {
        if (instance == null) {
            instance = new DatabaseHandlerAdmin();
        }
        return (DatabaseHandlerAdmin) instance;
    }

    /**
     * @return queriedProjects: Returns the data from the database which was queried when executing the preparestatement
     * */
    @Override
    public ArrayList<TableViewProjects> getAllProjects() {
        ArrayList<TableViewProjects> queriedProjects = new ArrayList<>();
        try {
            ps = con.prepareStatement("SELECT * FROM tbl_Projects");
            rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String deadline = rs.getString(4);
                String description = rs.getString(3);
                queriedProjects.add(new TableViewProjects(new AdminProject(id, description, deadline, name))
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return queriedProjects;
    }

    @Override
    public ArrayList<TableViewStatistics> getAllProjectsStatistics() {
        ArrayList<TableViewStatistics> queriedProjects = new ArrayList<>();
        try {
            ps = con.prepareStatement("SELECT * FROM tbl_Projects");
            rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String deadline = rs.getString(4);
                String description = rs.getString(3);
                queriedProjects.add(new TableViewStatistics(id, name, description, deadline, ""));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return queriedProjects;
    }

    /**
     * @return listOfEstimatedTimes Returns a list of objects, ProjectEstimatedTimes
     * */
    @Override
    public ArrayList<ProjectEstimatedTimes> getAllProjectEstimatedTimes() {
        ArrayList<ProjectEstimatedTimes> listOfEstimatedTimes = new ArrayList<>();
        ProjectEstimatedTimes currentRow;

        try{
            cs = con.prepareCall("{CALL sp_getProjectsByTotalEstimatedTime ()}");
            rs = cs.executeQuery();
            while(rs.next()){
                String name = rs.getString(1);
                int estTime = rs.getInt(2);
                currentRow = new ProjectEstimatedTimes(name, estTime);
                listOfEstimatedTimes.add(currentRow);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return listOfEstimatedTimes;
    }

    @Override
    public List<Integer> getAllOverDueProjects() {
        List<Integer> overdueList = new ArrayList<>();
        try {
            cs = con.prepareCall("{CALL sp_GetAllOverDueProjects()}");
            rs = cs.executeQuery();
            while (rs.next()) {
                overdueList.add(rs.getInt(1));
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return overdueList;
    }

    @Override
    public ArrayList<TaskStatistics> getAllCompletedTasksForAllConsultants() {
        ArrayList<TaskStatistics> completedTasks = new ArrayList<>();
        int tasks;
        String email;

        try {
            cs = con.prepareCall("{CALL SP_GetCompletedTasksOfAllConsultants()}");
            rs = cs.executeQuery();
            while (rs.next()) {
                tasks = rs.getInt(1);
                email = rs.getString(2);
                TaskStatistics statistic = new TaskStatistics(tasks, email);
                completedTasks.add(statistic);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return completedTasks;
    }

    @Override
    public ArrayList<TaskStatistics> getAllOverdueTasksForAllConsultants() {
        ArrayList<TaskStatistics> overdueTasks = new ArrayList<>();
        int tasks;
        String email;

        try {
            cs = con.prepareCall("{CALL sp_GetAllOverDueTasksForAllConsultants()}");
            rs = cs.executeQuery();
            while (rs.next()) {
                tasks = rs.getInt(1);
                email = rs.getString(2);
                TaskStatistics statistic = new TaskStatistics(tasks, email);
                overdueTasks.add(statistic);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return overdueTasks;
    }

    @Override
    public ArrayList<TaskStatistics> getAllNotOverdueTasksForAllConsultants() {
        ArrayList<TaskStatistics> notOverdueTasks = new ArrayList<>();
        int tasks;
        String email;
        try {
            cs = con.prepareCall("{CALL sp_GetAllNotOverDueTasksForAllConsultants()}");
            rs = cs.executeQuery();
            while (rs.next()) {
                tasks = rs.getInt(1);
                email = rs.getString(2);
                TaskStatistics statistic = new TaskStatistics(tasks, email);
                notOverdueTasks.add(statistic);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return notOverdueTasks;
    }

    /**
     * @return workTimeOfConsultantArrayList: Returns an arraylist of objects of type WorkTimeOfConsultant.
     * */
    @Override
    public ArrayList<WorkTimeOfConsultant> getAllConsultantWorkTimes() {
        ArrayList<WorkTimeOfConsultant> workTimeOfConsultantArrayList = new ArrayList<>();
        String email;
        int workTime;
        int shortBreakTime;
        int longBreaktime;
        int longBreakInterval;
        String name;

        try{
            ps = con.prepareStatement("select * from tbl_AccountSettings \n" +
                    "inner join \n" +
                    "tbl_Accounts on \n" +
                    "tbl_AccountSettings.fld_AccountEmail = tbl_Accounts.fld_AccountEmail");
            rs = ps.executeQuery();
            while(rs.next()){
                email = rs.getString(2);
                workTime = rs.getInt(3);
                shortBreakTime = rs.getInt(4);
                longBreaktime = rs.getInt(5);
                longBreakInterval = rs.getInt(6);
                name = rs.getString(9);
                workTimeOfConsultantArrayList.add(new WorkTimeOfConsultant(email, name, workTime, shortBreakTime, longBreaktime, longBreakInterval));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return workTimeOfConsultantArrayList;
    }

    /**
     * @param id Get information of a project using it's ID. Execute a stored procedure which uses the ID as a parameter
     * @return Returns a TableViewProjects object. Used in the initialise method of the ControllerSelectedProject
     * */
    @Override
    public TableViewProjects getProjectByID(String id) {
        try {
            cs = con.prepareCall("{CALL sp_GetProjectByID (?)}");
            cs.setInt(1, Integer.parseInt(id));
            rs = cs.executeQuery();
            if (rs.next()) {
                String name = rs.getString(2);
                String description = rs.getString(3);
                String deadline = rs.getString(4);
                return new TableViewProjects(new AdminProject(Integer.parseInt(id), description, deadline, name));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * @param id Project ID
     * @param name Project Name
     * @param description Project Description
     * @param deadline Project Deadline
     * Using the ID, update the name, description and deadline of the project
     * */
    @Override
    public void editProject(String id, String name, String description, String deadline) {
        try {
            cs = con.prepareCall("{CALL sp_EditProject (?,?,?,?)}");
            cs.setInt(1, Integer.parseInt(id));
            cs.setString(2, name); //Description
            cs.setString(3, description); //deadline
            cs.setString(4, deadline); //name
            cs.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param id Project iD
     * Using the ID, delete the project from the database
     * */
    @Override
    public void deleteProject(String id) {
        try {
            cs = con.prepareCall("{CALL sp_DeleteProject (?)}");
            cs.setString(1, id);
            cs.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param project An object of the class TableViewProjects
     * Using the parameter, insert a new row into the project table. The parameter contains information about the project name, deadlien and description
     * */
    @Override
    public void insertProject(TableViewProjects project) {
        try {
            cs = con.prepareCall("{CALL sp_InsertProject(?,?,?)}");
            cs.setString(1, project.getDescription());
            cs.setString(2, project.getDeadline());
            cs.setString(3, project.getName());
            cs.execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param parameters Using the array of Strings, call a stored procedure which updates a project in the database
     * @return searchedList Returns an arraylist of objects of class TableViewProjects
     * */
    @Override
    public ArrayList<TableViewProjects> searchProjectsWithAnyParameters(String[] parameters) {
        ArrayList<TableViewProjects> searchedList = new ArrayList<>();
        try {
            cs = con.prepareCall("{CALL sp_SearchProjectsByName (?)}");
            for (int i = 0; i < parameters.length; i++) {
                cs.setString((i + 1), parameters[i]);
            }
            rs = cs.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String description = rs.getString(3);
                String deadline = rs.getString(4);

                searchedList.add(new TableViewProjects(new AdminProject(id, description, deadline, name))
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return searchedList;
    }

    /**
     * @param _projectID using the projectID, get all tasks belonging to that project.
     * @return returnedList Returns an arraylist of objects of class TableViewTasks
     * */
    @Override
    public ArrayList<TableViewTasks> getAllTasksForTableView(int _projectID) {
        ArrayList<TableViewTasks> returnedList = new ArrayList<>();
        try {
            cs = con.prepareCall("{CALL sp_getAllTasksForTableView(?)}");
            cs.setInt(1, _projectID);
            rs = cs.executeQuery();
            while (rs.next()) {
                int projectID = rs.getInt(1);
                int taskID = rs.getInt(2);
                String taskName = rs.getString(3);
                String taskDescription = rs.getString(4);
                String projectName = rs.getString(5);
                int priorityLevel = rs.getInt(6);
                String accountEmail = rs.getString(7);
                int estimatedTime = rs.getInt(8);
                String reservationDate = rs.getString(9);
                returnedList.add(new TableViewTasks(new AdminTask(projectID, taskID, taskName, taskDescription, projectName, priorityLevel, accountEmail, estimatedTime, reservationDate)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnedList;
    }


    /**
     * @param projectID The ID of the project which the new task should belong to
     * @param taskName The name of the task to be created
     * @param description The description of the new task
     * @param priority The priority level of the new task
     * @param estTime The estimated time to complete the new task
     * The method calls a stored procedure to insert a new row into the tasks table with the input parameters
     * */
    @Override
    public void createTaskInProject(int projectID, String taskName, String description, int priority, int estTime) {
        try {
            cs = con.prepareCall("{CALL sp_CreateTaskInProject(?,?,?,?,?)}");
            cs.setInt(1, projectID);
            cs.setString(2, taskName);
            cs.setString(3, description);
            cs.setInt(4, priority);
            cs.setInt(5, estTime);
            cs.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param taskID Delete a task using the task ID
     * */
    @Override
    public void deleteTaskByTaskID(int taskID) {
        try {
            cs = con.prepareCall("{CALL sp_DeleteTaskByTaskID(?)}");
            cs.setInt(1, taskID);
            cs.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param ID Primary key of the task
     * @param taskName Name of the task
     * @param taskDescription Task Description
     * @param priority Priority level of the task
     * @param estTime Estimated time to complete the task
     * Execute a stored procedure which executes an update statement with the parameters
     * */
    @Override
    public void updateTask(int ID, String taskName, String taskDescription, int priority, int estTime) {
        try {
            cs = con.prepareCall("{CALL sp_UpdateTask(?,?,?,?,?)}");
            cs.setInt(1, ID);
            cs.setString(2, taskName);
            cs.setString(3, taskDescription);
            cs.setInt(4, priority);
            cs.setInt(5, estTime);
            cs.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param projID Primary key of the project and foreign key of the task
     * @param parameters attributes of the task which can be used to search for a task or many tasks
     *
     * */
    @Override
    public ArrayList<TableViewTasks> universalSearchForTask(int projID, String[] parameters) {
        ArrayList<TableViewTasks> returnedList = new ArrayList<>();
        try {
            cs = con.prepareCall("{CALL sp_UniversalSearchForTask (?,?,?,?,?,?)}");
            cs.setInt(1, projID);
            for (int i = 0; i < parameters.length; i++) {
                cs.setString((i + 2), parameters[i]);
            }
            rs = cs.executeQuery();
            while (rs.next()) {
                int projectID = rs.getInt(1);
                int taskID = rs.getInt(2);
                String taskName = rs.getString(3);
                String taskDescription = rs.getString(4);
                String projectName = rs.getString(5);
                int priorityLevel = rs.getInt(6);
                String accountEmail = rs.getString(7);
                int estimatedTime = rs.getInt(8);
                String reservationDate = rs.getString(9);
                returnedList.add(new TableViewTasks(new AdminTask(projectID, taskID, taskName, taskDescription, projectName, priorityLevel, accountEmail, estimatedTime, reservationDate)));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return returnedList;
    }

    @Override
    public ArrayList<TableViewAccounts> getAccountsForAdminTableView() {
        ArrayList<TableViewAccounts> returnedList = new ArrayList<>();
        try {
            cs = con.prepareCall("{CALL sp_GetAccountsForAdminTableView()}");
            rs = cs.executeQuery();
            while (rs.next()) {
                String firstName = rs.getString(1);
                String lastName = rs.getString(2);
                String email = rs.getString(3);
                String phone = rs.getString(4);
                String roleDescription = rs.getString(5);
                String officeAddress = rs.getString(6);
                returnedList.add(new TableViewAccounts(
                        new AdminAccount(firstName, lastName, email, phone, roleDescription, officeAddress)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnedList;
    }

    @Override
    public void deleteAccountByEmail(String email) {
        try {
            cs = con.prepareCall("{CALL sp_DeleteAccount(?)}");
            cs.setString(1, email);
            cs.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAccount(String email, String firstName, String lastName, String phone, String zip, String address, int officeID, int typeID) {
        try {
            cs = con.prepareCall("{CALL sp_UpdateAccount (?,?,?,?,?,?,?,?)}");
            cs.setString(1, email);
            cs.setString(2, firstName);
            cs.setString(3, lastName);
            cs.setString(4, phone);
            cs.setString(5, zip);
            cs.setString(6, address);
            cs.setInt(7, officeID);
            cs.setInt(8, typeID);
            cs.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertAccount(String email, String firstName, String lastName, String phone, String zip, String address, int officeID, int typeID) {
        try {
            cs = con.prepareCall("{CALL sp_InsertAccount (?,?,?,?,?,?,?,?)}");
            cs.setString(1, email);
            cs.setString(2, firstName);
            cs.setString(3, lastName);
            cs.setString(4, phone);
            cs.setString(5, zip);
            cs.setString(6, address);
            cs.setInt(7, officeID);
            cs.setInt(8, typeID);
            cs.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public AdminAccountSelected getAccountByEmail(String email) {
        try {
            cs = con.prepareCall("{CALL sp_GetAccountByEmail(?)}");
            cs.setString(1, email);
            rs = cs.executeQuery();
            if (rs.next()) {
                String firstName = rs.getString(2);
                String lastName = rs.getString(3);
                String phone = rs.getString(4);
                String zip = rs.getString(5);
                String address = rs.getString(6);
                int officeID = rs.getInt(7);
                int typeID = rs.getInt(8);
                return new AdminAccountSelected(firstName, lastName, phone, zip, address, officeID, typeID);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public List<Integer> getAllOfficeIDs() {
        List<Integer> officeIDs = new ArrayList<>();
        try {
            cs = con.prepareCall("{CALL sp_GetAllOfficeIDs()}");
            rs = cs.executeQuery();
            while (rs.next()) {
                officeIDs.add(rs.getInt(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return officeIDs;
    }

    @Override
    public List<Integer> getAllTypeIDs() {
        List<Integer> typeIDs = new ArrayList<>();
        try {
            cs = con.prepareCall("{CALL sp_GetAllTypeIDs()}");
            rs = cs.executeQuery();
            while (rs.next()) {
                typeIDs.add(rs.getInt(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return typeIDs;
    }

    @Override
    public ArrayList<TableViewPermissions> getPermissionsForAccount(String email) {
        ArrayList<TableViewPermissions> returnedPermissionsList = new ArrayList<>();
        try {
            cs = con.prepareCall("{CALL sp_GetPermissionsForAccount(?)}");
            cs.setString(1, email);
            rs = cs.executeQuery();
            while (rs.next()) {
                int permissionID = rs.getInt(1);
                System.out.println(permissionID);
                int projectID = rs.getInt(2);
                String projectName = rs.getString(3);
                int edit = rs.getInt(4);
                int view = rs.getInt(5);

                returnedPermissionsList.add(new TableViewPermissions(email, permissionID, projectID, projectName, edit, view));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnedPermissionsList;
    }

    @Override
    public void insertPermission(int projectID,String email, boolean edit, boolean view) {
        int editInt;
        int viewInt;
        if (edit){
            editInt = 1;
        }
        else{
            editInt = 0;
        }
        if (view){
            viewInt = 1;
        }
        else{
            viewInt = 0;
        }
        try {
            cs = con.prepareCall("{CALL sp_InsertPermission (?,?,?,?)}");
            cs.setInt(1, projectID);
            cs.setString(2, email);
            cs.setInt(3, editInt);
            cs.setInt(4, viewInt);
            cs.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updatePermission(int permissionID, boolean view, boolean edit) {
        int editInt;
        int viewInt;
        if (edit){
            editInt = 1;
        }
        else{
            editInt = 0;
        }
        if (view){
            viewInt = 1;
        }
        else{
            viewInt = 0;
        }
        try {
            cs = con.prepareCall("{CALL sp_UpdatePermission (?,?,?)}");
            cs.setInt(1, permissionID);
            cs.setInt(2, viewInt);
            cs.setInt(3, editInt);
            cs.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<TableViewAccounts> searchAccountByString(String searchString) {
        ArrayList<TableViewAccounts> returnedAccountsList = new ArrayList<>();
        try {
            cs = con.prepareCall("{CALL sp_SearchAccountByString(?)}");
            cs.setString(1, searchString);
            rs = cs.executeQuery();
            while (rs.next()) {
                String firstName = rs.getString(1);
                String lastName = rs.getString(2);
                String email = rs.getString(3);
                String phone = rs.getString(4);
                String roleDescription = rs.getString(5);
                String officeAddress = rs.getString(6);
                returnedAccountsList.add(new TableViewAccounts(
                        new AdminAccount(firstName, lastName, email, phone, roleDescription, officeAddress)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnedAccountsList;
    }

}

