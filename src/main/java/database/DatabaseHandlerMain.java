package database;

import database.DAOInterfaces.MainDAO;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandlerMain extends DatabaseHandler implements MainDAO {

    public DatabaseHandlerMain() {
        super();
    }

    public static DatabaseHandlerMain getInstance() {
        if (instance == null) {
            instance = new DatabaseHandlerMain();
        }
        return (DatabaseHandlerMain) instance;
    }

    @Override
    public int getTypeIDByEmail(String email) {
        try {
            ps = con.prepareStatement("sp_GetTypeIDByEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<String> getAllAccountEmails() {
        List<String> emails = new ArrayList<>();
        try {
            ps = con.prepareStatement("sp_GetAllAccountEmails");
            rs = ps.executeQuery();
            while (rs.next()) {
                emails.add(rs.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return emails;
    }

    @Override
    public boolean checkIfAccountSettingExistsByEmail(String email) {
        try {
            ps = con.prepareStatement("sp_CheckIfAccountSettingExistsByEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean checkIfAccountIsActivatedByEmail(String email) {
        try {
            ps = con.prepareStatement("sp_CheckIfAccountIsActivatedByEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
