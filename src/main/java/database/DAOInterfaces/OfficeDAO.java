package database.DAOInterfaces;

import office.records.*;

import java.util.List;

public interface OfficeDAO {
    boolean checkIfConsultantStartedByConsultantEmail(String email);

    boolean checkIfConsultantPausedByConsultantEmail(String email);

    boolean checkIfConsultantHasBreakByConsultantEmail(String email);

    int getOfficeIDByEmail(String email);

    String getOfficeNameByID(int officeID);

    OfficeAccountSettings getAccountSettingsByConsultantEmail(String email);

    List<OfficeAccount> getAllConsultantInfoByOfficeID(int officeID);

    List<OfficeTask> getAllTasksTodayByConsultantEmail(String email);

    List<OfficePause> getAllPausesTodayByConsultantEmail(String email);

    List<OfficeBreak> getAllBreaksTodayByConsultantEmail(String email);
}
