package database.DAOInterfaces;

import consultant.records.ConsultantAccount;
import consultant.records.ConsultantAccountSettings;
import consultant.records.ConsultantTask;

import java.util.List;

public interface ConsultantDAO {
    void insertAccountSettings(String email, int workTime, int shortBreakTime, int longBreakTime, int longBreakInterval);

    void updateAccountSettings(String email, int workTime, int shortBreakTime, int longBreakTime, int longBreakInterval);

    void insertTotalPomodorosByTaskID(int taskID, int totalPomodoros);

    void insertToDoList(String email);

    void setOccupiedTaskByEmailTaskIDOrderEstimatedPomodoros(String email, int taskID, int order, int estimatedTime);

    void unoccupyTaskByTaskID(int taskID);

    void reorderTasksByTaskID(int taskID);

    void updatePauseStatusByConsultantEmail(String email);

    void updateTaskStatusByTaskID(int taskID);

    void updateBreakStatusByConsultantEmail(String email);

    void correctTaskOrdersByConsultantEmail(String email);

    boolean checkIfConsultantStartedByConsultantEmail(String email);

    boolean checkIfConsultantPausedByConsultantEmail(String email);

    boolean checkIfConsultantHasBreakByConsultantEmail(String email);

    boolean checkIfConsultantHasOnGoingTaskByEmail(String email);

    boolean checkIfTaskIsEditableByTaskIDConsultantEmail(int taskID, String email);

    int getTaskEstimatedPomodorosByTaskID(int taskID);

    ConsultantAccountSettings getAccountSettingsByConsultantEmail(String email);

    ConsultantAccount getAllConsultantInfoByEmail(String email);

    List<ConsultantTask> getAllNonProjectTasks();

    List<ConsultantTask> getAllTasksTodayByConsultantEmail(String email);

    List<ConsultantTask> getAllEditableTasksByConsultantEmail(String email);

    List<ConsultantTask> getAllViewableTasksByConsultantEmail(String email);
}
