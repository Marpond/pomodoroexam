package database.DAOInterfaces;

import admin.TableViewProjects;

import java.util.ArrayList;

public interface ProjectDAO {
    ArrayList<TableViewProjects> getAllProjects();

    TableViewProjects getProjectByID(String id);

    void editProject(String id, String name, String description, String deadline);

    void deleteProject(String id);

    void insertProject(TableViewProjects project);

    ArrayList<TableViewProjects> searchProjectsWithAnyParameters(String[] parameters);

}
