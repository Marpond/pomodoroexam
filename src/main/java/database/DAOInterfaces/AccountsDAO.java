package database.DAOInterfaces;

import admin.TableViewAccounts;
import admin.TableViewPermissions;
import admin.records.AdminAccountSelected;

import java.util.ArrayList;
import java.util.List;

public interface AccountsDAO {

    ArrayList<TableViewAccounts> getAccountsForAdminTableView();

    void deleteAccountByEmail(String id);

    void updateAccount(String email, String firstName, String lastName, String phone, String zip, String address, int type, int officeAddress);

    void insertAccount(String email, String firstName, String lastName, String phone, String zip, String address, int officeID, int typeID);

    AdminAccountSelected getAccountByEmail(String email);

    List<Integer> getAllOfficeIDs();

    List<Integer> getAllTypeIDs();

    ArrayList<TableViewPermissions> getPermissionsForAccount(String email);

    void insertPermission(int projectID,String email, boolean edit, boolean view);

    void updatePermission(int permissionID, boolean view, boolean edit);

    ArrayList<TableViewAccounts> searchAccountByString(String searchString);

}
