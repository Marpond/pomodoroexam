package database.DAOInterfaces;

import admin.TableViewTasks;

import java.util.ArrayList;

public interface TaskViewDAO {
    ArrayList<TableViewTasks> getAllTasksForTableView(int projectiD);

    void createTaskInProject(int projectID, String taskName, String description, int priority, int estTime);

    void deleteTaskByTaskID(int taskID);

    void updateTask(int ID, String taskName, String taskDescription, int priority, int estTime);

    ArrayList<TableViewTasks> universalSearchForTask(int projectID, String[] parameters);


}
