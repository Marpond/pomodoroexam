package database.DAOInterfaces;

import admin.ProjectEstimatedTimes;
import admin.TableViewStatistics;
import admin.TaskStatistics;
import admin.WorkTimeOfConsultant;

import java.util.ArrayList;
import java.util.List;

public interface StatisticsDAO {
    ArrayList<TableViewStatistics> getAllProjectsStatistics();

    ArrayList<ProjectEstimatedTimes> getAllProjectEstimatedTimes();

    ArrayList<WorkTimeOfConsultant> getAllConsultantWorkTimes();

    List<Integer> getAllOverDueProjects();

    ArrayList<TaskStatistics> getAllCompletedTasksForAllConsultants();

    ArrayList<TaskStatistics> getAllOverdueTasksForAllConsultants();

    ArrayList<TaskStatistics> getAllNotOverdueTasksForAllConsultants();
}
