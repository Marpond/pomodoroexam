package database.DAOInterfaces;

import java.util.List;

public interface MainDAO {

    int getTypeIDByEmail(String email);

    List<String> getAllAccountEmails();
    boolean checkIfAccountSettingExistsByEmail(String email);

    boolean checkIfAccountIsActivatedByEmail(String email);
}
