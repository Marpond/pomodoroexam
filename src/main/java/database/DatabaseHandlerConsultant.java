package database;

import consultant.records.ConsultantAccount;
import consultant.records.ConsultantAccountSettings;
import consultant.records.ConsultantTask;
import database.DAOInterfaces.ConsultantDAO;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHandlerConsultant extends DatabaseHandler implements ConsultantDAO {

    public DatabaseHandlerConsultant() {
        super();
    }

    public static DatabaseHandlerConsultant getInstance() {
        if (instance == null) {
            instance = new DatabaseHandlerConsultant();
        }
        return (DatabaseHandlerConsultant) instance;
    }

    @Override
    public void insertAccountSettings(String email, int workTime, int shortBreakTime, int longBreakTime, int longBreakInterval) {
        try {
            ps = con.prepareStatement("sp_InsertAccountSettings ?, ?, ?, ?, ?");
            ps.setString(1, email);
            ps.setInt(2, workTime);
            ps.setInt(3, shortBreakTime);
            ps.setInt(4, longBreakTime);
            ps.setInt(5, longBreakInterval);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAccountSettings(String email, int workTime, int shortBreakTime, int longBreakTime, int longBreakInterval) {
        try {
            ps = con.prepareStatement("sp_UpdateAccountSettings ?, ?, ?, ?, ?");
            ps.setString(1, email);
            ps.setInt(2, workTime);
            ps.setInt(3, shortBreakTime);
            ps.setInt(4, longBreakTime);
            ps.setInt(5, longBreakInterval);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertTotalPomodorosByTaskID(int taskID, int totalPomodoros) {
        try {
            ps = con.prepareStatement("sp_InsertTotalPomodorosByTaskID ?, ?");
            ps.setInt(1, taskID);
            ps.setInt(2, totalPomodoros);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertToDoList(String email) {
        try {
            ps = con.prepareStatement("sp_InsertToDoList ?");
            ps.setString(1, email);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOccupiedTaskByEmailTaskIDOrderEstimatedPomodoros(String email, int taskID, int order, int estimatedTime) {
        try {
            ps = con.prepareStatement("sp_SetOccupiedTaskByEmailTaskIDOrderEstimatedPomodoros ?, ?, ?, ?");
            ps.setString(1, email);
            ps.setInt(2, taskID);
            ps.setInt(3, order);
            ps.setInt(4, estimatedTime);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unoccupyTaskByTaskID(int taskID) {
        try {
            ps = con.prepareStatement("sp_UnoccupyTaskByTaskID ?");
            ps.setInt(1, taskID);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reorderTasksByTaskID(int taskID) {
        try {
            ps = con.prepareStatement("sp_ReorderTasksByTaskID ?");
            ps.setInt(1, taskID);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updatePauseStatusByConsultantEmail(String email) {
        try {
            ps = con.prepareStatement("sp_UpdatePauseStatusByConsultantEmail ?");
            ps.setString(1, email);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateTaskStatusByTaskID(int taskID) {
        try {
            ps = con.prepareStatement("sp_UpdateTaskStatusByTaskID ?");
            ps.setInt(1, taskID);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateBreakStatusByConsultantEmail(String email) {
        try {
            ps = con.prepareStatement("sp_UpdateBreakStatusByConsultantEmail ?");
            ps.setString(1, email);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void correctTaskOrdersByConsultantEmail(String email) {
        try {
            ps = con.prepareStatement("sp_CorrectTaskOrdersByConsultantEmail ?");
            ps.setString(1, email);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean checkIfConsultantStartedByConsultantEmail(String email) {
        try {
            ps = con.prepareStatement("sp_CheckIfConsultantStartedByConsultantEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean checkIfConsultantPausedByConsultantEmail(String email) {
        try {
            ps = con.prepareStatement("sp_CheckIfConsultantPausedByConsultantEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean checkIfConsultantHasBreakByConsultantEmail(String email) {
        try {
            ps = con.prepareStatement("sp_CheckIfConsultantHasBreakByConsultantEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean checkIfConsultantHasOnGoingTaskByEmail(String email) {
        try {
            ps = con.prepareStatement("sp_CheckIfConsultantHasOnGoingTaskByEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean checkIfTaskIsEditableByTaskIDConsultantEmail(int taskID, String email) {
        try {
            ps = con.prepareStatement("sp_CheckIfTaskIsEditableByTaskIDConsultantEmail ?, ?");
            ps.setInt(1, taskID);
            ps.setString(2, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public int getTaskEstimatedPomodorosByTaskID(int taskID) {
        try {
            ps = con.prepareStatement("sp_GetTaskEstimatedPomodorosByTaskID ?");
            ps.setInt(1, taskID);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public ConsultantAccountSettings getAccountSettingsByConsultantEmail(String email) {
        try {
            ps = con.prepareStatement("sp_GetAccountSettingsByConsultantEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                int workTime = rs.getInt(1);
                int shortBreakTime = rs.getInt(2);
                int longBreakTime = rs.getInt(3);
                int longBreakInterval = rs.getInt(4);
                return new ConsultantAccountSettings(workTime, shortBreakTime, longBreakTime, longBreakInterval);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ConsultantAccount getAllConsultantInfoByEmail(String email) {
        try {
            ps = con.prepareStatement("sp_GetAllConsultantInfoByEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                String mail = rs.getString(1);
                String fName = rs.getString(2);
                String lName = rs.getString(3);
                String phone = rs.getString(4);
                String zip = rs.getString(5);
                String address = rs.getString(6);
                int officeId = rs.getInt(7);
                return new ConsultantAccount(mail, fName, lName, phone, zip, address, officeId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ConsultantTask> getAllNonProjectTasks() {
        List<ConsultantTask> consultantTasks = new ArrayList<>();
        try {
            ps = con.prepareStatement("sp_GetAllNonProjectTasks");
            rs = ps.executeQuery();
            while (rs.next()) {
                int taskID = rs.getInt(1);
                int projectID = rs.getInt(2);
                String taskName = rs.getString(3);
                String description = rs.getString(5);
                int priority = rs.getInt(6);
                int estimatedTime = rs.getInt(7);
                String startTime = rs.getString(8);
                String endTime = rs.getString(9);
                int order = rs.getInt(10);
                consultantTasks.add(new ConsultantTask(
                        taskID, projectID, taskName, description, priority, estimatedTime, startTime, endTime, order)
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return consultantTasks;
    }

    @Override
    public List<ConsultantTask> getAllTasksTodayByConsultantEmail(String email) {
        try {
            return getAllSpecificTasksByConsultantEmail(
                    con.prepareStatement("sp_GetAllTasksTodayByConsultantEmail ?"), email
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<ConsultantTask> getAllEditableTasksByConsultantEmail(String email) {
        try {
            return getAllSpecificTasksByConsultantEmail(
                    con.prepareStatement("sp_GetAllEditableTasksByConsultantEmail ?"), email
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<ConsultantTask> getAllViewableTasksByConsultantEmail(String email) {
        try {
            return getAllSpecificTasksByConsultantEmail(
                    con.prepareStatement("sp_GetAllViewableTasksByConsultantEmail ?"), email
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private List<ConsultantTask> getAllSpecificTasksByConsultantEmail(
            PreparedStatement ps,
            String email
    ) throws SQLException {
        List<ConsultantTask> consultantTasks = new ArrayList<>();
        ps.setString(1, email);
        rs = ps.executeQuery();
        while (rs.next()) {
            int taskID = rs.getInt(1);
            int projectID = rs.getInt(2);
            String taskName = rs.getString(3);
            String description = rs.getString(5);
            int priority = rs.getInt(6);
            int estimatedTime = rs.getInt(7);
            String startTime = rs.getString(8);
            String endTime = rs.getString(9);
            int order = rs.getInt(10);
            consultantTasks.add(new ConsultantTask(
                    taskID, projectID, taskName, description, priority, estimatedTime, startTime, endTime, order)
            );
        }
        return consultantTasks;
    }
}
