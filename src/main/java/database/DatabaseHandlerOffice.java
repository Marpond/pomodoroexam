package database;

import database.DAOInterfaces.OfficeDAO;
import office.records.*;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandlerOffice extends DatabaseHandler implements OfficeDAO {

    public DatabaseHandlerOffice() {
        super();
    }

    public static DatabaseHandlerOffice getInstance() {
        if (instance == null) {
            instance = new DatabaseHandlerOffice();
        }
        return (DatabaseHandlerOffice) instance;
    }

    @Override
    public boolean checkIfConsultantStartedByConsultantEmail(String email) {
        try {
            ps = con.prepareStatement("sp_CheckIfConsultantStartedByConsultantEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean checkIfConsultantPausedByConsultantEmail(String email) {
        try {
            ps = con.prepareStatement("sp_CheckIfConsultantPausedByConsultantEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean checkIfConsultantHasBreakByConsultantEmail(String email) {
        try {
            ps = con.prepareStatement("sp_CheckIfConsultantHasBreakByConsultantEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public int getOfficeIDByEmail(String email) {
        try {
            ps = con.prepareStatement("sp_GetOfficeIDByEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public String getOfficeNameByID(int officeID) {
        try {
            ps = con.prepareStatement("sp_GetOfficeNameByID ?");
            ps.setInt(1, officeID);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public OfficeAccountSettings getAccountSettingsByConsultantEmail(String email) {
            try {
                ps = con.prepareStatement("sp_GetAccountSettingsByConsultantEmail ?");
                ps.setString(1, email);
                rs = ps.executeQuery();
                if (rs.next()) {
                    int workTime = rs.getInt(1);
                    int shortBreakTime = rs.getInt(2);
                    int longBreakTime = rs.getInt(3);
                    int longBreakInterval = rs.getInt(4);
                    return new OfficeAccountSettings(workTime, shortBreakTime, longBreakTime, longBreakInterval);
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param officeID The office ID of the consultant
     * @return The consultants with the given office ID
     */
    @Override
    public List<OfficeAccount> getAllConsultantInfoByOfficeID(int officeID) {
        List<OfficeAccount> officeAccounts = new ArrayList<>();
        try {
            ps = con.prepareStatement("sp_GetAllConsultantInfoByOfficeID ?");
            ps.setInt(1, officeID);
            rs = ps.executeQuery();
            while (rs.next()) {
                String email = rs.getString(1);
                String firstname = (rs.getString(2));
                String lastName = rs.getString(3);
                String phoneNumber = rs.getString(4);
                officeAccounts.add(
                        new OfficeAccount(email, firstname, lastName, phoneNumber)
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return officeAccounts;
    }

    @Override
    public List<OfficeTask> getAllTasksTodayByConsultantEmail(String email) {
        List<OfficeTask> officeTasks = new ArrayList<>();
        try {
            ps = con.prepareStatement("sp_GetAllTasksTodayByConsultantEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            while (rs.next()) {
                int projectID;
                try {
                    projectID = rs.getInt(2);
                } catch (Exception e) {
                    projectID = 0;
                }
                String taskName = rs.getString(3);
                int toDoListID = rs.getInt(4);
                String description = rs.getString(5);
                int priority = rs.getInt(6);
                int estimatedPomodoros = rs.getInt(7);
                int totalPomodoros = rs.getInt(11);
                java.sql.Time startTime = rs.getTime(8);
                java.sql.Time endTime = rs.getTime(9);
                officeTasks.add(
                        new OfficeTask(projectID, taskName, toDoListID, description,
                                priority, estimatedPomodoros, totalPomodoros, startTime, endTime)
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return officeTasks;
    }

    @Override
    public List<OfficePause> getAllPausesTodayByConsultantEmail(String email) {
        List<OfficePause> officePauses = new ArrayList<>();
        try {
            ps = con.prepareStatement("sp_GetAllPausesTodayByConsultantEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            while (rs.next()) {
                java.sql.Time startTime = rs.getTime(3);
                java.sql.Time endTime = rs.getTime(4);
                officePauses.add(
                        new OfficePause(startTime, endTime)
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return officePauses;
    }

    @Override
    public List<OfficeBreak> getAllBreaksTodayByConsultantEmail(String email) {
        List<OfficeBreak> officeBreaks = new ArrayList<>();
        try {
            ps = con.prepareStatement("sp_GetAllBreaksTodayByConsultantEmail ?");
            ps.setString(1, email);
            rs = ps.executeQuery();
            while (rs.next()) {
                java.sql.Time startTime = rs.getTime(3);
                java.sql.Time endTime = rs.getTime(4);
                officeBreaks.add(
                        new OfficeBreak(startTime, endTime)
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return officeBreaks;
    }

}
