USE [db_pomodoro]

GO

/****** Object:  StoredProcedure [dbo].[sp_CheckIfAccountIsActivatedByEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_CheckIfAccountIsActivatedByEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    SELECT fld_IsActivated

    FROM tbl_AccountSettings

    WHERE fld_AccountEmail = @email

END

GO

/****** Object:  StoredProcedure [dbo].[sp_CheckIfAccountSettingExistsByEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_CheckIfAccountSettingExistsByEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    IF EXISTS(SELECT * FROM tbl_AccountSettings WHERE fld_AccountEmail = @email)

        BEGIN

            SELECT 1

        END

    ELSE

        BEGIN

            SELECT 0

        END

END

GO

/****** Object:  StoredProcedure [dbo].[sp_CheckIfConsultantHasBreakByConsultantEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_CheckIfConsultantHasBreakByConsultantEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    IF EXISTS(SELECT * FROM tbl_Breaks WHERE fld_AccountEmail = @email AND fld_BreakEnd IS NULL)

        BEGIN

            SELECT 1

        END

    ELSE

        BEGIN

            SELECT 0

        END

END

GO

/****** Object:  StoredProcedure [dbo].[sp_CheckIfConsultantHasOnGoingTaskByEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_CheckIfConsultantHasOnGoingTaskByEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    DECLARE @today DATE;

    SET @today = GETDATE();

    DECLARE @toDoListID INT;

    SELECT @toDoListID = fld_ToDoListID

    FROM tbl_ToDoLists

    WHERE fld_AccountEmail = @email

      AND fld_Date = @today;

    IF EXISTS(SELECT *

              FROM tbl_Tasks

              WHERE fld_ToDoListID = @toDoListID

                AND fld_StartTime IS NOT NULL

                AND fld_FinishTime IS NULL)
        SELECT 1

    ELSE

        SELECT 0

END

GO

/****** Object:  StoredProcedure [dbo].[sp_CheckIfConsultantPausedByConsultantEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_CheckIfConsultantPausedByConsultantEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    IF EXISTS(SELECT * FROM tbl_Pauses WHERE fld_AccountEmail = @email AND fld_PauseEnd IS NULL)

        BEGIN

            SELECT 1

        END

    ELSE

        BEGIN

            SELECT 0

        END

END

GO

/****** Object:  StoredProcedure [dbo].[sp_CheckIfConsultantStartedByConsultantEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_CheckIfConsultantStartedByConsultantEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    DECLARE @today DATE;

    SET @today = GETDATE();

    DECLARE @toDoListID INT;

    SELECT @toDoListID = fld_ToDoListID

    FROM tbl_ToDoLists

    WHERE fld_AccountEmail = @email

      AND fld_Date = @today;

    IF EXISTS(SELECT * FROM tbl_Tasks WHERE fld_ToDoListID = @toDoListID AND fld_StartTime IS NOT NULL)

        BEGIN

            SELECT 1;

        END

    ELSE

        BEGIN

            SELECT 0;

        END

END

GO

/****** Object:  StoredProcedure [dbo].[sp_CheckIfTaskIsEditableByTaskIDConsultantEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_CheckIfTaskIsEditableByTaskIDConsultantEmail](
    @taskID INT,
    @consultantEmail VARCHAR(50)
)
AS

BEGIN

    IF EXISTS(SELECT *
              FROM tbl_Tasks

                       INNER JOIN tbl_Permissions tP ON
                      (tbl_Tasks.fld_ProjectID = tP.fld_ProjectID
                          AND tP.fld_Edit = 1
                          AND tP.fld_AccountEmail = @consultantEmail)
                      OR tbl_Tasks.fld_ProjectID IS NULL

              WHERE tbl_Tasks.fld_TaskID = @taskID)
        BEGIN

            SELECT 1

        END

    ELSE

        BEGIN

            SELECT 0

        END

END

GO

/****** Object:  StoredProcedure [dbo].[sp_CorrectTaskOrdersByConsultantEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_CorrectTaskOrdersByConsultantEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    DECLARE @today DATE;

    SET @today = GETDATE();

    DECLARE @toDoListID INT;

    SELECT @toDoListID = fld_ToDoListID

    FROM tbl_ToDoLists

    WHERE fld_AccountEmail = @email

      AND fld_Date = @today;

    --Store all the tasks in a temporary table with their current order

    DECLARE @taskList TABLE
                      (

                          Row#       INT,

                          fld_TaskID INT,

                          fld_Order  INT

                      );

    INSERT INTO @taskList (Row#, fld_TaskID, fld_Order)

    SELECT Row_Number() OVER (ORDER BY fld_Order), fld_TaskID, fld_Order

    FROM tbl_Tasks

    WHERE fld_ToDoListID = @toDoListID;

    --Update the order of the tasks in the database

    DECLARE @i INT;

    SET @i = 1;

    WHILE @i <= (SELECT COUNT(*) FROM @taskList)
        BEGIN

            UPDATE tbl_Tasks

            SET fld_Order = @i

            WHERE fld_TaskID = (SELECT fld_TaskID FROM @taskList WHERE Row# = @i);

            SET @i = @i + 1;

        END

END

GO

/****** Object:  StoredProcedure [dbo].[sp_CreateTaskInProject]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_CreateTaskInProject](
    @projectID INT,
    @taskName VARCHAR(50),
    @taskDescription VARCHAR(50),
    @priority INT,
    @estTime INT
)
AS

BEGIN

    INSERT INTO tbl_Tasks

    (fld_ProjectID,
     fld_TaskName,
     fld_Description,
     fld_Priority,
     fld_EstimatedPomodoros)

    VALUES (@projectID,
            @taskName,
            @taskDescriptioN,
            @priority,
            @estTime)

END

GO

/****** Object:  StoredProcedure [dbo].[sp_DeleteAccount]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE procedure [dbo].[sp_DeleteAccount](
    @AccountEmail varchar(50)
)
as

begin

    delete T1
    from tbl_Tasks T1
             inner join tbl_ToDoLists T2 on T1.fld_ToDoListID = T2.fld_ToDoListID
    where fld_AccountEmail = @AccountEmail;

    delete from tbl_Permissions where fld_AccountEmail = @AccountEmail;

    delete from tbl_Breaks where fld_AccountEmail = @AccountEmail;

    delete from tbl_ToDoLists where fld_AccountEmail = @AccountEmail

    delete from tbl_Pauses where fld_AccountEmail = @AccountEmail;

    delete from tbl_AccountSettings where fld_AccountEmail = @AccountEmail

    delete from tbl_Accounts where fld_AccountEmail = @AccountEmail

end

GO

/****** Object:  StoredProcedure [dbo].[sp_DeleteProject]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

-- Create procedure to delete a project

-- It removes from the table, the tasks and permissions associated with the project

create procedure [dbo].[sp_DeleteProject](
    @ProjectID int
)
as

begin

    -- Delete the tasks associated with the project

    delete from tbl_Tasks where fld_ProjectID = @ProjectID;

    -- Delete the permissions associated with the project

    delete from tbl_Permissions where fld_ProjectID = @ProjectID;

    -- Delete the project

    delete from tbl_Projects where fld_ProjectID = @ProjectID;

end

GO

/****** Object:  StoredProcedure [dbo].[sp_DeleteTaskByTaskID]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_DeleteTaskByTaskID](
    @taskID int
)
AS

BEGIN

    DELETE FROM tbl_Tasks WHERE fld_TaskID = @taskID

END

GO

/****** Object:  StoredProcedure [dbo].[sp_EditAccount]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

Create procedure [dbo].[sp_EditAccount](
    @AccountEmail varchar(50),
    @FirstName varchar(20),
    @LastName varchar(20),
    @PhoneNumber varchar(8),
    @Zip varchar(4),
    @Address varchar(50),
    @OfficeID int,
    @TypeID int
)
AS

BEGIN

    UPDATE tbl_Accounts

    SET fld_FirstName   = @FirstName,

        fld_LastName    = @LastName,

        fld_PhoneNumber = @PhoneNumber,

        fld_Zip         = @Zip,

        fld_Address     = @Address,

        fld_OfficeID    = @OfficeID,

        fld_TypeID      = @TypeID

    WHERE fld_AccountEmail = @AccountEmail;

END;

GO

/****** Object:  StoredProcedure [dbo].[sp_EditProject]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_EditProject](
    @projectID INT,
    @name VARCHAR(50),
    @description VARCHAR(50),
    @deadline datetime
)
AS

BEGIN

    UPDATE tbl_Projects

    SET fld_ProjectName = @name,

        fld_Description = @description,

        fld_Deadline    = @deadline

    WHERE fld_ProjectID = @projectID

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAccountByEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAccountByEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    SELECT *

    FROM tbl_Accounts

    WHERE fld_AccountEmail = @email

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAccountSettingsByConsultantEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAccountSettingsByConsultantEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    SELECT fld_WorkTime, fld_ShortBreakTime, fld_LongBreakTime, fld_LongBreakInterval

    FROM tbl_AccountSettings

    WHERE fld_AccountEmail = @email

      AND fld_IsActivated = 1

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAccountsForAdminTableView]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE procedure [dbo].[sp_GetAccountsForAdminTableView]
as

begin

    select fld_FirstName, fld_LastName, fld_AccountEmail, fld_PhoneNumber, fld_RoleDescription, tbl_Offices.fld_Address

    from tbl_Offices

             inner join tbl_Accounts
                        on tbl_Offices.fld_OfficeID = tbl_Accounts.fld_OfficeID

             inner join tbl_AccountTypes
                        on tbl_Accounts.fld_TypeID = tbl_AccountTypes.fld_TypeID

end

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllAccountEmails]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAllAccountEmails]
AS

BEGIN

    SELECT fld_AccountEmail FROM tbl_Accounts

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllBreaksTodayByConsultantEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAllBreaksTodayByConsultantEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    DECLARE @today DATE = GETDATE();

    SELECT *

    FROM tbl_Breaks

    WHERE fld_AccountEmail = @email

      AND fld_BreakStart >= @today

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllConsultantInfoByEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAllConsultantInfoByEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    SELECT *

    FROM tbl_Accounts

    WHERE fld_AccountEmail = @email

      AND fld_TypeID = 1;

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllConsultantInfoByOfficeID]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAllConsultantInfoByOfficeID](
    @officeID int
)
AS

BEGIN

    SELECT *

    FROM tbl_Accounts

    WHERE fld_OfficeID = @officeID

      AND fld_TypeID = 1

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllEditableTasksByConsultantEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAllEditableTasksByConsultantEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    SELECT *

    FROM tbl_Tasks

             INNER JOIN tbl_Permissions
                        ON tbl_Tasks.fld_ProjectID = tbl_Permissions.fld_ProjectID

    WHERE tbl_Permissions.fld_AccountEmail = @email

      AND tbl_Permissions.fld_Edit = 1

      AND tbl_Tasks.fld_ToDoListID IS NULL

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllNonProjectTasks]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAllNonProjectTasks]
AS

BEGIN

    SELECT * FROM tbl_Tasks WHERE fld_ProjectID IS NULL AND fld_ToDoListID IS NULL

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllNotOverDueTasksForAllConsultants]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAllNotOverDueTasksForAllConsultants]
AS

BEGIN

    select COUNT(fld_TaskID) as 'not overdue tasks',

           T4.fld_AccountEmail

    from tbl_Tasks T1

             inner join tbl_Projects T2 on T1.fld_ProjectID = T2.fld_ProjectID

             inner join tbl_ToDoLists T3 on T3.fld_ToDoListID = T1.fld_ToDoListID

             inner join tbl_Accounts T4 on T4.fld_AccountEmail = T3.fld_AccountEmail

    where fld_FinishTime <= fld_Deadline

      and fld_FinishTime is not null

    group by T4.fld_AccountEmail

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllOfficeIDs]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAllOfficeIDs]
AS

BEGIN

    SELECT fld_OfficeID FROM tbl_Offices

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllOverDueProjects]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAllOverDueProjects]
AS

BEGIN

    DECLARE @projectWithNoTask TABLE
                               (

                                   fld_ProjectId INT

                               );

    DECLARE @projectWithTask TABLE
                             (

                                 fld_ProjectId INT

                             );

    INSERT INTO @projectWithNoTask (fld_ProjectId)

    SELECT fld_ProjectId

    FROM tbl_Projects

    WHERE fld_ProjectId NOT IN (SELECT fld_ProjectId FROM tbl_Tasks)

      AND fld_Deadline < GETDATE();

    INSERT INTO @projectWithTask (fld_ProjectId)

    SELECT fld_ProjectId

    FROM tbl_Projects

    WHERE fld_ProjectId IN (SELECT fld_ProjectId

                            FROM tbl_Tasks

                            WHERE fld_FinishTime > tbl_Projects.fld_Deadline

                               OR fld_FinishTime IS NULL)

      AND fld_Deadline < GETDATE();

    SELECT *

    FROM @projectWithNoTask

    UNION ALL

    SELECT *

    FROM @projectWithTask

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllOverDueTasksForAllConsultants]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAllOverDueTasksForAllConsultants]
AS

BEGIN

    select COUNT(fld_TaskID) as 'Over due tasks',

           T4.fld_AccountEmail

    from tbl_Tasks T1

             inner join tbl_Projects T2 on T1.fld_ProjectID = T2.fld_ProjectID

             inner join tbl_ToDoLists T3 on T3.fld_ToDoListID = T1.fld_ToDoListID

             inner join tbl_Accounts T4 on T4.fld_AccountEmail = T3.fld_AccountEmail

    where fld_FinishTime > fld_Deadline

    group by T4.fld_AccountEmail

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllPausesTodayByConsultantEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAllPausesTodayByConsultantEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    DECLARE @today DATE = GETDATE();

    SELECT *

    FROM tbl_Pauses

    WHERE fld_AccountEmail = @email

      AND fld_PauseStart >= @today

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllTasksForTableView]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAllTasksForTableView](
    @projectID INT
)
AS

BEGIN

    SELECT T1.fld_ProjectID,

           fld_TaskID,

           fld_TaskName,

           T2.fld_description,

           T1.fld_ProjectName,

           fld_Priority,

           fld_AccountEmail,

           fld_EstimatedPomodoros,

           fld_Date

    FROM tbl_Projects T1

             inner join
         tbl_Tasks T2
         ON
             T1.fld_ProjectID = T2.fld_ProjectID

             LEFT JOIN
         tbl_ToDoLists T3
         ON
             T2.fld_ToDoListID = T3.fld_ToDoListID

    WHERE T1.fld_ProjectID = @projectID

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllTasksTodayByConsultantEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAllTasksTodayByConsultantEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    DECLARE @today DATE = GETDATE();

    SELECT *

    FROM tbl_Tasks

             INNER JOIN tbl_ToDoLists
                        ON tbl_Tasks.fld_ToDoListID = tbl_ToDoLists.fld_ToDoListID

    WHERE tbl_ToDoLists.fld_Date = @today

      AND tbl_ToDoLists.fld_AccountEmail = @email

    ORDER BY fld_Order ASC

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllTypeIDs]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAllTypeIDs]
AS

BEGIN

    SELECT fld_TypeID FROM tbl_AccountTypes

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetAllViewableTasksByConsultantEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetAllViewableTasksByConsultantEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    SELECT *

    FROM tbl_Tasks

             INNER JOIN tbl_Permissions
                        ON tbl_Tasks.fld_ProjectID = tbl_Permissions.fld_ProjectID

    WHERE tbl_Permissions.fld_AccountEmail = @email

      AND tbl_Permissions.fld_View = 1

      AND tbl_Permissions.fld_Edit = 0

      AND tbl_Tasks.fld_ToDoListID IS NULL

END

GO

/****** Object:  StoredProcedure [dbo].[SP_GetCompletedTasksOfAllConsultants]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[SP_GetCompletedTasksOfAllConsultants]
AS

BEGIN

    select COUNT(fld_TaskID) as 'Completed tasks',

           T4.fld_AccountEmail

    from tbl_Tasks T1

             inner join tbl_Projects T2 on T1.fld_ProjectID = T2.fld_ProjectID

             inner join tbl_ToDoLists T3 on T3.fld_ToDoListID = T1.fld_ToDoListID

             inner join tbl_Accounts T4 on T4.fld_AccountEmail = T3.fld_AccountEmail

    where fld_FinishTime is not null

    group by T4.fld_AccountEmail

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetOfficeIDByEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetOfficeIDByEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    SELECT fld_OfficeID

    FROM tbl_Accounts

    WHERE fld_AccountEmail = @email

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetOfficeNameByID]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetOfficeNameByID](
    @officeID INT
)
AS

BEGIN

    SELECT fld_OfficeName

    FROM tbl_Offices

    WHERE fld_OfficeID = @officeID

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetPermissionsForAccount]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE procedure [dbo].[sp_GetPermissionsForAccount](
    @email VARCHAR(50))
AS

BEGIN

    SELECT T4.fld_PermissionID,

           T3.fld_ProjectID,

           T3.fld_ProjectName,

           T4.fld_Edit,

           T4.fld_View

    FROM tbl_Projects T3

             left join

         (SELECT T1.fld_ProjectID,

                 T2.fld_View,

                 T2.fld_Edit,

                 T2.fld_PermissionID

          FROM tbl_Projects T1

                   inner join
               tbl_Permissions T2
               ON
                   T1.fld_ProjectID = T2.fld_ProjectID

          WHERE fld_AccountEmail = @email) T4
         ON
             T3.fld_ProjectID = T4.fld_ProjectID

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetProjectByID]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

create procedure [dbo].[sp_GetProjectByID](
    @ProjectID int
)
as

begin

    select * from tbl_Projects where fld_ProjectID = @ProjectID

end

GO

/****** Object:  StoredProcedure [dbo].[sp_GetProjectsByTotalEstimatedTime]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetProjectsByTotalEstimatedTime]
AS

BEGIN

    SELECT tbl_Projects.fld_ProjectName,

           SUM(fld_EstimatedPomodoros)

    FROM tbl_Tasks

             inner join
         tbl_Projects
         ON
             tbl_Tasks.fld_ProjectID = tbl_Projects.fld_ProjectID

    GROUP BY tbl_Projects.fld_ProjectName

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetTaskEstimatedPomodorosByTaskID]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetTaskEstimatedPomodorosByTaskID](
    @taskID INT
)
AS

BEGIN

    SELECT fld_EstimatedPomodoros

    FROM tbl_Tasks

    WHERE fld_TaskID = @taskID

END

GO

/****** Object:  StoredProcedure [dbo].[sp_GetTypeIDByEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_GetTypeIDByEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    SELECT fld_TypeID

    FROM tbl_Accounts

    WHERE fld_AccountEmail = @email

END

GO

/****** Object:  StoredProcedure [dbo].[sp_InsertAccount]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_InsertAccount](
    @AccountEmail varchar(50),
    @FirstName varchar(20),
    @LastName varchar(20),
    @PhoneNumber varchar(8),
    @Zip varchar(4),
    @Address varchar(50),
    @OfficeID int,
    @TypeID int
)
AS

BEGIN

    INSERT INTO tbl_Accounts(fld_AccountEmail,
                             fld_FirstName,
                             fld_LastName,
                             fld_PhoneNumber,
                             fld_Zip,
                             fld_Address,
                             fld_OfficeID,
                             fld_TypeID)

    VALUES (@AccountEmail,
            @FirstName,
            @LastName,
            @PhoneNumber,
            @Zip,
            @Address,
            @OfficeID,
            @TypeID);

END

GO

/****** Object:  StoredProcedure [dbo].[sp_InsertAccountSettings]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_InsertAccountSettings](
    @email VARCHAR(50),
    @workTime INT,
    @shortBreakTime INT,
    @longBreakTime INT,
    @longBreakInterval INT
)
AS

BEGIN

    INSERT INTO tbl_AccountSettings

    VALUES (@email, @workTime, @shortBreakTime, @longBreakTime, @longBreakInterval, 1)

END

GO

/****** Object:  StoredProcedure [dbo].[sp_InsertPermission]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE procedure [dbo].[sp_InsertPermission](
    @projectID int,
    @email varchar(50),
    @view bit,
    @edit bit)
as

begin

    insert into tbl_Permissions(fld_ProjectID, fld_AccountEmail, fld_View, fld_Edit)

    values (@projectID, @email, @view, @edit);

end

GO

/****** Object:  StoredProcedure [dbo].[sp_InsertProject]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE procedure [dbo].[sp_InsertProject](
    @Description varchar(50),
    @Deadline date,
    @Name varchar(50)
)
as

begin

    insert into tbl_Projects (fld_Description, fld_Deadline, fld_ProjectName)

    values (@Description, @Deadline, @Name);

end

GO

/****** Object:  StoredProcedure [dbo].[sp_InsertTestDataOfficeWithEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_InsertTestDataOfficeWithEmail](
    @Email VARCHAR(50),
    @TaskAmount INT
)
AS

BEGIN

    EXEC sp_InsertToDoList @Email;

    DECLARE @ToDoListID INT;

    SELECT @ToDoListID = MAX(fld_ToDoListID) FROM tbl_ToDoLists WHERE fld_AccountEmail = @Email;

    DECLARE @ProjectID INT;

    SELECT @ProjectID = MAX(tbl_Projects.fld_ProjectID)

    FROM tbl_Projects

             INNER JOIN tbl_Permissions
                        ON tbl_Projects.fld_ProjectID = tbl_Permissions.fld_ProjectID

    WHERE tbl_Permissions.fld_AccountEmail = @Email

      AND tbl_Permissions.fld_Edit = 1;

    DECLARE @Today DATETIME = DATEADD(HOUR, 2, GETDATE());

    DECLARE @WorkTime INT;

    SELECT @WorkTime = fld_WorkTime FROM tbl_AccountSettings WHERE fld_AccountEmail = @Email;

    DECLARE @ShortBreakTime INT;

    SELECT @ShortBreakTime = fld_ShortBreakTime FROM tbl_AccountSettings WHERE fld_AccountEmail = @Email;

    DECLARE @LongBreakTime INT;

    SELECT @LongBreakTime = fld_LongBreakTime FROM tbl_AccountSettings WHERE fld_AccountEmail = @Email;

    DECLARE @LongBreakInterval INT;

    SELECT @LongBreakInterval = fld_LongBreakInterval FROM tbl_AccountSettings WHERE fld_AccountEmail = @Email;

    DECLARE @InsertCount INT = 1;

    --Insert tasks

    WHILE @InsertCount < @TaskAmount
        BEGIN

            --Insert available times

            DECLARE @i INT = 1;

            DECLARE @FirstBreakTimeForCurrentTask DATETIME;

            WHILE @i <= @InsertCount
                BEGIN

                    DECLARE @BreakCount INT;

                    SELECT @BreakCount = COUNT(*)

                    FROM tbl_Breaks

                    WHERE fld_AccountEmail = @Email

                      AND fld_BreakEnd >= @Today;

                    DECLARE @IsShortBreak BIT;

                    DECLARE @IsLongBreak BIT;

                    IF ((@BreakCount + 1) % @LongBreakInterval = 0)
                        BEGIN

                            SET @IsLongBreak = 1;

                            SET @IsShortBreak = 0;

                        END

                    ELSE

                        BEGIN

                            SET @IsLongBreak = 0;

                            SET @IsShortBreak = 1;

                        END

                    DECLARE @AvailableStart DATETIME;

                    SELECT @AvailableStart = MAX(fld_BreakEnd)

                    FROM tbl_Breaks

                    WHERE fld_AccountEmail = @Email

                      AND fld_BreakEnd >= @Today;

                    IF (@AvailableStart IS NULL)
                        BEGIN

                            SET @AvailableStart = DATEADD(
                                    MINUTE,
                                    @WorkTime,
                                    @Today
                                );

                        END

                    ELSE

                        BEGIN

                            SET @AvailableStart = DATEADD(
                                    MINUTE,
                                    @WorkTime,
                                    @AvailableStart
                                );

                        END

                    IF (@FirstBreakTimeForCurrentTask IS NULL)
                        BEGIN

                            SET @FirstBreakTimeForCurrentTask = @AvailableStart;

                        END

                    DECLARE @AvailableFinish DATETIME = DATEADD(
                            MINUTE,
                            (@ShortBreakTime * @IsShortBreak) +
                            (@LongBreakTime * @IsLongBreak),
                            @AvailableStart
                        );

                    INSERT INTO tbl_Breaks(fld_AccountEmail, fld_BreakStart, fld_BreakEnd)

                    VALUES (@Email, @AvailableStart, @AvailableFinish);

                    SET @i = @i + 1;

                END

            DECLARE @TaskStart DATETIME;

            DECLARE @TaskFinish DATETIME;

            SELECT @TaskFinish = MAX(fld_BreakStart)

            FROM tbl_Breaks

            WHERE fld_AccountEmail = @Email

              AND fld_BreakEnd >= @Today;


            SELECT @TaskStart = DATEADD(
                    MINUTE,
                    @WorkTime * -1,
                    @FirstBreakTimeForCurrentTask
                );

            SET @FirstBreakTimeForCurrentTask = NULL;

            INSERT INTO tbl_Tasks(fld_ProjectID,
                                  fld_TaskName,
                                  fld_ToDoListID,
                                  fld_Description,
                                  fld_Priority,
                                  fld_EstimatedPomodoros,
                                  fld_StartTime,
                                  fld_FinishTime,
                                  fld_Order,
                                  fld_TotalPomodoros)

            VALUES (@ProjectID,
                    'Task' + CAST(@InsertCount AS VARCHAR(10)),
                    @ToDoListID,
                    'Description' + CAST(@InsertCount AS VARCHAR(10)),
                    CAST(RAND() * 3 AS INT),
                    @InsertCount,
                    @TaskStart,
                    @TaskFinish,
                    @InsertCount,
                    @InsertCount);

            SET @InsertCount = @InsertCount + 1;

        END

END

GO

/****** Object:  StoredProcedure [dbo].[sp_InsertToDoList]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_InsertToDoList](
    @email VARCHAR(50)
)
AS

BEGIN

    DECLARE @today DATE;

    SET @today = GETDATE();

    IF NOT EXISTS(SELECT * FROM tbl_ToDoLists WHERE fld_AccountEmail = @email AND fld_Date = @today)
        BEGIN

            INSERT INTO tbl_ToDoLists

            VALUES (@email, @today)

        END

END

GO

/****** Object:  StoredProcedure [dbo].[sp_InsertTotalPomodorosByTaskID]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_InsertTotalPomodorosByTaskID](
    @taskID INT,
    @totalPomodoros INT
)
AS

BEGIN

    UPDATE tbl_Tasks

    SET fld_TotalPomodoros = @totalPomodoros

    WHERE fld_TaskID = @taskID;

END

GO

/****** Object:  StoredProcedure [dbo].[sp_RemoveAllUnfinishedTasksFromToDoList]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_RemoveAllUnfinishedTasksFromToDoList]
AS

BEGIN

    DECLARE @yesterday DATE;

    SET @yesterday = DATEADD(DAY, -1, GETDATE());

    UPDATE tbl_Tasks

    SET fld_ToDoListID         = NULL,

        fld_Order              = NULL,

        fld_EstimatedPomodoros = NULL

    WHERE fld_ToDoListID IN (SELECT fld_ToDoListID FROM tbl_ToDoLists WHERE fld_Date = @yesterday)

      AND fld_FinishTime IS NULL;

END

GO

/****** Object:  StoredProcedure [dbo].[sp_ReorderTasksByTaskID]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_ReorderTasksByTaskID](
    @taskID INT
)
AS

BEGIN

    DECLARE @order INT;

    SELECT @order = fld_Order

    FROM tbl_Tasks

    WHERE fld_TaskID = @taskID;

    -- Get the ToDoListID

    DECLARE @toDoListID INT;

    SELECT @toDoListID = fld_ToDoListID

    FROM tbl_Tasks

    WHERE fld_TaskID = @taskID;

    -- If there are 2 tasks with the same order

    IF 1 < (SELECT COUNT(*) FROM tbl_Tasks WHERE fld_ToDoListID = @toDoListID AND fld_Order = @order)
        BEGIN

            DECLARE @taskToReorderID INT;

            SELECT @taskToReorderID = fld_TaskID

            FROM tbl_Tasks

            WHERE fld_ToDoListID = @toDoListID

              AND fld_Order = @order

              AND fld_TaskID <> @taskID;

            UPDATE tbl_Tasks

            SET fld_Order = @order + 1

            WHERE fld_TaskID = @taskToReorderID;

            EXEC sp_ReorderTasksByTaskID @taskToReorderID;

        END

END

GO

/****** Object:  StoredProcedure [dbo].[sp_SearchAccountByEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

create procedure [dbo].[sp_SearchAccountByEmail](
    @AccountEmail varchar(50)
)
as

begin

    select * from tbl_Accounts where fld_AccountEmail = @AccountEmail

end

GO

/****** Object:  StoredProcedure [dbo].[sp_SearchAccountByString]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

create procedure [dbo].[sp_SearchAccountByString](@searchString varchar(50))
as

begin

    select fld_FirstName, fld_LastName, fld_AccountEmail, fld_PhoneNumber, fld_RoleDescription, tbl_Offices.fld_Address

    from tbl_Offices

             inner join tbl_Accounts
                        on tbl_Offices.fld_OfficeID = tbl_Accounts.fld_OfficeID

             inner join tbl_AccountTypes
                        on tbl_Accounts.fld_TypeID = tbl_AccountTypes.fld_TypeID

    where fld_FirstName like '%' + @searchString + '%'
       or fld_LastName like '%' + @searchString + '%'
       or fld_AccountEmail like '%' + @searchString + '%'

end

GO

/****** Object:  StoredProcedure [dbo].[sp_searchProjectsByName]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_searchProjectsByName](
    @name VARCHAR(50)
)
AS

BEGIN

    SELECT * FROM tbl_Projects WHERE fld_ProjectName LIKE '%' + @name + '%'

END

GO

/****** Object:  StoredProcedure [dbo].[sp_SetOccupiedTaskByEmailTaskIDOrderEstimatedPomodoros]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_SetOccupiedTaskByEmailTaskIDOrderEstimatedPomodoros](
    @email VARCHAR(50),
    @taskID INT,
    @order INT,
    @estimatedTime INT
)
AS

BEGIN

    -- Get today

    DECLARE @today DATE;

    SET @today = GETDATE();

    -- Get the ToDoListID

    DECLARE @toDoListID INT;

    SELECT @toDoListID = fld_ToDoListID

    FROM tbl_ToDoLists

    WHERE fld_AccountEmail = @email

      AND fld_Date = @today;

    -- Set the task's new values

    UPDATE tbl_Tasks

    SET fld_ToDoListID         = @toDoListID,

        fld_Order              = @order,

        fld_EstimatedPomodoros = @estimatedTime

    WHERE fld_TaskID = @taskID

END

GO

/****** Object:  StoredProcedure [dbo].[sp_UniversalSearchForTask]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_UniversalSearchForTask](
    @projectID INT,
    @taskName VARCHAR(50),
    @taskDescription VARCHAR(50),
    @email VARCHAR(50),
    @startDate DATETIME = '1901-01-01',
    @endDate DATETIME = '2030-01-01'
)
AS

BEGIN

    SELECT T1.fld_ProjectID,

           T2.fld_TaskID,

           T2.fld_TaskName,

           T2.fld_Description,

           T1.fld_ProjectName,

           T2.fld_Priority,

           T3.fld_AccountEmail,

           T2.fld_EstimatedPomodoros,

           T3.fld_Date

    FROM tbl_Projects T1

             INNER JOIN
         tbl_Tasks T2
         ON
             T1.fld_ProjectID = T2.fld_ProjectID

             LEFT JOIN
         tbl_ToDoLists T3
         ON
             T2.fld_ToDoListID = T3.fld_ToDoListID

    WHERE T2.fld_TaskName LIKE '%' + @taskName + '%'

      AND T2.fld_Description LIKE '%' + @taskDescription + '%'

      AND (T3.fld_AccountEmail IS NULL or T3.fld_AccountEmail LIKE '%' + @email + '%')

      AND ((T3.fld_Date >= @startDate AND T3.fld_Date <= @endDate) OR T3.fld_Date IS NULL)

      AND T1.fld_ProjectID = @projectID

END

GO

/****** Object:  StoredProcedure [dbo].[sp_UnoccupyTaskByTaskID]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_UnoccupyTaskByTaskID](
    @taskID INT
)
AS

BEGIN

    UPDATE tbl_Tasks

    SET fld_ToDoListID         = NULL,

        fld_Order              = NULL,

        fld_EstimatedPomodoros = NULL

    WHERE fld_TaskID = @taskID

END

GO

/****** Object:  StoredProcedure [dbo].[sp_UpdateAccount]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_UpdateAccount](
    @email VARCHAR(50),
    @firstName VARCHAR(20),
    @lastName VARCHAR(20),
    @phoneNumber VARCHAR(8),
    @zip VARCHAR(4),
    @address VARCHAR(50),
    @officeID INT,
    @typeID INT
)
AS

BEGIN

    UPDATE tbl_Accounts

    SET fld_FirstName   = @firstName,

        fld_LastName    = @lastName,

        fld_PhoneNumber = @phoneNumber,

        fld_Zip         = @zip,

        fld_Address     = @address,

        fld_OfficeID    = @officeID,

        fld_TypeID      = @typeID

    WHERE fld_AccountEmail = @email;

END;

GO

/****** Object:  StoredProcedure [dbo].[sp_UpdateAccountSettings]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_UpdateAccountSettings](
    @email VARCHAR(50),
    @workTime INT,
    @shortBreakTime INT,
    @longBreakTime INT,
    @longBreakInterval INT
)
AS

BEGIN

    UPDATE tbl_AccountSettings

    SET fld_WorkTime          = @workTime,

        fld_ShortBreakTime    = @shortBreakTime,

        fld_LongBreakTime     = @longBreakTime,

        fld_LongBreakInterval = @longBreakInterval

    WHERE fld_AccountEmail = @email

END

GO

/****** Object:  StoredProcedure [dbo].[sp_UpdateBreakStatusByConsultantEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_UpdateBreakStatusByConsultantEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    DECLARE @now DATETIME = GETDATE();

    DECLARE @nowUTC DATETIME = DATEADD(HOUR, 2, @now);

    IF NOT EXISTS(SELECT * FROM tbl_Breaks WHERE fld_AccountEmail = @email AND fld_BreakEnd IS NULL)
        BEGIN

            INSERT INTO tbl_Breaks (fld_AccountEmail, fld_BreakStart)

            VALUES (@email, @nowUTC);

        END

    ELSE

        BEGIN

            UPDATE tbl_Breaks

            SET fld_BreakEnd = @nowUTC

            WHERE fld_AccountEmail = @email

              AND fld_BreakEnd IS NULL;

        END

END

GO

/****** Object:  StoredProcedure [dbo].[sp_UpdatePauseStatusByConsultantEmail]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_UpdatePauseStatusByConsultantEmail](
    @email VARCHAR(50)
)
AS

BEGIN

    -- Get the datetime in copenhagen timezone

    DECLARE @now DATETIME = GETDATE();

    DECLARE @nowUTC DATETIME = DATEADD(HOUR, 2, @now);

    IF EXISTS(SELECT * FROM tbl_Pauses WHERE fld_AccountEmail = @email AND fld_PauseEnd IS NULL)
        BEGIN

            UPDATE tbl_Pauses

            SET fld_PauseEnd = @nowUTC

            WHERE fld_AccountEmail = @email

              AND fld_PauseEnd IS NULL

        END

    ELSE

        BEGIN

            INSERT INTO tbl_Pauses(fld_AccountEmail, fld_PauseStart)

            VALUES (@email, @nowUTC)

        END

END

GO

/****** Object:  StoredProcedure [dbo].[sp_UpdatePermission]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE procedure [dbo].[sp_UpdatePermission](
    @PermissionID int,
    @view bit,
    @edit bit)
as

begin

    update tbl_Permissions
    set fld_View = @view,

        fld_Edit = @edit

    where fld_PermissionID = @PermissionID

end

GO

/****** Object:  StoredProcedure [dbo].[sp_UpdateTask]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_UpdateTask](
    @taskID INT,
    @taskName VARCHAR(50),
    @taskDescription VARCHAR(50),
    @PriorityLevel INT,
    @estTime INT
)
AS

BEGIN

    UPDATE
        tbl_Tasks

    SET fld_TaskName      = @taskName,

        fld_Description   = @taskDescription,

        fld_Priority      = @PriorityLevel,

        fld_EstimatedPomodoros = @estTime

    WHERE fld_TaskID = @taskID

END

GO

/****** Object:  StoredProcedure [dbo].[sp_UpdateTaskStatusByTaskID]    Script Date: 16-Jun-22 7:46:40 PM ******/

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE PROCEDURE [dbo].[sp_UpdateTaskStatusByTaskID](
    @taskID INT
)
AS

BEGIN

    DECLARE @now DATETIME = GETDATE();

    DECLARE @nowUTC DATETIME = DATEADD(HOUR, 2, @now);

    IF EXISTS(SELECT * FROM tbl_Tasks WHERE fld_TaskID = @taskID AND fld_StartTime IS NULL)
        BEGIN

            UPDATE tbl_Tasks

            SET fld_StartTime = @nowUTC

            WHERE fld_TaskID = @taskID;

        END

    ELSE

        BEGIN

            UPDATE tbl_Tasks

            SET fld_FinishTime = @nowUTC

            WHERE fld_TaskID = @taskID;

        END

END

GO
