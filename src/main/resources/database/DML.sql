INSERT INTO tbl_AccountTypes(fld_AccountTypeName, fld_RoleDescription)
VALUES ('Consultant', ' Office worker'),
       ('Manager', ' Manager'),
       ('Admin', ' Administrator');
GO

INSERT INTO tbl_Offices (fld_PhoneNo, fld_Zip, fld_Address)
VALUES ('12345678', '6400', 'address 1'),
       ('23456789', '6240', 'address 2'),
       ('34567890', '6200', 'address 3'),
       ('45678901', '6160', 'address 4'),
       ('56789012', '6140', 'address 5'),
       ('67890123', '6120', 'address 6'),
       ('78901234', '6080', 'address 7'),
       ('89012345', '6060', 'address 8'),
       ('90123456', '6040', 'address 9'),
       ('01234567', '6020', 'address 10');
GO

--Remove everything from tbl_Accounts tbl_Profiles tbl_ToDoList, tbl_Tasks
DELETE
FROM tbl_Tasks;
DELETE
FROM tbl_ToDoLists;
DELETE
FROM tbl_Accounts;
DELETE
FROM tbl_AccountSettings;

INSERT INTO tbl_Accounts(fld_AccountEmail, fld_FirstName, fld_LastName, fld_PhoneNumber, fld_Zip, fld_Address, fld_OfficeID, fld_TypeID)
VALUES ('dennis.k.n@hotmail.com', 'Dennis', 'Nielsen', '27896999', '2000', 'Address1', 1, 1),
       ('kotkadkofficial@gmail.com', 'Oliver', 'lastname2', '22380933', '3000', 'Address2', 1, 1),
       ('sonkoly.marci02@gmail.com', 'Marcell', 'Sonkoly', '30479199', '4000', 'Address3', 1, 1),
       ('ole.iersen403@gmail.com', 'Maamoun', 'LastName4', '42918169', '5000', 'Address4', 2, 1),
       ('allan2fernandes@hotmail.com', 'Allan', 'Fernandes', '21202139', '6000', 'Address5', 2, 1),
       ('example1@gmail.com', 'fExample1', 'lExample1', '12345678', '7000', 'Address6', 2, 1),
       ('example2@gmail.com', 'fExample2', 'lExample2', '12345678', '7000', 'Address7', 2, 1),
       ('example3@gmail.com', 'fExample3', 'lExample3', '12345678', '7000', 'Address8', 1, 1),
       ('example4@gmail.com', 'fExample4', 'lExample4', '12345678', '7000', 'Address9', 1, 1),
       ('example5@gmail.com', 'fExample5', 'lExample5', '12345678', '7000', 'Address10', 1, 1),
       ('c', 'fConsultant', 'lConsultant', '1', '6000', 'aConsultant', 1, 1)
GO

INSERT INTO tbl_AccountSettings(fld_AccountEmail, fld_WorkTime, fld_ShortBreakTime, fld_LongBreakTime, fld_LongBreakInterval,
                                fld_IsActivated)
VALUES ('dennis.k.n@hotmail.com', 50, 20, 40, 4, 1),
       ('kotkadkofficial@gmail.com', 40, 15, 30, 4, 1),
       ('sonkoly.marci02@gmail.com', 37, 20, 40, 4, 1),
       ('ole.iersen403@gmail.com', 45, 16, 32, 4, 1),
       ('allan2fernandes@hotmail.com', 34, 30, 54, 4, 1),
       ('example1@gmail.com', 34, 21, 42, 4, 1),
       ('example2@gmail.com', 59, 22, 24, 4, 1),
       ('example3@gmail.com', 34, 10, 20, 4, 1),
       ('example4@gmail.com', 40, 16, 32, 4, 1),
       ('example5@gmail.com', 34, 8, 16, 4, 1),
       ('c', 30, 15, 30, 4, 1);
GO

--Insert into tbl_ToDoList
DECLARE @today DATE = GETDATE();
INSERT INTO tbl_ToDoLists(fld_AccountEmail, fld_Date)
VALUES ('dennis.k.n@hotmail.com', @today),
       ('kotkadkofficial@gmail.com', @today),
       ('sonkoly.marci02@gmail.com', @today),
       ('ole.iersen403@gmail.com', @today),
       ('allan2fernandes@hotmail.com', @today),
       ('example1@gmail.com', @today),
       ('example2@gmail.com', @today),
       ('example3@gmail.com', @today),
       ('example4@gmail.com', @today),
       ('example5@gmail.com', @today)
GO
--Get the highest identity value from tbl_ToDoList
INSERT INTO tbl_Tasks(fld_ProjectID, fld_TaskName,
                      fld_Description, fld_Priority)
VALUES (3, 'TaskName1', 'Desc1', 1),
       (3, 'TaskName2', 'Desc2', 2),
       (3, 'TaskName3', 'Desc3', 3),
       (4, 'TaskName4', 'Desc4', 4),
       (4, 'TaskName5', 'Desc5', 4),
       (4, 'TaskName6', 'Desc6', 1),
       (7, 'TaskName7', 'Desc7', 2),
       (7, 'TaskName8', 'Desc8', 3),
       (7, 'TaskName9', 'Desc9', 2),
       (7, 'TaskName10', 'Desc10', 2),
       (7, 'TaskName11', 'Desc11', 2),
       (7, 'TaskName12', 'Desc12', 2),
       (7, 'TaskName13', 'Desc13', 2),
       (7, 'TaskName14', 'Desc14', 2),
       (7, 'TaskName15', 'Desc15', 2),
       (NULL, 'TaskName16', 'Desc16', 2),
       (NULL, 'TaskName17', 'Desc17', 1),
       (NULL, 'TaskName18', 'Desc18', 1),
       (NULL, 'TaskName19', 'Desc19', 1),
       (NULL, 'TaskName20', 'Desc20', 2),
       (NULL, 'TaskName21', 'Desc20', 2),
       (NULL, 'TaskName22', 'Desc20', 2),
       (NULL, 'TaskName23', 'Desc20', 2),
       (NULL, 'TaskName24', 'Desc20', 3),
       (NULL, 'TaskName25', 'Desc20', 3),
       (NULL, 'TaskName26', 'Desc20', 3),
       (NULL, 'TaskName27', 'Desc20', 2),
       (NULL, 'TaskName28', 'Desc20', 2),
       (NULL, 'TaskName29', 'Desc20', 2)
GO

INSERT INTO tbl_Projects(fld_ProjectName, fld_Description, fld_Deadline)
VALUES ('RProject1', 'RDescription1', '2015-03-18'),         --10
       ('RProject2', 'RDescription2', '2016-01-05'),         --11
       ('RProject3', 'RDescription3', '2016-05-17'),         --12
       ('RProject4', 'RDescription4', '2016-06-29'),         --13
       ('RProject5', 'RDescription5', '2016-06-30'),         --14
       ('RProject6', 'RDescription6', '2017-07-10'),         --15
       ('RProject7', 'RDescription7', '2017-08-30'),         --16
       ('RProject8', 'RDescription8', '2018-03-09'),         --17
       ('RProject9', 'RDescription9', '2020-09-18'),         --18
       ('RProject10', 'RDescription10', '2021-07-13'),       --19
      ('RProject11', 'RDescription11', '2021-09-02'),		--20
      ('RProject12', 'RDescription12', '2023-08-30'),		--21
      ('RProject13', 'RDescription13', '2023-10-12'),		--22
      ('RProject14', 'RDescription14', '2023-12-25'),		--23
      ('RProject15', 'RDescription15', '2024-04-04'),		--24
      ('RProject16', 'RDescription16', '2024-07-09'),		--25
      ('RProject17', 'RDescription17', '2024-07-10'),		--26
      ('RProject18', 'RDescription18', '2024-07-23'),		--27
      ('RProject19', 'RDescription19', '2025-09-04'),		--28
      ('RProject20', 'RDescription20', '2025-10-10')		--29

INSERT INTO tbl_Accounts(fld_AccountEmail, fld_FirstName, fld_LastName, fld_PhoneNumber, fld_Zip, fld_Address, fld_OfficeID, fld_TypeID)
VALUES('cons1@company.com', 'firstcons1', 'lastcons1', '12345678', '6400', 'consAddress1', 1, 1),
      ('cons2@company.com', 'firstcons2', 'lastcons2', '23456781', '6410', 'consAddress2', 3, 1),
      ('cons3@company.com', 'firstcons3', 'lastcons3', '86315868', '6420', 'consAddress3', 5, 1),
      ('cons4@company.com', 'firstcons4', 'lastcons4', '89413665', '6430', 'consAddress4', 1, 1),
      ('cons5@company.com', 'firstcons5', 'lastcons5', '98463854', '6410', 'consAddress5', 5, 1),
      ('cons6@company.com', 'firstcons6', 'lastcons6', '65489135', '6430', 'consAddress6', 6, 1),
      ('cons7@company.com', 'firstcons7', 'lastcons7', '65498315', '6450', 'consAddress7', 7, 1),
      ('cons8@company.com', 'firstcons8', 'lastcons8', '12635165', '6480', 'consAddress8', 4, 1),
      ('cons9@company.com', 'firstcons9', 'lastcons9', '98463548', '6440', 'consAddress9', 9, 1),
      ('cons10@company.com', 'firstcons10', 'lastcons10', '84675156', '6410', 'consAddress10', 7, 1),
      ('cons11@company.com', 'firstcons11', 'lastcons11', '65166318', '6400', 'consAddress11', 5, 1),
      ('cons12@company.com', 'firstcons12', 'lastcons12', '31612859', '6410', 'consAddress12', 4, 1),
      ('cons13@company.com', 'firstcons13', 'lastcons13', '48961378', '6430', 'consAddress13', 9, 1),
      ('cons14@company.com', 'firstcons14', 'lastcons14', '41784646', '6460', 'consAddress14', 10, 1),
      ('cons15@company.com', 'firstcons15', 'lastcons15', '52161635', '6440', 'consAddress15', 2, 1)


--Create a project then create task then create todolist, then add tasks to todolist
INSERT INTO tbl_Projects(fld_ProjectName, fld_Description, fld_Deadline)
VALUES ('OverDueProject', 'Project to test over due', '2020-03-18'), --10
       ('NotOverDueProject', 'Description2', '2023-01-05'),
       ('NotOverDueProject', 'Description3', '2023-05-17'),
       ('OverDueProject#2', 'Project to test over due', '2020-03-18')


INSERT INTO tbl_ToDoLists(fld_AccountEmail, fld_Date)
VALUES ('dennis.k.n@hotmail.com', '2000-03-18'),
       ('kotkadkofficial@gmail.com', '2000-03-18'),
       ('sonkoly.marci02@gmail.com', '2000-03-18'),
       ('ole.iersen403@gmail.com', '2000-03-18')

INSERT INTO tbl_Tasks(fld_ProjectID, fld_TaskName, fld_ToDoListID, fld_Description, fld_Priority, fld_EstimatedPomodoros)
VALUES (47, 'OverDueTask', 63, 'OverDueTask', 1, 3),
       (48, 'NotOverDueTask', 64, 'NotOverDueTask', 1, 3),
       (49, 'NotOverDueTask', 65, 'NotOverDueTask', 1, 3),
       (50, 'OverDueTask', 66, 'OverDueTask', 1, 3)

INSERT INTO tbl_Projects(fld_ProjectName, fld_Description, fld_Deadline)
VALUES ('OliverOverdue', 'OliverOverdue', '2022-06-09')

INSERT INTO tbl_Tasks(fld_ProjectID, fld_TaskName, fld_ToDoListID, fld_Description, fld_Priority, fld_EstimatedPomodoros, fld_StartTime,
                      fld_FinishTime)
VALUES (52, 'OverDueTask', 42, 'OverDueTask', 1, 3, '2022-06-07', '2022-06-10'),
       (52, 'IncompleteTask', 42, 'IncompleteTask', 1, 3, '2022-06-07', null),
       (52, 'CompletedTask', 42, 'CompletedTask', 1, 3, '2022-06-07', '2022-06-08'),
       (3, 'OverdueAndIncompleteTask', 42, 'OverdueAndIncompleteTask', 1, 3, '2015-01-10', null)

INSERT INTO tbl_Tasks(fld_ProjectID, fld_TaskName, fld_ToDoListID, fld_Description, fld_Priority, fld_EstimatedPomodoros, fld_StartTime,
                      fld_FinishTime)
VALUES (52, 'OverDueTask', 43, 'OverDueTask', 1, 3, '2022-06-07', '2022-06-10'),
       (52, 'NotOverDueTask', 43, 'NotOverDueTask', 1, 3, '2022-06-08', '2022-06-09'),
       (52, 'OverDueTask', 43, 'OverDueTask', 1, 3, '2022-06-09', '2022-06-11'),
       (52, 'NotOverDueTask', 44, 'NotOverDueTask', 1, 3, '2022-06-07', '2022-06-09'),
       (52, 'OverDueTask', 44, 'OverDueTask', 1, 3, '2022-06-09', '2022-06-10'),
       (52, 'NotOverDueTask', 44, 'NotOverDueTask', 1, 3, '2022-06-08', '2022-06-08'),
       (52, 'OverDueTask', 44, 'OverDueTask', 1, 3, '2022-06-09', '2022-06-11'),
       (52, 'OverDueTask', 41, 'OverDueTask', 1, 3, '2022-06-07', '2022-06-10'),
       (52, 'NotOverDueTask', 41, 'NotOverDueTask', 1, 3, '2022-06-08', '2022-06-09'),
       (52, 'NotOverDueTask', 41, 'NotOverDueTask', 1, 3, '2022-06-08', '2022-06-08'),
      (52,'OverDueTask',41,'OverDueTask',1,3,'2022-06-09','2022-06-11'),
      (52,'OverDueTask',41,'OverDueTask',1,3,'2022-06-10','2022-06-12'),
      (52,'OverDueTask',45,'OverDueTask',1,3,'2022-06-08','2022-06-10'),
      (52,'NotOverDueTask',45,'NotOverDueTask',1,3,'2022-06-08','2022-06-09'),
      (52,'NotOverDueTask',45,'NotOverDueTask',1,3,'2022-06-07','2022-06-08'),
      (52,'NotOverDueTask',45,'NotOverDueTask',1,3,'2022-06-08','2022-06-07'),
      (52,'NotOverDueTask',45,'NotOverDueTask',1,3,'2022-06-07','2022-06-09')


UPDATE tbl_Tasks SET fld_StartTime = '2021-03-18' WHERE fld_TaskID = 115
UPDATE tbl_Tasks SET fld_StartTime = '2020-03-10', fld_FinishTime = '2020-03-20' WHERE fld_TaskID = 118
UPDATE tbl_Tasks SET fld_StartTime = '2020-03-10', fld_FinishTime = '2022-03-25' WHERE fld_TaskID = 116
UPDATE tbl_Tasks SET fld_StartTime = '2020-03-10', fld_FinishTime = '2022-04-27' WHERE fld_TaskID = 117