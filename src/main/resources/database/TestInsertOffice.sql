DECLARE @foo TABLE
             (
                 row#             int,
                 fld_AccountEmail varchar(50)
             )
INSERT INTO @foo
SELECT row_number() OVER (ORDER BY fld_AccountEmail) AS row#, fld_AccountEmail
FROM tbl_Accounts
WHERE fld_TypeID = 1
  AND fld_OfficeID = 1

--For each row in @foo, insert test data with sp_InsertTestDataOfficeByEmail
DECLARE @row int
SET @row = 1
WHILE @row <= (SELECT COUNT(*)
               FROM @foo)
    BEGIN
        DECLARE @email varchar(50)
        SET @email = (SELECT fld_AccountEmail FROM @foo WHERE row# = @row)
        DECLARE @taskCount INT;
        -- Random number between 1 and 4
        SET @taskCount = (RAND() * 3) + 1
        EXEC sp_InsertTestDataOfficeWithEmail @email, @taskCount
        SET @row = @row + 1
    end