use db_pomodoro
go

create table tbl_Offices
(
    fld_OfficeID   int identity
        primary key,
    fld_PhoneNo    varchar(8)  not null,
    fld_Zip        varchar(4)  not null,
    fld_Address    varchar(50) not null,
    fld_OfficeName varchar(50) not null
)
go
create table tbl_AccountTypes
(
    fld_TypeID          int identity
        primary key,
    fld_AccountTypeName varchar(15)  not null,
    fld_RoleDescription varchar(100) not null
)
go

create table tbl_Accounts
(
    fld_AccountEmail varchar(50) not null
        primary key,
    fld_FirstName    varchar(20) not null,
    fld_LastName     varchar(20) not null,
    fld_PhoneNumber  varchar(8)  not null,
    fld_Zip          varchar(4)  not null,
    fld_Address      varchar(50) not null,
    fld_OfficeID     int         not null
        references tbl_Offices,
    fld_TypeID       int         not null
        references tbl_AccountTypes
)
go

create table tbl_AccountSettings
(
    fld_AccountSettingID  int identity
        primary key,
    fld_AccountEmail      varchar(50) not null
        references tbl_Accounts,
    fld_WorkTime          int         not null,
    fld_ShortBreakTime    int         not null,
    fld_LongBreakTime     int         not null,
    fld_LongBreakInterval int         not null,
    fld_IsActivated       bit         not null
)
go


create table tbl_Breaks
(
    fld_BreakID      int identity
        primary key,
    fld_AccountEmail varchar(50) not null
        references tbl_Accounts,
    fld_BreakStart   datetime    not null,
    fld_BreakEnd     datetime
)
go


create table tbl_Pauses
(
    fld_PauseID      int identity
        primary key,
    fld_AccountEmail varchar(50) not null
        references tbl_Accounts,
    fld_PauseStart   datetime    not null,
    fld_PauseEnd     datetime
)
go

create table tbl_Projects
(
    fld_ProjectID   int identity
        primary key,
    fld_ProjectName varchar(50) not null,
    fld_Description varchar(50) not null,
    fld_Deadline    date        not null
)
go

create table tbl_Permissions
(
    fld_PermissionID int identity
        primary key,
    fld_ProjectID    int         not null
        references tbl_Projects,
    fld_AccountEmail varchar(50) not null
        references tbl_Accounts,
    fld_View         bit         not null,
    fld_Edit         bit         not null
)
go

create table tbl_ToDoLists
(
    fld_ToDoListID   int identity
        primary key,
    fld_AccountEmail varchar(50) not null
        references tbl_Accounts,
    fld_Date         date        not null
)
go

create table tbl_Tasks
(
    fld_TaskID             int identity
        primary key,
    fld_ProjectID          int
        references tbl_Projects,
    fld_TaskName           varchar(50) not null,
    fld_ToDoListID         int
        references tbl_ToDoLists,
    fld_Description        varchar(50) not null,
    fld_Priority           int         not null,
    fld_EstimatedPomodoros int,
    fld_StartTime          datetime,
    fld_FinishTime         datetime,
    fld_Order              int,
    fld_TotalPomodoros     int
)
go


